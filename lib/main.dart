import 'package:bobot01/jpush/jpush_manager.dart';
import 'package:bobot01/ui/splash_viewcontroller.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'repository/user_api_repository.dart';

final apiProvider = Provider<UserApiRepositoryImpl>(
    (ref) => UserApiRepositoryImpl()
);

final GlobalKey<NavigatorState> navigatorKey = GlobalKey();
final scaffoldStateKey = GlobalKey<ScaffoldState>();

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await JPushManeger().initJPush();
  await EasyLocalization.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitDown,DeviceOrientation.portraitUp]).then((value) {
    runApp(ProviderScope(child: MyApp()));
  });
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      // localizationsDelegates: context.localizationDelegates,
      // supportedLocales: context.supportedLocales,
      // locale: context.locale,
      title: 'bobot01',
      // builder: DevicePreview.appBuilder, //must add
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        appBarTheme: AppBarTheme(
          backwardsCompatibility: false, // 1
          systemOverlayStyle: SystemUiOverlayStyle.dark, // 2
        ),
      ),
      home: SplashScreen(),
      navigatorKey: navigatorKey,
    );
  }
}