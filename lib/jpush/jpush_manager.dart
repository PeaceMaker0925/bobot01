import 'package:jpush_flutter/jpush_flutter.dart';
import '../constants/shared_preference.dart';


class JPushManeger{
  Future<void> initJPush() async {
    JPush jPush = JPush();

    /// api get registration id
    String channel = 'defaultChannel';

    ///配置应用 Key
    jPush.setup(
      appKey: "71da12c318f9f3ba81788832",
      channel: channel,
      production: false,
      debug: true,
    );

    jPush.setAlias("Bobot").then((map) {
      print('--------設置成功-----');
    });

    jPush.getRegistrationID().then((token) {
      print("notification Token $token");
      // BobotSharedPreferences().setNotiToken(token);
      BobotSharedPreferences().saveDeviceToken(token);
    }).onError((error, stackTrace) {
      print('error');
      print(error);
    });

    jPush.setAuth(enable: true);

    jPush.applyPushAuthority(
        NotificationSettingsIOS(sound: true, alert: true, badge: true));
    try {
      jPush.addEventHandler(
        onReceiveNotification: (Map<String, dynamic> message) async {
          print("flutter onReceiveNotification: $message");
        },
        onOpenNotification: (Map<String, dynamic> message) async {
          print("flutter onOpenNotification: $message");
          // _clickNotificationToPushPage(message); // 點擊推播action
        },
        onReceiveMessage: (Map<String, dynamic> message) async {
          print("flutter onReceiveMessage: $message");
        },
      );
    } on Exception {
      print("Failed to get platform version");
    }
  }
}