import 'package:bobot01/ui/bot/query_bot_viewcontroller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import '../../constants/constants.dart';
import '../../constants/public_component.dart';
import '../sign_in/login_viewmodel.dart';
import 'bot_viewmodel.dart';

class BotCopyScreen extends ConsumerWidget {
  const BotCopyScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return buildFlexBodyLayout(context, Container(margin: EdgeInsets.symmetric(
        horizontal: Constant().getViewWidth(context) / 20),
        child: Column(
            children: [
              Spacer(),
              Expanded(flex: 2, child: GestureDetector(
                onTap: (){
                  ref.watch(activationStatusProvider.future).then((event) {
                    event.then((value) {
                      showResponseDialog(context, "${(value).url}\n\n${(value).code}\n\n${(value).message}\n\n${(value).data}", (){});
                    });
                  });
                },
                child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.blueAccent),
                  child: Text('queryActivationStatus'),
                ),
              ),),
              Spacer(),
              Expanded(flex: 2, child: GestureDetector(
                onTap: (){
                  ref.watch(activationRecordProvider.future).then((event) {
                    event.then((value) {
                      showResponseDialog(context, "${(value).url}\n\n${(value).code}\n\n${(value).message}\n\n${(value).data}", (){});
                    });
                  });
                },
                child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.blueAccent),
                  child: Text('queryActivationRecord'),
                ),
              ),),
              Spacer(),
              Expanded(flex: 2, child: GestureDetector(
                onTap: (){
                  ref.watch(activationInfoProvider.future).then((event) {
                    event.then((value) {
                      showResponseDialog(context, "${(value).url}\n\n${(value).code}\n\n${(value).message}\n\n${(value).data}", (){});
                    });
                  });
                },
                child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.blueAccent),
                  child: Text('queryActivationInfo'),
                ),
              ),),
              Spacer(),
              Expanded(flex: 2, child: GestureDetector(
                onTap: (){
                  ref.watch(activationCouponInfoProvider("0925").future).then((event) {
                    event.then((value) {
                      showResponseDialog(context, "${(value).url}\n\n${(value).code}\n\n${(value).message}\n\n${(value).data}", (){});
                    });
                  });
                },
                child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.blueAccent),
                  child: Text('queryActivationCouponInfo'),
                ),
              ),),
              Spacer(),
              Expanded(flex: 2, child: GestureDetector(
                onTap: (){
                  showResponseDialog(context, "Not yet Impl", (){});
                },
                child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.greenAccent),
                  child: Text('activationEnable'),
                ),
              ),),
              Spacer(),
              Expanded(flex: 2, child: GestureDetector(
                onTap: (){
                  // Navigator.push(context, MaterialPageRoute(builder: (context) => CreateBotScreen()));
                  print("click c b");
                  ref.read(bodyIndexProvider.notifier).setBodyIndex(5);
                },
                child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.orangeAccent),
                  child: Text('Create Bot'),
                ),
              ),),
              Spacer(),
              Expanded(flex: 2, child: GestureDetector(
                onTap: (){
                  ref.watch(myBotProvider.future).then((event) {
                    event.then((value) {
                      showResponseDialog(context, "${(value).url}\n\n${(value).code}\n\n${(value).message}\n\n${(value).data}", (){});
                    });
                  });
                },
                child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.orangeAccent),
                  child: Text('Query My Bot'),
                ),
              ),),
              Spacer(),
              Expanded(flex: 2, child: GestureDetector(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) => QueryBotScreen()));
                },
                child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.orangeAccent),
                  child: Text('Query Bot Info'),
                ),
              ),),
              Spacer(),
              Expanded(flex: 2, child: GestureDetector(
                onTap: (){
                  ref.watch(myBalanceProvider.future).then((event) {
                    event.then((value) {
                      showResponseDialog(context, "${(value).url}\n\n${(value).code}\n\n${(value).message}\n\n${(value).data}", (){});
                    });
                  });
                },
                child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.orangeAccent),
                  child: Text('Query Balance'),
                ),
              ),),
              Spacer()
            ])));
  }
}
