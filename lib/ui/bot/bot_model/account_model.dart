
class ProfileCellListInfo {
  String title;
  String imagePath;
  Function() action;
  ProfileCellListInfo({
    required this.title,
    required this.imagePath,
    required this.action
  });
}

enum AccountGridViewType1 {
  Energy,
  Referral,
}

enum AccountGridViewType2 {
  Receive,
  Transfer,
}

class AccountGridViewInfo {
  String time;
  AccountGridViewType1 accountType;
  AccountGridViewType2 Type;
  double price;

  AccountGridViewInfo({
    this.time = '',
    required this.accountType,
    required this.Type,
    required this.price
  });
}