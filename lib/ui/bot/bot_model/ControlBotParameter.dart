import 'package:equatable/equatable.dart';

class ControlBotParameter extends Equatable {
  final String robotNo;
  final String status;

  const ControlBotParameter({
    required this.robotNo,
    required this.status,
  });

  @override
  List<Object?> get props => [robotNo, status];
}