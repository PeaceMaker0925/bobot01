import 'package:equatable/equatable.dart';

class CreateBobotListParameter extends Equatable {
  final String exName;
  final String coinName;

  const CreateBobotListParameter({
    required this.exName,
    required this.coinName,
  });

  @override
  List<Object?> get props => [exName, coinName];
}
class QueryRangeParameter extends Equatable {
  final String exName;
  final String coinName;
  final String tradeType;

  const QueryRangeParameter({
    required this.exName,
    required this.coinName,
    required this.tradeType,
  });

  @override
  List<Object?> get props => [exName, coinName, tradeType];
}