import 'package:equatable/equatable.dart';

class CreateBotParameter extends Equatable {
  final String marketName;
  final String coinName;
  final String positionNum;
  final String strategyType;
  final String tradeType;
  final String lever;

  const CreateBotParameter({
    required this.marketName,
    required this.coinName,
    required this.positionNum,
    required this.strategyType,
    required this.tradeType,
    required this.lever,
  });

  @override
  List<Object?> get props => [marketName, coinName, positionNum, strategyType, tradeType, lever];
}