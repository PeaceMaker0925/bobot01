import 'package:bobot01/constants/theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import '../../constants/SignInWidgets.dart';
import '../../constants/constants.dart';
import '../../constants/lightCell.dart';
import '../../constants/public_component.dart';
import '../../constants/shared_preference.dart';
import '../../main.dart';
import '../home/home_viewcontroller.dart';
import '../sign_in/login_viewmodel.dart';
import 'bot_model.dart';
import 'bot_viewmodel.dart';

class BotScreen extends ConsumerStatefulWidget {
  const BotScreen({Key? key}) : super(key: key);
  @override
  ConsumerState<BotScreen> createState() => _BotScreenState();
}

class _BotScreenState extends ConsumerState<BotScreen> {

  bool isLoading = false;
  bool isClose = false;
  bool isPause = false;
  bool isDelete = false;
  List<BotInfo> botList = List.empty(growable: true);
  List<BotInfo> filteredList = List.empty(growable: true);
  List<String> filterList = ["All", "Spot", "Contract",];
  int currentFilterNum = 0;
  @override
  void initState() {
    ref.read(myBotProvider.future).then((event) {
      event.then((value) {
        List<dynamic> BobotList = value.data;
        BobotList.map((info) {
          botList.add(BotInfo(
              marketCoinName: info['marketCoinName'],
              positionNum: info['positionNum'].toString(),
              positionType: info['positionType'],
              price: info['price'].toString(),
              tradeType: info['tradeType'],
              incomeTotal: info['incomeTotal'].toString(),
              riskRating: info['riskRating']
          ));
        }).toList();
        // print("priceList = $priceList");
        filteredList.addAll(botList);
        setState(() {});

      });
    });
  }

  @override
  Widget build(BuildContext context) {

    List<String> botInfoItemList = [
      "RobotType\nToken","Num\nType","TokenPrice\nIncome",
    ];
    ref.listen(isLoadingProvider,(_,__){
      setState(() {
        isLoading = __;
      });
    });
    return buildFlexBodyLayout(context, Container(margin: EdgeInsets.symmetric(
        horizontal: Constant().getViewWidth(context) / 20),
        child: Stack(children: [
        Container(margin: EdgeInsets.only(top: Constant().getViewHeight(context) * 72/100), alignment: Alignment.bottomLeft, height: Constant().getViewHeight(context)/8, child: Image.asset('assets/img/img_bg_bobot_01.png'),),
        Column(
            children: [
              Expanded(flex: 1, child: Container(child: buildAppBar(title: "My Bobot", ref: ref)),),
              Expanded(flex: 2, child: CreateBotBtn(onTap: (){
                ///need login
                BobotSharedPreferences().getUserToken().then((value) {
                  setState(() {
                    value != "userToken"
                        ? ref.read(bodyIndexProvider.notifier).setBodyIndex(5)
                        : ref.read(showToLogInProvider.notifier).setShowToLogIn(!ref.watch(showToLogInProvider));
                  });
                });
              })),
              Expanded(flex: 1, child: Container(alignment: Alignment.center, child: Row(children: [
                Expanded(flex: 5, child: BotContent()),
                Expanded(flex: 1, child: CircleSearchButton())
              ],),)),
              Expanded(flex: 1, child: Container(
                  alignment: Alignment.center,
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: botInfoItemList.map((item) => Text(item)).toList()
                  ))),
              Expanded(flex: 6, child: Container(child: SingleChildScrollView(child: Column(children: filteredList.map((botInfo) => buildBotListColumn(botInfo)).toList(),)),)),
              Expanded(flex: 2, child: Container()),
            ])])));
  }
  Widget BotContent() {
    return Container(color: Colors.transparent, alignment: Alignment.center, child: Row(children: filterList.asMap().entries.map((item) {
        bool isCurrent = currentFilterNum == item.key;
        return Expanded(child: GestureDetector(child: Container(
          decoration: BoxDecoration(
            border: Border(
              bottom: BorderSide(width: isCurrent ? 3 : 1, color: CustomTheme.fontPurple01Color.withAlpha(isCurrent ? 255 : 130)),
            ),
          ),
          padding: EdgeInsets.all(Constant().getViewWidth(context)/40),
          alignment: Alignment.center,
          child: Text(item.value, style: isCurrent ? CustomTheme.textPrimaryPurple02 : CustomTheme.textPrimaryPurple02Thin,),),
          onTap: (){
            currentFilterNum = item.key;
            filteredList.clear();
            filteredList.addAll(botList);
            filteredList.retainWhere((info){
              return (info.positionType).contains(item.value);
            });
            if(item.value == "All"){
              filteredList.clear();
              filteredList.addAll(botList);
            }
            setState(() { });

          },));
    }).toList(),),
    );
  }

  Widget buildBotListColumn(BotInfo info) {
      return GestureDetector(child: Container(
        margin: EdgeInsets.symmetric(vertical: Constant().getViewHeight(context)/140),
        child: lightCell(Container(padding: EdgeInsets.symmetric(vertical: Constant().getViewHeight(context)/140),child: Row(children: [
          Expanded(flex: 3, child: riskBobotImg(info.riskRating)),
          Expanded(flex: 8, child: Column(children: [
            Container(alignment: Alignment.topLeft, child: Text(info.tradeType, textAlign: TextAlign.left, style: TextStyle(fontSize: 16, color: CustomTheme.fontPurple02Color), maxLines: 1, overflow: TextOverflow.ellipsis,),),
            Container(alignment: Alignment.topLeft, child: Text(info.marketCoinName, style: TextStyle(fontSize: 12, color: CustomTheme.fontPurple01Color), maxLines: 1, overflow: TextOverflow.ellipsis,)),
          ],)),
          Expanded(flex: 1, child: Container()),
          Expanded(flex: 3, child: Container(alignment: Alignment.centerLeft, child: Text(info.positionNum, textAlign: TextAlign.center, style: TextStyle(fontSize: 16, color: CustomTheme.fontPurple02Color),)),),
          Expanded(flex: 5, child: Container(alignment: Alignment.centerLeft, child: Text(info.positionType, textAlign: TextAlign.center, style: TextStyle(fontSize: 12, color: CustomTheme.fontPurple01Color),)),),
          Expanded(flex: 8, child: Column(children: [
            Text(info.price, textAlign: TextAlign.center, style: TextStyle(fontSize: 16, color: CustomTheme.fontPurple02Color),),
            RedGreenCell(info.incomeTotal)
          ],)),
        ],),)),),onTap: (){
        showDialog(
            context: context,
            builder: (_) => StatefulBuilder(  // You need this, notice the parameters below:
            builder: (BuildContext context, StateSetter setState) {
          return Container(
            alignment: Alignment.center,
            margin: EdgeInsets.symmetric(vertical: Constant().getViewHeight(context)*0.1, horizontal: Constant().getViewWidth(context)*0.05),
            child: lightCell(isBar: true, Container(
              margin: EdgeInsets.symmetric(horizontal: Constant().getViewWidth(context)/20),
              child: Column(
                  children: [
                    Expanded(flex: 3, child: Container(alignment: Alignment.center, child: Text("量化控制", style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 18, fontWeight: FontWeight.w700)),)),
                    Expanded(flex: 4, child: Container(alignment: Alignment.center, child: Text(
                      "請點擊項目來設置量化控制。\n若想更改為預設值(單純運行，不設置量化控制)，\n請再次點擊勾選項目並關閉視窗即可取消勾選。",
                      style: TextStyle(color: CustomTheme.fontPurple01Color, fontSize: 14, fontWeight: FontWeight.w700),
                      textAlign: TextAlign.center,
                    ),)),
                    Spacer(),
                    Expanded(flex: 6, child: Column(children: [
                      Expanded(flex: 1, child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                        GestureDetector(onTap: (){
                          isClose = !isClose;
                          setState(() {});
                          // ref.watch(controlBotProvider().future)
                        }, child: Container(height: Constant().getViewHeight(context)/26, child: Image.asset(isClose?"assets/btn/btn_open_01_hover.png":"assets/btn/btn_close_01_nor.png"),),),
                        SizedBox(width: Constant().getViewHeight(context)/60,),
                        Text("平倉關閉", style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 12, fontWeight: FontWeight.w700))
                      ],)),
                      Expanded(flex: 1, child: Container(alignment: Alignment.centerLeft, child: Text("立即以市價賣出，並關閉機器人",style: TextStyle(color: CustomTheme.fontPurple01Color, fontSize: 12),),)),
                      SizedBox(height: Constant().getViewHeight(context)/80,),
                      Expanded(flex: 1, child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                        GestureDetector(onTap: (){
                          isPause = !isPause;
                          setState(() {});
                        }, child: Container(height: Constant().getViewHeight(context)/26, child: Image.asset(isPause?"assets/btn/btn_open_01_hover.png":"assets/btn/btn_close_01_nor.png"),),),
                        SizedBox(width: Constant().getViewHeight(context)/60,),
                        Text("暫停", style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 12, fontWeight: FontWeight.w700))
                      ],)),
                      Expanded(flex: 1, child: Container(alignment: Alignment.centerLeft, child: Text("機器人會暫時停止運作",style: TextStyle(color: CustomTheme.fontPurple01Color, fontSize: 12),),)),
                      SizedBox(height: Constant().getViewHeight(context)/80,),
                      Expanded(flex: 1, child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                        GestureDetector(onTap: (){
                          isDelete = !isDelete;
                          setState(() {});
                        }, child: Container(height: Constant().getViewHeight(context)/26, child: Image.asset(isDelete?"assets/btn/btn_open_01_hover.png":"assets/btn/btn_close_01_nor.png"),),),
                        SizedBox(width: Constant().getViewHeight(context)/60,),
                        Text("關閉AI", style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 12, fontWeight: FontWeight.w700))
                      ],)),
                      Expanded(flex: 1, child: Container(alignment: Alignment.centerLeft, child: Text("直接關閉並刪除機器人",style: TextStyle(color: CustomTheme.fontPurple01Color, fontSize: 12),),))
                    ],)),
                    Spacer(),
                    Expanded(flex: 2, child: GestureDetector(
                      onTap: () { Navigator.pop(context); },
                      child: Container(
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          color: CustomTheme.fontPurple01Color,
                          borderRadius: BorderRadius.all(Radius.circular(35)),
                        ),
                        child: Text("關閉視窗", style: CustomTheme.textPrimary,),
                      ),)),
                    Spacer()
                  ]
              ),)),
          );}),
        );
      },);
  }

  Widget ControlBotDialog(){
    return lightCell(isBar: true, Container());
  }

  Widget riskBobotImg(String riskRating) {
    String imgPath = "";
    switch(int.parse(riskRating)){
      case 1:
      case 2:
      case 3:
        imgPath = 'assets/img/img_bobot_green_01.png';
        break;
      case 4:
      case 5:
      case 6:
      case 7:
        imgPath = 'assets/img/img_bobot_nor_01.png';
        break;
      case 8:
      case 9:
      case 10:
        imgPath = 'assets/img/img_bobot_red_01.png';
        break;
    }
    return Container(child: Image.asset(imgPath, height: Constant().getViewHeight(context)/30,),);
  }
}

Widget CreateBotBtn({required Function() onTap}) {
  var context = navigatorKey.currentContext!;
  return Stack(children: [
    Container(margin: EdgeInsets.only(top: Constant().getViewHeight(context)/22),alignment: Alignment.topCenter,height: Constant().getViewHeight(context)/16, child: Image.asset('assets/img/img_btn_creat bobot_01.png'),),
    Container(
      alignment: Alignment.topCenter,
      margin: EdgeInsets.only(top: Constant().getViewHeight(context)/10),
      child: GestureDetector(
        onTap: onTap,
        child: Container(
          alignment: Alignment.center,
          // padding: EdgeInsets.all(10),
          width: Constant().getViewWidth(context)/3,
          height: Constant().getViewWidth(context)/8,
          // decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.blueAccent),
          child: lightCell(Text('CreateBobot', style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 14),), isBar: true),
        ),
      ),)
  ],);
}
