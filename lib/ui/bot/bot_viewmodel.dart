import 'package:bobot01/repository/activation_api_repository.dart';
import 'package:bobot01/ui/bot/bot_model/CreateBotParameter.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../repository/api_response.dart';
import '../../repository/bot_api_repository.dart';
import 'bot_model/ControlBotParameter.dart';
import 'bot_model/CreateBobotListParameter.dart';
import 'bot_model/EnableBotParameter.dart';

final activationStatusProvider = FutureProvider.autoDispose<Future<ApiResponse>>((ref) async {
  return ActivationApiRepositoryImpl().queryActivationStatus();
});

final activationRecordProvider = FutureProvider.autoDispose<Future<ApiResponse>>((ref) async {
  return ActivationApiRepositoryImpl().queryActivationRecord();
});

final activationInfoProvider = FutureProvider.autoDispose<Future<ApiResponse>>((ref) async {
  return ActivationApiRepositoryImpl().queryActivationInfo();
});

final activationCouponInfoProvider = FutureProvider.autoDispose.family<Future<ApiResponse>, String>((ref, code) async {
  return ActivationApiRepositoryImpl().queryActivationCouponInfo(code);
});

final activationEnableProvider = FutureProvider.autoDispose.family<Future<ApiResponse>, EnableBotParameter>((ref, parameter)  async {
  return ActivationApiRepositoryImpl().activationEnable(parameter);
});

final myBotProvider = FutureProvider.autoDispose<Future<ApiResponse>>((ref) async {
  return BotApiRepositoryImpl().queryMyBot();
});

final myBalanceProvider = FutureProvider.autoDispose<Future<ApiResponse>>((ref) async {
  return BotApiRepositoryImpl().queryBalance();
});

final connectedExProvider = FutureProvider.autoDispose<Future<ApiResponse>>((ref) async {
  return BotApiRepositoryImpl().queryConnectedEx();
});

final exCoinListProvider = FutureProvider.autoDispose.family<Future<ApiResponse>, String>((ref, exName) async {
  return BotApiRepositoryImpl().queryExCoinList(exName);
});

final createBobotListProvider = FutureProvider.autoDispose.family<Future<ApiResponse>, CreateBobotListParameter>((ref, parameter) async {
  return BotApiRepositoryImpl().queryCreateBobotList(parameter.exName, parameter.coinName);
});

final queryRangeProvider = FutureProvider.autoDispose.family<Future<ApiResponse>, QueryRangeParameter>((ref, parameter) async {
  return BotApiRepositoryImpl().queryRange(parameter.exName, parameter.coinName, parameter.tradeType);
});

final botSettingsProvider = FutureProvider.autoDispose<Future<ApiResponse>>((ref) async {
  return BotApiRepositoryImpl().queryBotSettings();
});

final botInfoProvider = FutureProvider.autoDispose.family<Future<ApiResponse>, String>((ref, tradeType) async {
  return BotApiRepositoryImpl().queryBotInfo(tradeType);
});

final createBotProvider = FutureProvider.autoDispose.family<Future<ApiResponse>, CreateBotParameter>((ref, parameter) async {
  return BotApiRepositoryImpl().createBot(parameter);
});

final controlBotProvider = FutureProvider.autoDispose.family<Future<ApiResponse>, ControlBotParameter>((ref, parameter) async {
  return BotApiRepositoryImpl().controlBot(parameter);
});
