class BotInfo {
  String marketCoinName;
  String positionNum;
  String positionType;
  String price;
  String incomeTotal;
  String tradeType;
  String riskRating;

  BotInfo({
    required this.marketCoinName,
    required this.positionNum,
    required this.positionType,
    required this.price,
    required this.incomeTotal,
    required this.tradeType,
    required this.riskRating,
  });
}

class CopyBotInfo {
  String market;
  String coin;
  String robotType;
  String tradeType;
  String positionType;
  String incomeRate;
  String openDays;
  String followNum;
  String riskRating;

  CopyBotInfo({
    required this.market,
    required this.coin,
    required this.robotType,
    required this.tradeType,
    required this.positionType,
    required this.incomeRate,
    required this.openDays,
    required this.followNum,
    required this.riskRating,
  });
}