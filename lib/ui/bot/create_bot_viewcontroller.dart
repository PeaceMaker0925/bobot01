import 'package:bobot01/ui/bot/bot_model/CreateBobotListParameter.dart';
import 'package:bobot01/ui/bot/bot_model/CreateBotParameter.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import '../../constants/constants.dart';
import '../../constants/theme.dart';
import '../../constants/public_component.dart';
import '../navigation_viewcontroller.dart';
import 'bot_viewcontroller.dart';
import 'bot_viewmodel.dart';

const List<String> marketList = <String>['Binance', 'Huobi'];
// const List<String> coinList = <String>['ADA', 'ETH', 'BNB', 'BTC'];
const List<String> kindList = <String>['CONSERVATIVE','ROBUST','ACTIVE'];
const List<String> strategyList = <String>['Auto','Bull','Bear'];
const List<String> positionList = ["Spot", "Contract",];
const List<String> leverList = <String>['1','2','3','4','5'];

class CreateBotScreen extends ConsumerStatefulWidget {
  const CreateBotScreen({super.key});

  @override
  ConsumerState<CreateBotScreen> createState() => _CreateBotScreenState();
}

class _CreateBotScreenState extends ConsumerState<CreateBotScreen> {
  List<dynamic> connectedExList = List.empty(growable: true);
  List<dynamic> bot1List = List.empty(growable: true);
  List<dynamic> bot2List = List.empty(growable: true);
  List<dynamic> bot3List = List.empty(growable: true);
  List<dynamic> currentBotList = List.empty(growable: true);
  List<String> coinList = List.empty(growable: true);
  String marketValue = marketList.first;
  String coinValue = "";
  String tradeTypeValue = "";
  String positionTypeValue = "";
  String rangeString = "";
  dynamic botValue = "";
  String kindValue = "";
  String strategyValue = strategyList.first;
  String leverValue = leverList.first;
  String balanceValue = "";
  bool isExConnected = false;
  TextEditingController positionTextController = TextEditingController();
  FocusNode positionFocusNode = FocusNode();

  @override
  void initState() {

    ref.read(connectedExProvider.future).then((event) {
      event.then((value) {
        List<dynamic> exList = (value).data;
        connectedExList.clear();
        exList.map((exInfo) {
          if(exInfo['bind'] == "true") connectedExList.add(exInfo['marketName']);
        }).toList();
        isExConnected = connectedExList.contains(marketValue);
        setState(() { });
        // showResponseDialog(context, "${(value).url}\n\n${(value).code}\n\n${(value).message}\n\n${(value).data}", (){});
      });
    });

    getCoinList();

  }

  @override
  Widget build(BuildContext context) {
    return buildFlexBodyLayout(context, InkWell(onTap: () { positionFocusNode.unfocus(); }, child: Container(margin: EdgeInsets.symmetric(
        horizontal: Constant().getViewWidth(context) / 20),
        child: Column(
            children: [
              Expanded(flex: 3, child: Container(child: Center(child: Text("CreateBobot", style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 18 , fontWeight: FontWeight.w700), )),)),
              Expanded(flex: 2, child: Row(children: [
                Expanded(flex: 6, child: Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.symmetric(horizontal: 15.0),
                  decoration: BoxDecoration(color: Colors.white, border: Border.all(color: CustomTheme.fontPurple02Color), borderRadius: BorderRadius.circular(35),),
                  child: DropdownButtonHideUnderline(child: DropdownButton2<String>(
                    isExpanded: true,
                    value: marketValue,
                    icon: const Icon(Icons.keyboard_arrow_down, color: CustomTheme.fontPurple02Color ),
                    onChanged: (String? value) {
                      // This is called when the user selects an item.
                        setState(() {
                          isExConnected = connectedExList.contains(value!);
                          marketValue = value;
                          getCoinList();
                        });
                    },
                    items: marketList.map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value, style: TextStyle(color: CustomTheme.fontPurple02Color),),
                      );
                    }).toList(),
                  )),
                )),
                Expanded(flex: 1, child: Container(child: Icon(
                  isExConnected ? Icons.check : Icons.dangerous ,
                  color: isExConnected ? Colors.greenAccent : Colors.redAccent ,
                ),))
              ],),),
              Spacer(),
              Expanded(flex: 2, child: Container(
                alignment: Alignment.center,
                padding: EdgeInsets.symmetric(horizontal: 15.0),
                decoration: BoxDecoration(color: Colors.white, border: Border.all(color: CustomTheme.fontPurple02Color), borderRadius: BorderRadius.circular(35),),
                child: coinList.length == 0 ? Container() : DropdownButtonHideUnderline(child: DropdownButton2<String>(
                  isExpanded: true,
                  value: coinValue,
                  icon: const Icon(Icons.keyboard_arrow_down, color: CustomTheme.fontPurple02Color),
                  onChanged: (String? value) {
                    // This is called when the user selects an item.
                    setState(() {
                      coinValue = value!;
                      getBobotList();
                    });
                  },
                  items: coinList.map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value, style: TextStyle(color: CustomTheme.fontPurple02Color),),
                    );
                  }).toList(),
                )),
              ),),
              Expanded(flex: 2, child: Container(
                margin:EdgeInsets.only(bottom: Constant().getViewHeight(context)/140),
                alignment: Alignment.bottomLeft,
                child: Text("Strategy", textAlign: TextAlign.left, style: TextStyle(color: CustomTheme.fontPurple02Color),),
              )),
              Expanded(flex: 2, child: Container(alignment: Alignment.center, child: Row(children: [
                Expanded(flex: 8, child: selectStrategyCell("Auto")),
                Spacer(),
                Expanded(flex: 8, child: selectStrategyCell("Bull")),
                Spacer(),
                Expanded(flex: 8, child: selectStrategyCell("Bear")),
              ])

              )),
              Expanded(flex: 2, child: Container(
                margin:EdgeInsets.only(bottom: Constant().getViewHeight(context)/140),
                alignment: Alignment.bottomLeft,
                child: Text("PositionType", textAlign: TextAlign.left, style: TextStyle(color: CustomTheme.fontPurple02Color),),
              )),
              Expanded(flex: 2, child: Container(alignment: Alignment.center, child: Row(children: [
                Expanded(flex: 9, child: positionCell("Spot")),
                Spacer(),
                Expanded(flex: 9, child: positionCell("Contract")),
              ],),)),
              Expanded(flex: 2, child: Container(
                margin:EdgeInsets.only(bottom: Constant().getViewHeight(context)/140),
                alignment: Alignment.bottomLeft,
                child: Text("Range", textAlign: TextAlign.left, style: TextStyle(color: CustomTheme.fontPurple02Color),),
              )),
              Expanded(flex: 2, child: Container(alignment: Alignment.center, child: textFieldCell(rangeString)),),
              Expanded(flex: 2, child: Container(),),
              Expanded(flex: 6, child: CreateBotBtn(
                onTap: () async {
                  ref.watch(createBotProvider(CreateBotParameter(
                      marketName: marketValue,
                      coinName: coinValue,
                      positionNum: positionTextController.text,
                      strategyType: kindList.first,
                      tradeType: tradeTypeValue,
                      lever: leverValue
                  )).future).then((event) {
                    event.then((value) {
                      if ((value).message == 'SUCCESS') {
                        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => NavigationScreen()));
                      }
                      showResponseDialog(context, "${(value).url}\n\n${(value).code}\n\n${(value).message}\n\n${(value).data}", (){});
                    });
                  });

                },
              ),),
              Expanded(flex: 6, child: Container()),
            ]))));
  }
  Widget selectStrategyCell(String item){
    bool isCurrent = strategyValue == item;
    return GestureDetector(onTap: (){
      switch(item){
        case "BullAndBear":
          if(bot1List.isEmpty)return;
          strategyValue = item;
          currentBotList = bot1List;
          break;

        case "Bull":
          if(bot2List.isEmpty)return;
          strategyValue = item;
          currentBotList = bot2List;
          break;

        case "Bear":
          if(bot3List.isEmpty)return;
          strategyValue = item;
          currentBotList = bot3List;
          break;
      }
      getBotListInfo();
      setState(() { });
    }, child: Container(
        alignment: Alignment.centerLeft,
        padding: EdgeInsets.symmetric(horizontal: 5.0),
        decoration: BoxDecoration(
          border: Border.all(color: CustomTheme.fontPurple02Color),
          borderRadius: BorderRadius.circular(35),
          color: isCurrent ? CustomTheme.fontPurple02Color : Colors.white
        ),
        child: Container(
          alignment: Alignment.center, child: Text(item , style: TextStyle(color: isCurrent ? Colors.white : CustomTheme.fontPurple02Color, fontSize: 12),),)
    ),);
  }

  Widget positionCell(String item){
    bool isCurrent = positionTypeValue == item;
    return Container(
        alignment: Alignment.centerLeft,
        padding: EdgeInsets.symmetric(horizontal: 5.0),
        decoration: BoxDecoration(
          border: Border.all(color: CustomTheme.fontPurple02Color),
          borderRadius: BorderRadius.circular(35),
          color: isCurrent ? CustomTheme.fontPurple02Color : Colors.white
        ),
        child: Container(
          alignment: Alignment.center, child: Text(item , style: TextStyle(color: isCurrent ? Colors.white : CustomTheme.fontPurple02Color, fontSize: 12),),)
    );
  }

  Widget textFieldCell(String rangeMin){
    return Container(
        alignment: Alignment.centerLeft,
        padding: EdgeInsets.symmetric(horizontal: 5.0),
        decoration: BoxDecoration(
          border: Border.all(color: CustomTheme.fontPurple02Color),
          borderRadius: BorderRadius.circular(35),
          color: Colors.white
        ),
        child: TextFormField(
          controller: positionTextController,
          textAlign: TextAlign.center,
          focusNode: positionFocusNode,
          textAlignVertical: TextAlignVertical.center,
          // inputFormatters: <TextInputFormatter>[
          //   FilteringTextInputFormatter.allow(RegExp("[0-9.]"))
          // ],
          decoration: InputDecoration(
            isDense: true,
            border: InputBorder.none,
            hintText: rangeMin,
            // suffixIcon: Icon(Icons.percent, size: Constant().getViewWidth(context)/30, color: Colors.white)
          ),
          style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 14),
          // focusNode: focusNode,
          // keyboardType: TextInputType.numberWithOptions(decimal: true)
        )
    );
  }

  void getCoinList() {
    ref.read(exCoinListProvider(marketValue).future).then((event) {
      event.then((value) {
        List<dynamic> list = (value).data;
        coinList.clear();
        list.map((exInfo) {
          coinList.add(exInfo);
        }).toList();
        coinValue = coinList.first;
        getBobotList();

        setState(() { });
      });
    });
  }

  void getRange() {
    ref.read(queryRangeProvider(QueryRangeParameter(exName: marketValue, coinName: coinValue, tradeType: tradeTypeValue)).future).then((event) {
      event.then((value) {
        // List<dynamic> list = (value).data;
        // coinList.clear();
        // list.map((exInfo) {
        //   coinList.add(exInfo);
        // }).toList();
        rangeString = (value).data["positionMin"];
        positionTextController.text = rangeString;
        setState(() { });
      });
    });
  }

  void getBobotList() {
    ref.read(createBobotListProvider(CreateBobotListParameter(exName: marketValue, coinName: coinValue)).future).then((event) {
      event.then((value) {
        List<dynamic>? auot_bb_bot_list = (value).data["Auto"];
        List<dynamic>? bull_bot_list = value.data["Bull"];
        List<dynamic>? bear_bot_list = (value).data["Bear"];
        bot1List.clear();
        bot2List.clear();
        bot3List.clear();
        auot_bb_bot_list?.map((info) {
          bot1List.add(info);
        }).toList();

        bull_bot_list?.map((info) {
          bot2List.add(info);
        }).toList();

        bear_bot_list?.map((info) {
          bot3List.add(info);
        }).toList();
        currentBotList = bot1List;
        getBotListInfo();
        setState(() { });
      });
    });
  }

  void getBotListInfo() {
    tradeTypeValue = currentBotList.first["tradeType"];
    positionTypeValue = currentBotList.first["positionType"];
    kindValue = currentBotList.first["positionType"];
    getRange();
    botValue = currentBotList.first;
    print(botValue);
  }
}
