import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import '../../constants/constants.dart';
import '../../constants/public_component.dart';
import 'bot_viewmodel.dart';

class QueryBotScreen extends ConsumerWidget {
  const QueryBotScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return buildFlexLayout(context, Container(margin: EdgeInsets.symmetric(
        horizontal: Constant().getViewWidth(context) / 20),
        child: Column(
            children: [
              Expanded(flex: 1, child: Container(),),
              Expanded(flex: 2, child: GestureDetector(
                onTap: (){
                  ref.watch(botInfoProvider('spot').future).then((event) {
                    event.then((value) {
                      showResponseDialog(context, "${(value).url}\n\n${(value).code}\n\n${(value).message}\n\n${(value).data}", (){});
                    });
                  });
                },
                child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.orangeAccent),
                  child: Text('Query Spot Bot'),
                ),
              ),),

              Expanded(flex: 1, child: Container(),),
              Expanded(flex: 2, child: GestureDetector(
                onTap: (){
                  ref.watch(botInfoProvider('contractLong').future).then((event) {
                    event.then((value) {
                      showResponseDialog(context, "${(value).url}\n\n${(value).code}\n\n${(value).message}\n\n${(value).data}", (){});
                    });
                  });
                },
                child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.orangeAccent),
                  child: Text('Query ContractLong Bot'),
                ),
              ),),

              Expanded(flex: 1, child: Container(),),
              Expanded(flex: 2, child: GestureDetector(
                onTap: (){
                  ref.watch(botInfoProvider('contractShort').future).then((event) {
                    event.then((value) {
                      showResponseDialog(context, "${(value).url}\n\n${(value).code}\n\n${(value).message}\n\n${(value).data}", (){});
                    });
                  });
                },
                child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.orangeAccent),
                  child: Text('Query ContractShort Bot'),
                ),
              ),),

              Expanded(flex: 1, child: Container(),),
              Expanded(flex: 2, child: GestureDetector(
                onTap: (){
                  ref.watch(botInfoProvider('contractAuto').future).then((event) {
                    event.then((value) {
                      showResponseDialog(context, "${(value).url}\n\n${(value).code}\n\n${(value).message}\n\n${(value).data}", (){});
                    });
                  });
                },
                child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.orangeAccent),
                  child: Text('Query ContractAuto Bot'),
                ),
              ),),

              Expanded(flex: 1, child: Container(),),
              Expanded(flex: 2, child: GestureDetector(
                onTap: (){
                  ref.watch(botInfoProvider('contractAutoHighSpeed').future).then((event) {
                    event.then((value) {
                      showResponseDialog(context, "${(value).url}\n\n${(value).code}\n\n${(value).message}\n\n${(value).data}", (){});
                    });
                  });
                },
                child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.orangeAccent),
                  child: Text('Query ContractAutoHighSpeed Bot'),
                ),
              ),),
              Expanded(flex: 10, child: Container(),),
            ])));
  }
}
