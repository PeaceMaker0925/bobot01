class IncomeInfo {
  String income;
  String dealTime;
  String tradeType;
  String symbol;
  String incomeRate;

  IncomeInfo({
    required this.income,
    required this.dealTime,
    required this.tradeType,
    required this.symbol,
    required this.incomeRate,
  });
}

class ReferralInfo {
  String logoUrl;
  String symbolName;
  String symbol;
  String price;
  String priceChangePercent;

  ReferralInfo({
    required this.logoUrl,
    required this.symbolName,
    required this.symbol,
    required this.price,
    required this.priceChangePercent,
  });
}