import 'package:equatable/equatable.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../repository/account_api_repository.dart';
import '../../../repository/api_response.dart';

final assetRecordProvider = FutureProvider.autoDispose.family<Future<ApiResponse>, AssetRecordParameter>((ref, parameter) async {
  return AccountApiRepositoryImpl().queryAssetRecord(parameter);
});

final assetInfoProvider = FutureProvider.autoDispose<Future<ApiResponse>>((ref) async {
  return AccountApiRepositoryImpl().queryAssetInfo();
});

final queryWithdrawInfoProvider = FutureProvider.autoDispose<Future<ApiResponse>>((ref) async {
  return AccountApiRepositoryImpl().queryWithdrawInfo();
});

final queryTransationTypeProvider = FutureProvider.autoDispose<Future<ApiResponse>>((ref) async {
  return AccountApiRepositoryImpl().queryTransationType();
});

final queryDepositInfoProvider = FutureProvider.autoDispose<Future<ApiResponse>>((ref) async {
  return AccountApiRepositoryImpl().queryDepositInfo();
});

final sendProvider = FutureProvider.autoDispose.family<Future<ApiResponse>, AssetWithdrawParameter>((ref, parameter) async {
  return AccountApiRepositoryImpl().assetWithdraw(parameter);
});

final transferProvider = FutureProvider.autoDispose.family<Future<ApiResponse>, String>((ref, parameter) async {
  return AccountApiRepositoryImpl().assetTransfer(parameter);
});

final innerTransferProvider = FutureProvider.autoDispose.family<Future<ApiResponse>, InnerTransferParameter>((ref, parameter) async {
  return AccountApiRepositoryImpl().assetInnerTransfer(parameter);
});

final assetSendVerifyCodeProvider = FutureProvider.autoDispose.family<Future<ApiResponse>, String>((ref, parameter) async {
  return AccountApiRepositoryImpl().assetSendVerifyCode(parameter);
});

final assetInnerTransferVerifyCodeProvider = FutureProvider.autoDispose.family<Future<ApiResponse>, String>((ref, parameter) async {
  return AccountApiRepositoryImpl().assetInnerTransferVerifyCode(parameter);
});

class AssetRecordParameter extends Equatable {
  final String startTime;
  final String endTime;
  final String walletType;
  final String positionType;

  const AssetRecordParameter({
    required this.startTime,
    required this.endTime,
    required this.walletType,
    required this.positionType,
  });

  @override
  List<Object?> get props => [startTime, endTime, walletType, positionType];
}

// class AssetWithdrawParameter extends Equatable {
//   final String startTime;
//   final String endTime;
//   final String walletType;
//   final String positionType;
//
//   const AssetWithdrawParameter({
//     required this.startTime,
//     required this.endTime,
//     required this.walletType,
//     required this.positionType,
//   });
//
//   @override
//   List<Object?> get props => [startTime, endTime, walletType, positionType];
// }

class InnerTransferParameter extends Equatable {
  final String unit;
  final String chainName;
  final String walletType;
  final String payee;
  final String amount;
  final String password;
  final String verifyCode;
  final String? code;

  const InnerTransferParameter(this.code,{
    required this.unit,
    required this.chainName,
    required this.walletType,
    required this.payee,
    required this.amount,
    required this.password,
    required this.verifyCode,
  });

  @override
  List<Object?> get props => [unit, chainName, walletType, payee, amount, password, verifyCode, code,];
}

class AssetWithdrawParameter extends Equatable {
  final String unit;
  final String chainName;
  final String fee;
  final String address;
  final String arrivedAmt;
  final String amount;
  final String password;
  final String verifyCode;
  final String? code;

  const AssetWithdrawParameter(this.code, {
    required this.unit,
    required this.chainName,
    required this.fee,
    required this.address,
    required this.amount,
    required this.arrivedAmt,
    required this.password,
    required this.verifyCode,
  });

  @override
  List<Object?> get props => [unit, chainName, address, fee, amount, password, verifyCode, code, arrivedAmt];
}
