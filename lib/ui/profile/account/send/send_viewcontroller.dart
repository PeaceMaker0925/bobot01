import 'package:bobot01/constants/constants.dart';
import 'package:bobot01/constants/lightCell.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../constants/SignInWidgets.dart';
import '../../../../constants/public_component.dart';
import '../../../../constants/shared_preference.dart';
import '../../../../constants/theme.dart';
import '../../../../constants/width_infinty_button.dart';
import '../account_viewmodel.dart';


class ProfileSendScreen extends ConsumerStatefulWidget {
  const ProfileSendScreen({Key? key}) : super(key: key);
  @override
  ConsumerState<ProfileSendScreen> createState() => _ProfileSendState();
}

class _ProfileSendState extends ConsumerState<ProfileSendScreen> {
  late TextEditingController addressTextController;
  late FocusNode addressFocusNode;
  late TextEditingController amountTextController;
  TextEditingController pwTextController = TextEditingController();
  TextEditingController verifyCodeTextController = TextEditingController();
  late FocusNode amountFocusNode;
  bool leftBtn = true;
  bool rightBtn = false;
  String balanceValue = '123,456.00';
  String referralValue = '123,456.00';
  String feeValue = '3';
  String amountSendValue = '50';
  String currentAccount = '';
  bool isPasswordShow = true;
  bool isVerifyCodeWrong = false;

  @override
  void initState() {
    addressTextController = TextEditingController();
    addressFocusNode = FocusNode();
    amountTextController = TextEditingController();
    amountFocusNode = FocusNode();


    amountTextController.text = amountSendValue;
    amountTextController.addListener(() {
      amountSendValue = amountTextController.text;
      setState(() {});
    });

    BobotSharedPreferences().getCurrentAccount().then((value) {
      setState(() {
        currentAccount = value;
      });
    });

    ref.read(assetInfoProvider.future).then((event) {
      event.then((value) {
        List<dynamic> list = value.data;
        balanceValue = list.first["balance"];
        // referralValue = list.last["balance"];
        // todayProfit = value.data['todayIncome'];
        // lastDayProfit = value.data['yesterdayIncome'];
        setState(() {});
      });
    });

    //
    ref.read(queryWithdrawInfoProvider.future).then((event) {
      event.then((value) {
        List<dynamic> list = value.data["coin"];
        feeValue = list.first["fee"];
        // referralValue = list.last["balance"];
        // todayProfit = value.data['todayIncome'];
        // lastDayProfit = value.data['yesterdayIncome'];
        setState(() {});
      });
    });

    super.initState();
  }

  @override
  void dispose() {
    addressTextController.dispose();
    addressFocusNode.dispose();
    amountTextController = TextEditingController();
    amountFocusNode = FocusNode();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return buildFlexBodyLayout(context, InkWell(onTap: () { addressFocusNode.unfocus(); amountFocusNode.unfocus(); },
      child: Container(margin: EdgeInsets.symmetric(
          horizontal: Constant().getViewWidth(context) / 20),
          child: Column(
              children: [
                buildAppBar(title: "Send", ref: ref, backPage: 9),
                Padding(padding: EdgeInsets.symmetric(horizontal: 20), child: Column(children: [
                  SizedBox(height: 20),
                  Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                    Text('Energy Account', style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 14, fontWeight: FontWeight.w600)),
                    Text(balanceValue, style: TextStyle(color: CustomTheme.fontPurple01Color, fontSize: 20, fontWeight: FontWeight.w600)),
                  ]),
                  SizedBox(height: 20),
                  Align(alignment: Alignment.centerLeft, child: Text('選擇鏈類型', style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 16))),
                  SizedBox(height: 5),
                  Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(child: _chainTypeBtn('USDT (TRC20)', leftBtn)),
                        SizedBox(width: 10),
                        Expanded(child: _chainTypeBtn('USDT (ERC20)', rightBtn))
                      ]),
                  SizedBox(height: 20),
                  Align(alignment: Alignment.centerLeft, child: Text('發送地址', style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 16))),
                  _buildTextField(context, addressTextController, addressFocusNode, '請輸入發送地址', false),
                  Align(alignment: Alignment.centerLeft, child: Text('請輸入正確地址', style: TextStyle(color: CustomTheme.fontRed01Color, fontSize: 14))),
                  SizedBox(height: 10),
                  Align(alignment: Alignment.centerLeft, child: Text('發送數量', style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 16))),
                  SizedBox(height: 5),
                  Text('能量帳戶若低於 <後台設定>，機器人將暫停量化。為確保機器人正常量化，建議能量帳戶保持100 USDT以上。', style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 11)),
                  _buildTextField(context, amountTextController, amountFocusNode, '最小發送數量為50', true),
                  Align(alignment: Alignment.centerLeft, child: Text('小於限額！請輸入大於50的數字', style: TextStyle(color: CustomTheme.fontRed01Color, fontSize: 14))),
                  SizedBox(height: 20),
                  Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                    Text('服務費', style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 14, fontWeight: FontWeight.w600)),
                    Text(feeValue, style: TextStyle(color: CustomTheme.fontPurple01Color, fontSize: 14, fontWeight: FontWeight.w600)),
                  ]),
                  SizedBox(height: 20),
                  Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                    Text('到帳數量', style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 14, fontWeight: FontWeight.w600)),
                    Text("${int.parse(amountSendValue)-int.parse(feeValue)}", style: TextStyle(color: CustomTheme.fontPurple01Color, fontSize: 20, fontWeight: FontWeight.w600)),
                  ]),
                ])),
                Expanded(child: Align(alignment: Alignment.centerLeft, child: Opacity(opacity: 0.5,
                  child: Image.asset('assets/img/img_btn_send_01.png'),))),
                buildConfirmBtn(context, 'Confirm', () => {
                    showDialog(context: context, builder: (_) =>  AlertDialog(
                      backgroundColor: Colors.transparent,
                      content: Container(
                        alignment: Alignment.center,
                        height: Constant().getViewHeight(context)/2,
                        // width: Constant().getViewWidth(context) * 0.8,
                        child: VerifyDialog(),),
                    ))
                })
              ]))));
  }

  Widget _buildTextField(BuildContext context, TextEditingController controller, FocusNode focusNode, String hint, bool suffixBtnEnable) {
    return Container(
        width: double.maxFinite,
        color: Colors.transparent,
        child: Container(
          height: 40,
          child: TextFormField(
              onTap: () {  }, controller: controller, decoration: InputDecoration(
            suffixIcon: (suffixBtnEnable)
                ? TextButton(onPressed: () {}, child: Text('MAX', style: TextStyle(color: CustomTheme.secondBlueColor)))
                : null,
            border: InputBorder.none,
//            icon: Icon(Icons.search_rounded, color: CustomTheme.labelGrayColor),
            hintText: hint, hintStyle: TextStyle(color: CustomTheme.labelGrayColor)),
            style: TextStyle(color: CustomTheme.fontPurple02Color), focusNode: focusNode),
          alignment: Alignment.center,
          padding: EdgeInsets.symmetric(horizontal: Constant().getViewWidth(context)/50),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              border: Border.all(width: 1, color: CustomTheme.fontPurple02Color),
              color: CustomTheme.labelWhiteColor
          ),
        )
    );
  }

  Widget _chainTypeBtn(String title, bool btnEnable) {
    return GestureDetector( onTap: () { /*leftBtn = boolToggle(leftBtn); rightBtn = boolToggle(rightBtn); setState((){});*/ },
      child: Container(
        height: 30,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            border: Border.all(width: 1, color: CustomTheme.fontPurple02Color),
            color: (btnEnable) ? CustomTheme.fontPurple02Color : CustomTheme.labelWhiteColor
        ),
        child: Align(alignment: Alignment.center,
          child: Text(title, style: TextStyle(color: (btnEnable) ? CustomTheme.labelWhiteColor : CustomTheme.fontPurple02Color, fontSize: 12))),
      ),
    );
  }
  Widget VerifyDialog(){
    FocusNode focusNode = FocusNode();
    return lightCell(isBar: true, Container(
      margin: EdgeInsets.symmetric(horizontal: Constant().getViewWidth(context)/40),
      child: Column(
          children: [
            Spacer(),
            Expanded(flex: 1, child: Container(alignment: Alignment.center, child: Text("輸入密碼完成發送", style: CustomTheme.textPrimaryPurple02,),)),

            Expanded(flex: 1, child: Container(alignment: Alignment.centerLeft, child: Text("請輸入密碼", style: CustomTheme.textPrimaryPurple02,),)),
            Expanded(flex: 2, child: SignInTextField(pwTextController, GestureDetector(child: Container(
              child: Icon(isPasswordShow ? Icons.remove_red_eye_outlined : Icons.visibility_off_outlined, color: CustomTheme.fontPurple01Color, size: 30,),
            ), onTap: (){ setState(() {
              // isPasswordShow = !isPasswordShow;
            }); },), passwordVisible: false, focusNode, "Please_enter_your_password")),
            Expanded(flex: 1, child: Container(alignment: Alignment.centerLeft, child: Text("信箱驗證碼", style: CustomTheme.textPrimaryPurple02,),)),
            Expanded(flex: 2, child: SignInTextField(verifyCodeTextController, GestureDetector(child: Container(
              margin: EdgeInsets.all(Constant().getViewHeight(context)/120),
              padding: EdgeInsets.all(Constant().getViewHeight(context)/120),
              decoration: BoxDecoration(
                color: CustomTheme.fontPurple01Color,
                borderRadius: BorderRadius.all(Radius.circular(35)),
              ),
              child: Text("Send", style: CustomTheme.textPrimary,),
            ), onTap: (){ sendVerifyCode(); },), focusNode, "Please_enter_email_verify_code")),
            Expanded(flex: 2, child: Container(
              margin: EdgeInsets.symmetric(horizontal: Constant().getViewHeight(context)/40),
              child: Row(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.start, children: [if(isVerifyCodeWrong)Text('VerifyCodeError', style: TextStyle(
                color: CustomTheme.fontRed01Color,)),],),
            )),
            Expanded(flex: 2, child: WidthInfintyButton(
              onTap: () { pressSendBtn(); },
              fontColor: Colors.white,
              foregroundColor: CustomTheme.fontPurple01Color,
              fontSize: 18,
              text: "Continue",
            ),),
            Spacer()
          ]
      ),));
  }
  void sendVerifyCode() {
    ref.read(assetSendVerifyCodeProvider(currentAccount).future);
  }
  void pressSendBtn() {
    ref.read(sendProvider(AssetWithdrawParameter(
        unit: "USDT",
        chainName: "TRON(TRC20)",
        fee: feeValue,
        address: addressTextController.text,
        amount: amountSendValue,
        arrivedAmt: "${int.parse(amountSendValue)-int.parse(feeValue)}",
        password: pwTextController.text,
        verifyCode: verifyCodeTextController.text,
        null
    )).future).then((event) {
      event.then((value) {
        showResponseDialog(context, "${(value).url}\n\n${(value).code}\n\n${(value).message}\n\n${(value).data}", (){});
      });
    });
  }
}
