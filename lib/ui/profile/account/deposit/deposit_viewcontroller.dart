import 'package:bobot01/constants/constants.dart';
import 'package:bobot01/constants/public_component.dart';
import 'package:bobot01/ui/bot/bot_viewcontroller.dart';
import 'package:bobot01/ui/sign_in/login_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../constants/theme.dart';
import '../account_viewmodel.dart';


class ProfileDepositScreen extends ConsumerStatefulWidget {
  const ProfileDepositScreen({Key? key}) : super(key: key);
  @override
  ConsumerState<ProfileDepositScreen> createState() => _ProfileDepositState();
}

class _ProfileDepositState extends ConsumerState<ProfileDepositScreen> with TickerProviderStateMixin{
  String USDTaddress = "";
  List<Tab> _depositTabs = [Tab(text: 'USDT (TRC20)'), Tab(text: 'USDT (ERC20)')];
  late TabController _depositTabController = TabController(length: _depositTabs.length, vsync: this);

  @override
  void initState() {
    super.initState();
    ref.read(queryDepositInfoProvider.future).then((event) {
      event.then((value) {
        List<dynamic> list = value.data["coin"];
        USDTaddress = list.first["address"];
        // referralValue = list.last["balance"];
        // todayProfit = value.data['todayIncome'];
        // lastDayProfit = value.data['yesterdayIncome'];
        setState(() {});
      });
    });


  }

  @override
  void dispose() {
    _depositTabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return buildFlexBodyLayout(context, Container(margin: EdgeInsets.symmetric(
        horizontal: Constant().getViewWidth(context) / 20),
        child: Column(
            children: [
              buildAppBar(title: "Deposit", ref: ref, backPage: 9),
              buildTabBar(context),
              Divider(height: 0, thickness: 1, color: CustomTheme.labelGrayColor, indent: 15, endIndent: 15),
              Expanded(child: TabBarView(physics: NeverScrollableScrollPhysics(),controller: _depositTabController, children: [
                _buildLeftTab(),
                _buildRightTab(),
              ]))
            ])));
  }

  Widget _buildLeftTab() {
    return Padding(padding: EdgeInsets.symmetric(horizontal: 20),
      child: Column(children: [
        SizedBox(height: 30),
        Padding(padding: EdgeInsets.symmetric(horizontal: Constant().getViewWidth(context)/10), child: Image.asset('assets/img/img_qrcode_card.png')),
        SizedBox(height: 10),
        Text('此地址只可接收USDT', style: TextStyle(color: CustomTheme.secondBlueColor, fontSize: 16)),
        SizedBox(height: 10),
        GestureDetector(onTap: () {},
            child: Container(
              height: 40,
              width: double.maxFinite,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: CustomTheme.fontPurple01Color
              ),
              child: Align(alignment: Alignment.center, child: Text('儲存圖片', style: TextStyle(color: CustomTheme.labelWhiteColor))),
            )),
        SizedBox(height: 20),
        Align(alignment: Alignment.centerLeft, child: Text('USDT接收地址', style: TextStyle(color: CustomTheme.secondBlueColor, fontSize: 16))),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 10),
          height: 50,
          decoration: BoxDecoration(
              image: DecorationImage(image: AssetImage('assets/img/img_bar_bg_01.png'), fit: BoxFit.fill)),
          child: Row(children: [
            Align(alignment: Alignment.centerLeft, child: Text(USDTaddress, style: TextStyle(color: CustomTheme.secondBlueColor, fontSize: 13))),
            SizedBox(width: 15),
            Expanded(child: Image.asset('assets/btn/btn_edit_01_nor.png', height: Constant().getViewHeight(context)/30,)),
          ])),
        SizedBox(height: 10),
        Expanded(child: _buildColumText())
      ]));
  }

  Widget _buildColumText() {
    return GridView.count(crossAxisCount: 1, childAspectRatio: 5, children: warningTextList().map((warningText) {
      return Row(children: [
        Align(alignment: Alignment.center, child: Image.asset('assets/icon/icon_warningtext_01.png')),
        SizedBox(width: 10),
        Expanded(child: Text('${warningText}'))
      ]);
    }).toList());
  }

  Widget _buildRightTab() {
    return Padding(padding: EdgeInsets.symmetric(horizontal: 20),
      child: Column(children: [
        SizedBox(height: 30),
        Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          Text('網路', style: TextStyle(color: CustomTheme.secondBlueColor, fontSize: 16)),
          Text('Ethereum (ERC20)', style: TextStyle(color: CustomTheme.secondBlueColor, fontSize: 16)),
        ]),
        SizedBox(height: 20),
        Align(alignment: Alignment.centerLeft, child: Text('USDT 接收地址', style: TextStyle(color: CustomTheme.secondBlueColor, fontSize: 16))),
        Container(
            padding: EdgeInsets.symmetric(horizontal: 10),
            height: 50,
            decoration: BoxDecoration(
                image: DecorationImage(image: AssetImage('assets/img/img_bar_bg_01.png'), fit: BoxFit.fill)),
            child: Row(children: [
              Align(alignment: Alignment.centerLeft, child: Text('TTBn6JoYsNQe4gD5obr...A6ZoBZ427', style: TextStyle(color: CustomTheme.secondBlueColor, fontSize: 13))),
              SizedBox(width: 15),
              Expanded(child: Image.asset('assets/btn/btn_edit_01_nor.png')),
            ])),
        SizedBox(height: 20),
        Expanded(child: _buildColumText())
      ]));
  }

  Widget buildTabBar(BuildContext context) {
    return Consumer(builder: (context, ref, _) {
      return Container(width: Constant().getViewWidth(context), child: TabBar(
        indicatorColor: CustomTheme.fontPurple02Color,
        indicatorSize: TabBarIndicatorSize.tab,
        indicatorWeight: 4,
        labelColor: CustomTheme.fontPurple02Color,
        labelStyle: CustomTheme.textPrimary,
        labelPadding: EdgeInsets.zero,
        unselectedLabelColor: CustomTheme.labelGrayColor,
        padding: EdgeInsets.symmetric(horizontal: Constant().getViewWidth(context)/30),
        controller: _depositTabController,
        tabs: _depositTabs,
        physics: NeverScrollableScrollPhysics(),
        onTap: (index) {
          if(index == 1 ){
            _depositTabController.index = _depositTabController.previousIndex;
            return;
          }
          /*_depositTabController.animateTo(index);*/},
      ));
    });
  }

}