import 'package:bobot01/constants/constants.dart';
import 'package:bobot01/constants/public_component.dart';
import 'package:bobot01/constants/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../account_viewmodel.dart';


class ProfileTransferScreen extends ConsumerStatefulWidget {
  const ProfileTransferScreen({Key? key}) : super(key: key);
  @override
  ConsumerState<ProfileTransferScreen> createState() => _ProfileTransferState();
}

class _ProfileTransferState extends ConsumerState<ProfileTransferScreen> {

  late TextEditingController transferTextController;
  late FocusNode transferFocusNode;
  String balanceValue = '123,456.00';
  String referralValue = '123,456.00';

  @override
  void initState() {
    transferTextController = TextEditingController();
    transferFocusNode = FocusNode();
    super.initState();
    ref.read(assetInfoProvider.future).then((event) {
      event.then((value) {
        List<dynamic> list = value.data;
        balanceValue = list.first["balance"];
        referralValue = list.last["balance"];
        // todayProfit = value.data['todayIncome'];
        // lastDayProfit = value.data['yesterdayIncome'];
        setState(() {});
      });
    });
  }

  @override
  void dispose() {
    transferTextController = TextEditingController();
    transferFocusNode = FocusNode();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return buildFlexBodyLayout(context, Container(margin: EdgeInsets.symmetric(
        horizontal: Constant().getViewWidth(context) / 20),
        child: Container(//padding: EdgeInsets.symmetric(horizontal: 20),
          child: Column(
              children: [
                buildAppBar(title: "Transfer", ref: ref, backPage: 9),
                SizedBox(height: 30),
                Text('Referral Account', style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 12, fontWeight: FontWeight.w600)),
                SizedBox(height: 10),
                Text(referralValue, style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 18, fontWeight: FontWeight.w600)),
                SizedBox(height: 30),
                Image.asset('assets/icon/icon_vector_01.png', height: 20),
                SizedBox(height: 10),
                Text('Energy Account', style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 12, fontWeight: FontWeight.w600)),
                SizedBox(height: 10),
                Text(balanceValue, style: TextStyle(color: CustomTheme.fontPurple01Color, fontSize: 23, fontWeight: FontWeight.w600)),
                SizedBox(height: 30),
                Align(alignment: Alignment.centerLeft, child: Text('劃轉數量', style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 12, fontWeight: FontWeight.w600))),
                _buildTextField(context, transferTextController, transferFocusNode, '請輸入劃轉數量', true),
                Align(alignment: Alignment.centerLeft, child: Text('餘額不足', style: TextStyle(color: CustomTheme.fontRed01Color, fontSize: 14))),
                Expanded(child: Align(alignment: Alignment.bottomCenter, child: Opacity(opacity: 0.5,
                  child: Image.asset('assets/img/img_btn_transfer_01.png'),))),
                buildConfirmBtn(context, 'Confirm', () => {
                  ref.watch(transferProvider(transferTextController.text).future).then((event) {
                    event.then((value) {
                    showResponseDialog(context, "${(value).url}\n\n${(value).code}\n\n${(value).message}\n\n${(value).data}", (){});
                    });
                  })
                })
              ]))));
  }

  Widget _buildTextField(BuildContext context, TextEditingController controller, FocusNode focusNode, String hint, bool suffixBtnEnable) {
    return Container(
        width: double.maxFinite,
        color: Colors.transparent,
        child: Container(
          height: 40,
          child: TextFormField(onTap: () {  }, controller: controller, decoration: InputDecoration(
              suffixIcon: (suffixBtnEnable)
                  ? TextButton(onPressed: () {}, child: Text('MAX', style: TextStyle(color: CustomTheme.secondBlueColor)))
                  : null,
              border: InputBorder.none,
//            icon: Icon(Icons.search_rounded, color: CustomTheme.labelGrayColor),
              hintText: hint, hintStyle: TextStyle(color: CustomTheme.labelGrayColor)),
              style: TextStyle(color: CustomTheme.fontPurple02Color), focusNode: focusNode),
          alignment: Alignment.center,
          padding: EdgeInsets.symmetric(horizontal: Constant().getViewWidth(context)/50),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              border: Border.all(width: 1, color: CustomTheme.fontPurple02Color),
              color: CustomTheme.labelWhiteColor
          ),
        )
    );
  }
}