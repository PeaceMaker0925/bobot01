import 'package:bobot01/constants/constants.dart';
import 'package:bobot01/ui/bot/bot_model/account_model.dart';
import 'package:bobot01/ui/sign_in/login_viewmodel.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../constants/public_component.dart';
import '../../../constants/theme.dart';
import '../profile_viewcontroller.dart';
import 'account_viewmodel.dart';


class ProfileAccountScreen extends ConsumerStatefulWidget {
  const ProfileAccountScreen({Key? key}) : super(key: key);
  @override
  ConsumerState<ProfileAccountScreen> createState() => _ProfileAccountScreenState();
}

class _ProfileAccountScreenState extends ConsumerState<ProfileAccountScreen> {
  String balanceValue = '123,456.00';
  String referralValue = '123,456.00';
  String proxyValue = 'AGENT';
  String positionValue = 'Contract';
  DateTime selectedDate = DateTime(2022);
  List<String> proxyList = ['AGENT','ENERGY'];
  List<String> agentTransList = List.empty(growable: true);
  List<String> energyTransList = List.empty(growable: true);
  List<String> currentList = List.empty(growable: true);
  List<AccountGridViewInfo> gridViewList = [
    AccountGridViewInfo(accountType: AccountGridViewType1.Energy, Type: AccountGridViewType2.Receive, price: 300),
    AccountGridViewInfo(accountType: AccountGridViewType1.Referral, Type: AccountGridViewType2.Transfer, price: -150),
    AccountGridViewInfo(accountType: AccountGridViewType1.Referral, Type: AccountGridViewType2.Transfer, price: -500),
  ];


  @override
  void initState() {
    ref.read(assetInfoProvider.future).then((event) {
      event.then((value) {
        List<dynamic> list = value.data;
        balanceValue = list.first["balance"];
        referralValue = list.last["balance"];
        // todayProfit = value.data['todayIncome'];
        // lastDayProfit = value.data['yesterdayIncome'];
        setState(() {});
      });
    });

    ref.read(queryTransationTypeProvider.future).then((event) {
      event.then((value) {
        // print(value.data["AGENT"]);
        // agentTransList = value.data["AGENT"];
        List<dynamic> list01 = value.data["AGENT"];
        list01.map((e) => {
          agentTransList.add(e.toString()),
          positionValue = agentTransList.first,
          currentList = agentTransList
        }).toList();

        List<dynamic> list02 = value.data["ENERGY"];
        list02.map((e) => {
          energyTransList.add(e)
        }).toList();

        setState(() {});
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    List<ProfileCellListInfo> accountCellList = [
      ProfileCellListInfo(title: 'Deposit', imagePath: 'assets/img/img_btn_deposit_f_01.png', action: () {ref.read(bodyIndexProvider.notifier).setBodyIndex(12);} ),
      ProfileCellListInfo(title: 'Send',  imagePath: 'assets/img/img_btn_send_01.png', action: () {ref.read(bodyIndexProvider.notifier).setBodyIndex(13);} ),
      ProfileCellListInfo(title: 'Transfer',  imagePath: 'assets/img/img_btn_transfer_01.png', action: () {ref.read(bodyIndexProvider.notifier).setBodyIndex(14);} ),
      ProfileCellListInfo(title: 'Internal',  imagePath: 'assets/img/img_btn_internal_01.png', action: () {ref.read(bodyIndexProvider.notifier).setBodyIndex(15);} ),
    ];

    return buildFlexBodyLayout(context, Container(
        margin: EdgeInsets.symmetric(horizontal: Constant().getViewWidth(context) / 20),
        child: Column(
            children: [
              buildAppBar(title: "Account", ref: ref, backPage: 4),
              Expanded(flex: 4  , child: Container(alignment: Alignment.center, color: Colors.transparent, child: Column(children: [
                Expanded(flex: 2, child: CircleAvatar(
                  backgroundColor: CustomTheme.fontPurple01Color,
                  radius: 40,
                )),
                Container(width: 10, height: 10, color: CustomTheme.fontPurple01Color,),
                Expanded(flex: 14, child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: CustomTheme.fontPurple01Color,
                    borderRadius: BorderRadius.all(Radius.circular(35)),
                  ),
                  child: AccountBotContent(),
                ))
              ],),)),
              Expanded(flex: 2, child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: accountCellList.asMap().entries.map((info) => AccountCell(info)).toList())),
              Expanded(flex: 1, child: Row(children: [
                Expanded(child: _buildProxyAccountDropBtn()),
                SizedBox(width: 10) ,
                Expanded(child: _buildSelectTypeDropBtn())])),
              Expanded(flex: 1, child: Row(children: [
                Expanded(child: _buildSelectTimeZoneDropBtn()),
                SizedBox(width: 10),
                GestureDetector(child: Image.asset('assets/btn/button_search_01.png', height: 45),onTap: (){
                  var pp = AssetRecordParameter(
                      startTime: "${selectedDate.year}-${addZero(selectedDate.month)}-${addZero(selectedDate.day)} 00:00",
                      endTime: "${selectedDate.year}-${addZero(selectedDate.month)}-${addZero(selectedDate.day+1)} 00:00",
                      walletType: proxyValue,
                      positionType: positionValue
                  );
                  print(pp);
                  ref.watch(assetRecordProvider(pp).future).then((event) {
                    event.then((value) {
                      showResponseDialog(context, "${(value).url}\n\n${(value).code}\n\n${(value).message}\n\n${(value).data}", (){});
                    });
                  });
                },)])),
              SizedBox(height: 10),
              Divider(height: 1, color: CustomTheme.secondGrayColor,),
              Expanded(flex: 1, child: Row(children: [
                Text('Account Type', style: TextStyle(color: CustomTheme.secondBlueColor, fontSize: 14)),
                Expanded(flex:2, child: Text('Type', style: TextStyle(color: CustomTheme.secondBlueColor, fontSize: 14), textAlign: TextAlign.center,)),
                Text('Price (USDT))', style: TextStyle(color: CustomTheme.secondBlueColor, fontSize: 14)),
                Spacer()
              ])),
              Expanded(flex:4, child: _buildAccountGridView()),
              Expanded(flex:2, child: Container()),
            ])));
  }

  Widget _buildAccountGridView(){
    return GridView.count(crossAxisCount: 1, childAspectRatio: 5,
      children: gridViewList.map((info) => _buildAccountGridViewCard(context, info)).toList());
  }

  Widget _buildAccountGridViewCard(BuildContext context, AccountGridViewInfo info) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 5),
      decoration: BoxDecoration(
        image: DecorationImage(image: AssetImage('assets/img/img_card_04.png'), fit: BoxFit.fill),
      ),
      padding: EdgeInsets.all(12),
      child: Row(children: [
        Expanded(child: Container(
          height: double.maxFinite,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            border: Border.all(width: 1, color: CustomTheme.fontPurple01Color)
          ),
          child: Align(alignment: Alignment.center, child: Text(info.accountType.name, style: TextStyle(color: CustomTheme.fontPurple01Color, fontSize: 18))),
        )),
        Expanded(child: Align(alignment: Alignment.center, child: Text(info.Type.name, style: TextStyle(color: CustomTheme.secondBlueColor, fontSize: 18)))),
        Expanded(child: _buildPrice(info.price)),
        Image.asset('assets/btn/btn_detailed_01_nor.png')
    ]));
  }

  Widget _buildPrice(double price) {
    if (price.isNegative) {
      return  Container(
        height: double.maxFinite,
        decoration: BoxDecoration(
          color: CustomTheme.fontRed01Color,
          borderRadius: BorderRadius.circular(10),
        ),
        child: Align(alignment: Alignment.center,
          child: Text('${price}', style: TextStyle(color: CustomTheme.labelWhiteColor, fontSize: 18), maxLines: 1, overflow: TextOverflow.ellipsis)),
      );
    } else {
      return  Container(
        height: double.maxFinite,
        decoration: BoxDecoration(
          color: CustomTheme.fontGreen01Color,
          borderRadius: BorderRadius.circular(10),
        ),
        child: Align(alignment: Alignment.center,
          child: Text('+${price}', style: TextStyle(color: CustomTheme.labelWhiteColor, fontSize: 18), maxLines: 1, overflow: TextOverflow.ellipsis)),
      );
    }
  }

  Widget AccountCell(MapEntry<int, ProfileCellListInfo> info) {
    Widget cell = Container();
    switch (info.key) {
      case 0: {
        cell = buildAccountCellBtn(title: info.value.title, imageString: info.value.imagePath, onTap: () { info.value.action(); });
        break;
      }
      case 1: {
        cell = buildAccountCellBtn(title: info.value.title, imageString: info.value.imagePath, onTap: () { info.value.action(); });
        break;
      }
      case 2: {
        cell = buildAccountCellBtn(title: info.value.title, imageString: info.value.imagePath, onTap: () { info.value.action(); });
        break;
      }
      case 3: {
        cell = buildAccountCellBtn(title: info.value.title, imageString: info.value.imagePath, onTap: () { info.value.action(); });
        break;
      }
    }
    return Container(alignment: Alignment.center, child: cell);
  }

  Widget _buildProxyAccountDropBtn(){
    return Container(
      margin: EdgeInsets.symmetric(vertical: 0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        border: Border.all(width: 1, color: CustomTheme.fontPurple02Color)
      ),
      child: DropdownButtonHideUnderline(child: DropdownButton2<String>(
        buttonHeight: 35,
        buttonPadding: EdgeInsets.symmetric(horizontal: 10),
        isExpanded: true,
        value: proxyValue,
        icon: const Icon(Icons.keyboard_arrow_down, color: CustomTheme.fontPurple02Color ),
        onChanged: (String? value) {
          // This is called when the user selects an item.
          setState(() {
            proxyValue = value!;
            currentList = value == "AGENT" ? agentTransList : energyTransList;
            positionValue = value == "AGENT" ? agentTransList.first : energyTransList.first;
          });
        },
        items: proxyList.map<DropdownMenuItem<String>>((String value) {
          return DropdownMenuItem<String>(
            value: value,
            child: Text(value, style: TextStyle(color: CustomTheme.fontPurple02Color),),
          );
        }).toList(),
      )),
    );
  }

  Widget _buildSelectTypeDropBtn(){
    return Container(
      margin: EdgeInsets.symmetric(vertical: 0),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          border: Border.all(width: 1, color: CustomTheme.fontPurple02Color)
      ),
      child: DropdownButtonHideUnderline(child: DropdownButton2<String>(
        buttonHeight: 35,
        value: positionValue,
        hint: Text('請選擇類型', style: TextStyle(color: CustomTheme.primaryGrayColor, fontSize: 16)),
        buttonPadding: EdgeInsets.symmetric(horizontal: 10),
        isExpanded: true,
        // value: ,
        icon: const Icon(Icons.keyboard_arrow_down, color: CustomTheme.fontPurple02Color ),
        onChanged: (String? value) {
          // This is called when the user selects an item.
          setState(() { positionValue = value!; });
        },
        items: currentList.map<DropdownMenuItem<String>>((String value) {
          return DropdownMenuItem<String>(
            value: value,
            child: Text(value, style: TextStyle(color: CustomTheme.fontPurple02Color),),
          );
        }).toList(),
      )),
    );
  }

  Widget _buildSelectTimeZoneDropBtn(){
    return GestureDetector(onTap: (){
      DatePicker.showDatePicker(context,
          showTitleActions: true,
          minTime: DateTime(2018, 1, 1),
          maxTime: DateTime(2026, 12, 31),
          theme: DatePickerTheme(
              headerColor: CustomTheme.fontPurple01Color,
              backgroundColor: Colors.white,
              itemStyle: TextStyle(
                  color: CustomTheme.fontPurple02Color,
                  fontWeight: FontWeight.bold,
                  fontSize: 18),
              doneStyle:
              TextStyle(color: Colors.white, fontSize: 16),
              cancelStyle: TextStyle(color: Colors.white, fontSize: 16)),
          onConfirm: (date) {
            print('confirm $date');
            selectedDate = date;
            setState(() {});
          }, currentTime: DateTime.now(), locale: LocaleType.en);

    },child: Container(
      height: Constant().getViewWidth(context)/10,
      width: Constant().getViewWidth(context)/10,
      // padding: EdgeInsets.symmetric(horizontal: 15),
      alignment: Alignment.center,
      decoration: BoxDecoration(
          border: Border.all(color: CustomTheme.fontPurple02Color),
          borderRadius: BorderRadius.circular(25),
          color: Colors.transparent
      ),
      child: Text(
        "${selectedDate.year}-${selectedDate.month}-${selectedDate.day}",
        style: CustomTheme.bigTextPrimaryPurple01,
      ),
    ),);
  }

  Widget AccountBotContent() {
    return Container(
      margin: EdgeInsets.all(10),
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(25)),
      ),
      child: Column(children: [
        Spacer(),
        Expanded(flex: 2, child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
          Expanded(flex: 4, child: Container()),
          Expanded(flex: 1, child: CircleAvatar(backgroundColor: CustomTheme.fontPurple01Color, radius: 40,)),
          Expanded(flex: 1, child: CircleAvatar(backgroundColor: CustomTheme.fontPurple01Color, radius: 40,)),
          Expanded(flex: 4, child: Container())
        ],)),
        Spacer(),
        Expanded(flex: 3, child: Text('Energy Balance (USDT)', style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 14),)),
        Expanded(flex: 3, child: Text(balanceValue, style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 18),)),
        Expanded(flex: 3, child: Text('Referral Account (USDT)', style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 14),)),
        Expanded(flex: 3, child: Text(referralValue, style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 18))),
        Spacer()
      ]),);
  }

  addZero(int month) {
    return month < 10 ? "0$month" : "$month";
  }
}