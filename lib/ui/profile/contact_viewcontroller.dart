import 'package:bobot01/constants/theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../constants/constants.dart';
import '../../constants/public_component.dart';

class ContactScreen extends ConsumerStatefulWidget {
  const ContactScreen({Key? key}) : super(key: key);

  @override
  _ContactScreenState createState() => _ContactScreenState();
}

class _ContactScreenState extends ConsumerState<ContactScreen> {
  @override
  Widget build(BuildContext context) {
    return buildFlexBodyLayout(context, Container(
        color: Colors.transparent,
        margin: EdgeInsets.symmetric(horizontal: Constant().getViewWidth(context) / 20,),
        child: Stack(children: [
        Container(margin: EdgeInsets.only(top: Constant().getViewHeight(context) * 38/100), alignment: Alignment.center, height: Constant().getViewHeight(context)/10, child: Image.asset('assets/img/img_bobot_contact_01.png'),),
    Column(children: [
    Expanded(flex: 2, child: Container(child: buildAppBar(title: "Contact", ref: ref, backPage: 4))),
    Expanded(flex: 26, child: Container(alignment: Alignment.center, color: Colors.transparent, child: Container(child: Center(child:
    Text("Not yet activated", style: CustomTheme.textPrimaryPurple02,),),))
    )])])));
  }
}