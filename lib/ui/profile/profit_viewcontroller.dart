import 'package:bobot01/ui/profile/profile_model.dart';
import 'package:bobot01/ui/profile/profile_viewmodel.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../constants/SignInWidgets.dart';
import '../../constants/constants.dart';
import '../../constants/lightCell.dart';
import '../../constants/public_component.dart';
import '../../constants/theme.dart';
import '../home/home_viewcontroller.dart';
import '../home/home_viewmodel.dart';

class ProfitScreen extends ConsumerStatefulWidget {
  const ProfitScreen({Key? key}) : super(key: key);

  @override
  _ProfitScreenState createState() => _ProfitScreenState();
}

class _ProfitScreenState extends ConsumerState<ProfitScreen> {

  bool isLoading = false;
  List<IncomeInfo> incomeList = List.empty(growable: true);
  String totalProfit = "";
  String todayProfit = "";
  String lastDayProfit = "";
  String incomeRate = "";
  String totalIncome = "";
  DateTime selectedDate = DateTime(2022);
  TextEditingController textController = TextEditingController();

  @override
  void initState() {

    ref.read(queryIncomeProvider.future).then((event) {
      event.then((value) {
        totalProfit = value.data['incomeTotal'];
        todayProfit = value.data['todayIncome'];
        lastDayProfit = value.data['yesterdayIncome'];
        setState(() {});
      });
    });

  }

  @override
  Widget build(BuildContext context) {
    return buildFlexBodyLayout(context, Container(color: Colors.transparent,
        margin: EdgeInsets.symmetric(horizontal: Constant().getViewWidth(context) / 20,),
        child: Stack(children: [
        Container(margin: EdgeInsets.only(top: Constant().getViewHeight(context) * 76/100), alignment: Alignment.bottomRight, height: Constant().getViewHeight(context)/12, child: Image.asset('assets/img/img_bg_bobot_03.png'),),
        Column(children: [
          Expanded(flex: 2, child: Container(child: buildAppBar(title: "Profit", ref: ref, backPage: 4))),
          Expanded(flex: 7, child: Container(alignment: Alignment.center, color: Colors.transparent, child: Column(children: [
            Expanded(flex: 2, child: CircleAvatar(
              backgroundColor: CustomTheme.fontPurple01Color,
              radius: 40,
            )),
            Container(width: 10, height: 10, color: CustomTheme.fontPurple01Color,),
            Expanded(flex: 10, child: Container(
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: CustomTheme.fontPurple01Color,
                borderRadius: BorderRadius.all(Radius.circular(35)),
              ),
              child: ProfitBotContent(totalProfit, todayProfit, lastDayProfit),
            ))
          ],),)),
          Expanded(flex: 2, child: Row(children: [
            Expanded(flex: 4, child: Container(alignment: Alignment.centerLeft, child: Text("Income info", style: TextStyle(fontSize: 14, color: CustomTheme.fontPurple02Color)))),
            Expanded(flex: 8, child: _buildSelectTimeZoneDropBtn()),
            Spacer(),
            Expanded(flex: 2, child: Container(alignment: Alignment.center, child: GestureDetector(child: Image.asset('assets/btn/button_search_01.png', height: 45),onTap: (){
              var pp = ProfitInfoParameter(
                  startTime: "${selectedDate.year}-${addZero(selectedDate.month)}-${addZero(selectedDate.day)} 00:00",
                  endTime: "${selectedDate.year}-${addZero(selectedDate.month)}-${addZero(selectedDate.day+1)} 00:00",
                  // walletType: proxyValue,
                  // positionType: positionValue
              );
              print(pp);
              ref.watch(profitInfoProvider(pp).future).then((event) {
                event.then((value) {
                  totalIncome = value.data["totalIncome"];
                  incomeRate = value.data["incomeRate"];
                  setState(() {});
                  showResponseDialog(context, "${(value).url}\n\n${(value).code}\n\n${(value).message}\n\n${(value).data}", (){});
                });
              });
            },))),
          ],)),
          Expanded(flex: 1, child: Container(color: Colors.transparent, child: Row(children: [
            Expanded(flex: 2, child: Container(alignment: Alignment.centerLeft, child: Text("Total Profit(USDT)", style: TextStyle(fontSize: 12, color: CustomTheme.fontPurple02Color)))),
            Expanded(flex: 2, child: Container(alignment: Alignment.center, child: Text(totalIncome, style: TextStyle(fontSize: 20, color: CustomTheme.fontPurple01Color)),)),
            Expanded(flex: 1, child: Container(alignment: Alignment.center, child: Text("Rate", style: TextStyle(fontSize: 14, color: CustomTheme.fontPurple02Color)))),
            Expanded(flex: 1, child: Container(alignment: Alignment.center, child: Text(incomeRate+"%", style: TextStyle(fontSize: 20, color: CustomTheme.fontGreen01Color)))),
          ],),)),
          Container(height: 1, alignment: Alignment.center, color: CustomTheme.fontPurple02Color,),
          Expanded(flex: 1, child: Container(color: Colors.transparent, child: Row(children: [
            Expanded(flex: 5, child: Container(alignment: Alignment.centerLeft, child: Text("Income(USDT)", style: TextStyle(fontSize: 14, color: CustomTheme.fontPurple02Color)))),
            Expanded(flex: 3, child: Container(alignment: Alignment.center, child: Text("Rate", style: TextStyle(fontSize: 14, color: CustomTheme.fontPurple02Color)),)),
            Spacer(),
            Expanded(flex: 4, child: Container(alignment: Alignment.center, child: Text("Robot", style: TextStyle(fontSize: 14, color: CustomTheme.fontPurple02Color)))),
            Expanded(flex: 4, child: Container(alignment: Alignment.center, child: Text("Coin", style: TextStyle(fontSize: 14, color: CustomTheme.fontPurple02Color)))),
          ],),)),
          Expanded(flex: 12, child: Container(child: SingleChildScrollView(child: Column(children: [
            Column(
              children: incomeList.map((info) => buildIncmoeListColumn(info)).toList(),
            ),
            Container(height: Constant().getViewHeight(context)/8,)
          ],),),)),
        ])])), isLoading: isLoading);
  }

  addZero(int month) {
    return month < 10 ? "0$month" : "$month";
  }

  Widget _buildSelectTimeZoneDropBtn(){
    return GestureDetector(onTap: (){
      DatePicker.showDatePicker(context,
          showTitleActions: true,
          minTime: DateTime(2018, 1, 1),
          maxTime: DateTime(2026, 12, 31),
          theme: DatePickerTheme(
              headerColor: CustomTheme.fontPurple01Color,
              backgroundColor: Colors.white,
              itemStyle: TextStyle(
                  color: CustomTheme.fontPurple02Color,
                  fontWeight: FontWeight.bold,
                  fontSize: 18),
              doneStyle:
              TextStyle(color: Colors.white, fontSize: 16),
              cancelStyle: TextStyle(color: Colors.white, fontSize: 16)),
          onConfirm: (date) {
            print('confirm $date');
            selectedDate = date;
            setState(() {});
          }, currentTime: DateTime.now(), locale: LocaleType.en);

    },child: Container(
      height: Constant().getViewWidth(context)/10,
      width: Constant().getViewWidth(context)/10,
      // padding: EdgeInsets.symmetric(horizontal: 15),
      alignment: Alignment.center,
      decoration: BoxDecoration(
          border: Border.all(color: CustomTheme.fontPurple02Color),
          borderRadius: BorderRadius.circular(25),
          color: Colors.transparent
      ),
      child: Text(
        "${selectedDate.year}-${selectedDate.month}-${selectedDate.day}",
        style: CustomTheme.bigTextPrimaryPurple01,
      ),
    ),);
  }

  Widget buildIncmoeListColumn(IncomeInfo info) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: Constant().getViewHeight(context)/140),
      child: lightCell(Container(padding: EdgeInsets.symmetric(vertical: Constant().getViewHeight(context)/140),child: Row(children: [
        Expanded(flex: 6 , child: RedGreenCell(info.income)),
        Expanded(flex: 6, child: Container(
          alignment: Alignment.topLeft,
          child: Text(
            info.incomeRate,
            textAlign: TextAlign.left,
            style: TextStyle(fontSize: 16, color: CustomTheme.fontPurple02Color),
            maxLines: 1,
            overflow: TextOverflow.ellipsis,),
        ),),
        Expanded(flex: 6, child: Container(alignment: Alignment.centerLeft, child: Text(info.tradeType, textAlign: TextAlign.center, style: TextStyle(fontSize: 16, color: CustomTheme.fontPurple02Color),)),),
        Expanded(flex: 6, child: Container(alignment: Alignment.centerLeft, child: Text(info.symbol, textAlign: TextAlign.center, style: TextStyle(fontSize: 16, color: CustomTheme.fontPurple02Color),)),),

      ],),)),);
  }
}

Widget ProfitBotContent(String total, String totalProfit, String todayProfit) {
  return Column(children: [
    Expanded(flex: 8, child: Container(
      margin: EdgeInsets.all(10),
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(topLeft: Radius.circular(25),topRight: Radius.circular(25),bottomLeft: Radius.circular(10),bottomRight: Radius.circular(10),),
      ),
      child: Column(children: [
        Spacer(),
        Expanded(flex: 3, child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
          Expanded(flex: 4, child: Container()),
          Expanded(flex: 1, child: CircleAvatar(backgroundColor: CustomTheme.fontPurple01Color, radius: 40,)),
          Expanded(flex: 1, child: CircleAvatar(backgroundColor: CustomTheme.fontPurple01Color, radius: 40,)),
          Expanded(flex: 4, child: Container())
        ],)),
        Spacer(),
        Expanded(flex: 3, child: Text("Total Profit (USDT)",style: TextStyle(fontSize: 14, color: CustomTheme.fontPurple01Color))),
        Expanded(flex: 6, child: Text(total, style: TextStyle(fontSize: 24, color: CustomTheme.fontPurple02Color, fontWeight: FontWeight.w700))),
      ],),)),
    Expanded(flex: 3, child: Row(children: [
      Spacer(),
      Expanded(flex: 6, child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        Row(children: [
          Container(margin: EdgeInsets.only(right: 5), height: 15, width: 3, color: CustomTheme.cellYellowColor,),
          Text("Today Profit",style: TextStyle(fontSize: 12, color: Colors.white)),
        ],),
        Container(margin: EdgeInsets.only(left: 8), alignment:Alignment.centerLeft, child: Text(totalProfit, style: TextStyle(fontSize: 14, color: Colors.white),),)
      ],)),
      Spacer(),
      Expanded(flex: 6, child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        Row(children: [
          Container(margin: EdgeInsets.only(right: 5), height: 15, width: 3, color: CustomTheme.cellYellowColor,),
          Text("Last Day Profit",style: TextStyle(fontSize: 12, color: Colors.white)),
        ],),
        Container(margin: EdgeInsets.only(left: 8), alignment:Alignment.centerLeft, child: Text(todayProfit, style: TextStyle(fontSize: 14, color: Colors.white)),)
      ],)),
      Spacer()
    ],)),
    Spacer()
  ],);
}