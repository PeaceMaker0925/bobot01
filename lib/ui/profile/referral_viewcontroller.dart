import 'package:bobot01/ui/profile/profile_model.dart';
import 'package:bobot01/ui/profile/profile_viewmodel.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../constants/SignInWidgets.dart';
import '../../constants/constants.dart';
import '../../constants/lightCell.dart';
import '../../constants/public_component.dart';
import '../../constants/theme.dart';

class ReferralScreen extends ConsumerStatefulWidget {
  const ReferralScreen({Key? key}) : super(key: key);

  @override
  _ReferralScreenState createState() => _ReferralScreenState();
}

class _ReferralScreenState extends ConsumerState<ReferralScreen> {
  bool isLoading = false;
  String totalReferral = "1,000";
  String personal_today = "0.00";
  String personal_yesterday = "0.00";
  String team_today = "0.00";
  String team_yesterday = "0.00";
  List<ReferralInfo> referralList = List.empty(growable: true);
  TextEditingController textController = TextEditingController();
  DateTime selectedDate = DateTime(2022);

  @override
  void initState() {
    ref.read(agentInfoProvider.future).then((event) {
      event.then((value) {
        showResponseDialog(context, "${(value).url}\n\n${(value).code}\n\n${(value).message}\n\n${(value).data}", (){});
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return buildFlexBodyLayout(context, Container(
        color: Colors.transparent,
        margin: EdgeInsets.symmetric(horizontal: Constant().getViewWidth(context) / 20,),
        child: Stack(children: [
        Container(margin: EdgeInsets.only(top: Constant().getViewHeight(context) * 76/100), alignment: Alignment.bottomRight, height: Constant().getViewHeight(context)/12, child: Image.asset('assets/img/img_bg_bobot_07.png'),),
        Column(children: [
          Expanded(flex: 3, child: Container(child: buildAppBar(title: "Referral", ref: ref, backPage: 4))),
          Expanded(flex: 6  , child: Container(alignment: Alignment.center, color: Colors.transparent, child: Column(children: [
            Expanded(flex: 2, child: CircleAvatar(
              backgroundColor: CustomTheme.fontPurple01Color,
              radius: 40,
            )),
            Container(width: 10, height: 10, color: CustomTheme.fontPurple01Color,),
            Expanded(flex: 10, child: Container(
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: CustomTheme.fontPurple01Color,
                borderRadius: BorderRadius.all(Radius.circular(35)),
              ),
              child: ReferralBotContent(totalReferral),
            ))
          ],),)),
          Spacer(),
          Expanded(flex: 4, child: Row(children: [
            Expanded(flex: 30, child: Container(child: lightCell(CommissionWidget("Personal Commission\n( Today )", personal_today)))),
            Spacer(),
            Expanded(flex: 30, child: Container(child: lightCell(CommissionWidget("Personal Commission\n( Yesterday )", personal_today)))),
          ],)),
          Container(height: 5,),
          Expanded(flex: 4, child: Row(children: [
            Expanded(flex: 30, child: Container(child: lightCell(CommissionWidget("Team Commission\n( Today )", team_today)))),
            Spacer(),
            Expanded(flex: 30, child: Container(child: lightCell(CommissionWidget("Team Commission\n( Yesterday )", team_yesterday)))),
          ],)),
          Spacer(),
          Container(height: 1, alignment: Alignment.center, color: CustomTheme.fontPurple02Color,),
          Expanded(flex: 4, child: Row(children: [
            Expanded(flex: 8, child: GestureDetector(onTap: (){
              DatePicker.showDatePicker(context,
                  showTitleActions: true,
                  minTime: DateTime(2018, 1, 1),
                  maxTime: DateTime(2026, 12, 31),
                  theme: DatePickerTheme(
                      headerColor: CustomTheme.fontPurple01Color,
                      backgroundColor: Colors.white,
                      itemStyle: TextStyle(
                          color: CustomTheme.fontPurple02Color,
                          fontWeight: FontWeight.bold,
                          fontSize: 18),
                      doneStyle:
                      TextStyle(color: Colors.white, fontSize: 16),
                  cancelStyle: TextStyle(color: Colors.white, fontSize: 16)),
                  onConfirm: (date) {
                    print('confirm $date');
                    selectedDate = date;
                    setState(() {});
                  }, currentTime: DateTime.now(), locale: LocaleType.en);

            },child: Container(
              height: Constant().getViewWidth(context)/10,
              width: Constant().getViewWidth(context)/10,
              // padding: EdgeInsets.symmetric(horizontal: 15),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  border: Border.all(color: CustomTheme.fontPurple02Color),
                  borderRadius: BorderRadius.circular(25),
                  color: Colors.transparent
              ),
              child: Text(
                "${selectedDate.year}-${selectedDate.month}-${selectedDate.day}",
                style: CustomTheme.bigTextPrimaryPurple01,
              ),
            ),)),
            Spacer(),
            Expanded(flex: 2, child: Container(alignment: Alignment.center, child: CircleSearchButton())),
          ],)),
          Expanded(flex: 1, child: Container(color: Colors.transparent, child: Row(children: [
            Expanded(flex: 3, child: Container(alignment: Alignment.centerLeft, child: Text("Name", style: TextStyle(fontSize: 14, color: CustomTheme.fontPurple02Color)))),
            Expanded(flex: 4, child: Container(alignment: Alignment.center, child: Text("Commission", style: TextStyle(fontSize: 14, color: CustomTheme.fontPurple02Color)),)),
            Spacer(),
            Expanded(flex: 3, child: Container(alignment: Alignment.center, child: Text("time", style: TextStyle(fontSize: 14, color: CustomTheme.fontPurple02Color)))),
            Expanded(flex: 3, child: Container(alignment: Alignment.center, child: Text("Deposit", style: TextStyle(fontSize: 14, color: CustomTheme.fontPurple02Color)))),
          ],),)),
          Expanded(flex: 12, child: Container(child: SingleChildScrollView(child: Column(children: [
            Column(
              children: referralList.map((info) => buildReferralListColumn(info)).toList(),
            ),
            Container(height: Constant().getViewHeight(context)/8,)
          ],),),)),
        ])])), isLoading: isLoading);
  }
  Widget buildReferralListColumn(ReferralInfo info) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: Constant().getViewHeight(context)/140),
      child: lightCell(Container(padding: EdgeInsets.symmetric(vertical: Constant().getViewHeight(context)/140),child: Row(children: [
        Expanded(flex: 6, child: Container(alignment: Alignment.centerLeft, child: Text(info.price, textAlign: TextAlign.center, style: TextStyle(fontSize: 16, color: CustomTheme.fontPurple02Color),))),
        Expanded(flex: 6, child: Container(alignment: Alignment.centerLeft, child: Text(info.price, textAlign: TextAlign.center, style: TextStyle(fontSize: 16, color: CustomTheme.fontPurple02Color),))),
        Expanded(flex: 6, child: Container(alignment: Alignment.centerLeft, child: Text(info.price, textAlign: TextAlign.center, style: TextStyle(fontSize: 16, color: CustomTheme.fontPurple02Color),)),),
        Expanded(flex: 6, child: Container(alignment: Alignment.centerLeft, child: Text(info.price, textAlign: TextAlign.center, style: TextStyle(fontSize: 16, color: CustomTheme.fontPurple02Color),)),),
      ],),)),);
  }

}

Widget CommissionWidget(String text, String data) {
  return Container(
    margin: EdgeInsets.all(0.5),
    alignment: Alignment.center,child: Column(children: [
      Expanded(child: Text(text, style: TextStyle(fontSize: 12, color: CustomTheme.fontPurple02Color), textAlign: TextAlign.center,)),
      Expanded(child: Text(data, style: TextStyle(fontSize: 20, color: CustomTheme.fontPurple01Color, fontWeight: FontWeight.w700))),
    ],),);
}

Widget ReferralBotContent(String totalReferral) {
  return Container(
    margin: EdgeInsets.all(10),
    alignment: Alignment.center,
    decoration: BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.all(Radius.circular(25)),
    ),
    child: Column(children: [
      Spacer(),
      Expanded(flex: 2, child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
        Expanded(flex: 4, child: Container()),
        Expanded(flex: 1, child: CircleAvatar(backgroundColor: CustomTheme.fontPurple01Color, radius: 40,)),
        Expanded(flex: 1, child: CircleAvatar(backgroundColor: CustomTheme.fontPurple01Color, radius: 40,)),
        Expanded(flex: 4, child: Container())
      ],)),
      Spacer(),
      Expanded(flex: 3, child: Text("Total Referral (USDT)",style: TextStyle(fontSize: 14, color: CustomTheme.fontPurple01Color))),
      Expanded(flex: 6, child: Text(totalReferral, style: TextStyle(fontSize: 24, color: CustomTheme.fontPurple02Color, fontWeight: FontWeight.w700))),
    ],),);
}