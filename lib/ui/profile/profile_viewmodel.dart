import 'package:equatable/equatable.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../repository/api_response.dart';
import '../../repository/profile_api_repository.dart';

final userInfoProvider = FutureProvider.autoDispose<Future<ApiResponse>>((ref) async {
  return ProfileApiRepositoryImpl().queryUserInfo();
});

final userNameEditProvider = FutureProvider.autoDispose.family<Future<ApiResponse>, String>((ref, newName) async {
  return ProfileApiRepositoryImpl().editUserName(newName);
});

final totalProfitProvider = FutureProvider.autoDispose<Future<ApiResponse>>((ref) async {
  return ProfileApiRepositoryImpl().queryTotalProfit();
});

final profitInfoProvider = FutureProvider.autoDispose.family<Future<ApiResponse>, ProfitInfoParameter>((ref, parameter) async {
  return ProfileApiRepositoryImpl().queryProfitInfo(parameter);
});

final agentInfoProvider = FutureProvider.autoDispose<Future<ApiResponse>>((ref) async {
  return ProfileApiRepositoryImpl().queryAgentInfo();
});


class ProfitInfoParameter extends Equatable {
  final String startTime;
  final String endTime;
  // final String walletType;
  // final String positionType;

  const ProfitInfoParameter({
    required this.startTime,
    required this.endTime,
    // required this.walletType,
    // required this.positionType,
  });

  @override
  List<Object?> get props => [startTime, endTime/*, walletType, positionType*/];
}