import 'package:bobot01/constants/lightCell.dart';
import 'package:bobot01/constants/theme.dart';
import 'package:bobot01/ui/profile/profile_viewmodel.dart';
import 'package:bobot01/ui/sign_in/login_viewmodel.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../constants/constants.dart';
import '../../constants/public_component.dart';
import '../../constants/shared_preference.dart';
import '../../main.dart';
import '../bot/bot_model/account_model.dart';

class ProfileScreen extends ConsumerStatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends ConsumerState<ProfileScreen> {
  String currentAccount = '';
  String memberId = '';
  String name = '';
  String uid = '';
  String imgUrl = '';
  String agentName = '';
  String vipLevel = '';
  String contactInfo = '';
  bool isEdit = false;
  TextEditingController nameTextController = TextEditingController();

  @override
  void initState() {
    ///need login
    print('profile init');
    BobotSharedPreferences().getUserToken().then((value) {
      setState(() {
        value != "userToken"
            ? {}
            : ref.read(showToLogInProvider.notifier).setShowToLogIn(!ref.watch(showToLogInProvider));
      });
    });
    ref.read(userInfoProvider.future).then((event) {
      event.then((value) {
        currentAccount = value.data["loginAccount"];
        memberId = value.data["memberId"];
        nameTextController.text = value.data["name"];
        name = value.data["name"];
        uid = value.data["uid"];
        imgUrl = value.data["imgUrl"];
        agentName = value.data["agentName"];
        vipLevel = value.data["vipLevel"];
        contactInfo = value.data["contactInfo"];
        setState(() {});
      });
    });
    ref.read(totalProfitProvider.future).then((event) {
      event.then((value) {
        // currentAccount = value.data["loginAccount"];
        // memberId = value.data["memberId"];
        // nameTextController.text = value.data["name"];
        // name = value.data["name"];
        // uid = value.data["uid"];
        // imgUrl = value.data["imgUrl"];
        // agentName = value.data["agentName"];
        // vipLevel = value.data["vipLevel"];
        // contactInfo = value.data["contactInfo"];
        setState(() {});
      });
    });
  }
  @override
  Widget build(BuildContext context) {
    List<ProfileCellListInfo> profileCellList = [
      ProfileCellListInfo(title: 'Profit', imagePath: '',  action: () {ref.read(bodyIndexProvider.notifier).setBodyIndex(8);} ),
      ProfileCellListInfo(title: 'Account', imagePath: '',  action: () {ref.read(bodyIndexProvider.notifier).setBodyIndex(9);} ),
      ProfileCellListInfo(title: 'Referral', imagePath: '',  action: () {ref.read(bodyIndexProvider.notifier).setBodyIndex(10);} ),
      ProfileCellListInfo(title: 'Contact', imagePath: '',  action: () {ref.read(bodyIndexProvider.notifier).setBodyIndex(11);} ),
    ];
    return buildFlexBodyLayout(context, Container(
      color: Colors.transparent,
      margin: EdgeInsets.symmetric(
          horizontal: Constant().getViewWidth(context) / 20,
      ),
      child: Column(children: [
        Expanded(flex: 1, child: Container()),
        Expanded(flex: 4, child: Container(height: Constant().getViewHeight(context)/8, child: Image.asset('assets/img/img_preset_photo_01.png'),)),
        Expanded(flex: 1, child: Container(
            alignment: Alignment.center,
            child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              isEdit ? Container(
                height: Constant().getViewWidth(context)/10,
                  width: Constant().getViewWidth(context)/10,
                  alignment: Alignment.centerLeft,
                  decoration: BoxDecoration(
                      border: Border.all(color: CustomTheme.fontPurple02Color),
                      borderRadius: BorderRadius.circular(5),
                      color: Colors.transparent
                  ),
                  child: TextFormField(
                    controller: nameTextController,
                    textAlign: TextAlign.center,
                    textAlignVertical: TextAlignVertical.center,
                    decoration: InputDecoration(
                      isDense: true,
                      border: InputBorder.none,
                    ),
                    style: CustomTheme.bigTextPrimaryPurple01,
                  ),
              ) : Text(name , style: CustomTheme.bigTextPrimaryPurple01, textAlign: TextAlign.right,),
              GestureDetector(
                child: Container(
                  child: isEdit
                      ? Icon(Icons.check_box, color: CustomTheme.fontPurple02Color,)
                      : Icon(Icons.edit, color: CustomTheme.fontPurple02Color,),),
                onTap: (){
                  if(isEdit && name != nameTextController.text){
                    ref.read(userNameEditProvider(nameTextController.text).future).then((event) => {
                      event.then((value) {
                        if(value.message == "SUCCESS"){ name = nameTextController.text; setState(() {});}
                        else {
                          showResponseDialog(context, (value).message, (){});
                        }
                      }

                      )});
                  }
                  isEdit = !isEdit;
                  setState(() {});
                  },)
            ],))),
        Expanded(flex: 1, child: Container(child: Text(currentAccount,style: CustomTheme.textPrimaryPurple02,))),
        Expanded(flex: 3, child: Container(child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: profileCellList.asMap().entries.map((info) => profileCell(info)).toList(),),)),
        Expanded(flex: 7, child: Container(
          margin: EdgeInsets.symmetric(horizontal: Constant().getViewWidth(context)/20),
          alignment: Alignment.center,
          child: lightCell(Container(
            margin: EdgeInsets.all(Constant().getViewWidth(context)/40),
            child: Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
            Container(margin: EdgeInsets.all(4), alignment: Alignment.centerLeft, child: Text("My Invitation Code" , style: CustomTheme.textPrimaryPurple02, textAlign: TextAlign.left,)),
            Container(margin: EdgeInsets.all(4), alignment: Alignment.centerLeft, child: Row(children: [
              Text(uid , style: CustomTheme.textPrimaryPurple02Thin, textAlign: TextAlign.left,),
              GestureDetector(child: Container(height: Constant().getViewHeight(context)/30, child: Image.asset('assets/btn/btn_edit_01_nor.png'),),
                  onTap: ()async {
                    Clipboard.setData(ClipboardData(text: uid)).then((value) {
                      return ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(content: Text('uid Copied'),),
                      );},);
                  })
            ],)),
            Container(margin: EdgeInsets.all(4), alignment: Alignment.centerLeft, child: Text("Member ID" , style: CustomTheme.textPrimaryPurple02, textAlign: TextAlign.left,)),
            Container(margin: EdgeInsets.all(4), alignment: Alignment.centerLeft, child: Row(children: [
              Text(memberId , style: CustomTheme.textPrimaryPurple02Thin, textAlign: TextAlign.left,),
              GestureDetector(child: Container(height: Constant().getViewHeight(context)/30, child: Image.asset('assets/btn/btn_edit_01_nor.png'),),
                  onTap: ()async {
                    Clipboard.setData(ClipboardData(text: memberId)).then((value) {
                      return ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(content: Text('memberId Copied'),),
                      );},);
                  })
            ],)),
            Container(margin: EdgeInsets.all(4), alignment: Alignment.centerLeft, child: Text("Inviter" , style: CustomTheme.textPrimaryPurple02, textAlign: TextAlign.left,)),
            Container(margin: EdgeInsets.all(4), alignment: Alignment.centerLeft, child: Text(agentName , style: CustomTheme.textPrimaryPurple02Thin, textAlign: TextAlign.left,)),

          ],),)),)),
        Expanded(flex: 4, child: Container()),

    ])));
  }

  Widget profileCell(MapEntry<int, ProfileCellListInfo> info) {
    Widget cell = Container();
    switch (info.key) {
      case 0: {
        cell = ProfitBtn(onTap: () { info.value.action(); });
        break;
      }
      case 1: {
        cell = AccountBtn(onTap: () { info.value.action(); });
        break;
      }
      case 2: {
        cell = ReferralBtn(onTap: () { info.value.action(); });
        break;
      }
      case 3: {
        cell = ContactBtn(onTap: () { info.value.action(); });
        break;
      }
    }
    return Container(alignment: Alignment.center, child: cell);
  }
}

Widget buildAccountCellBtn({required String title, required String imageString, required Function() onTap}) {
  var context = navigatorKey.currentContext!;
  // print(imageString);
  return Stack(alignment: Alignment.topCenter,
    children: [
      Container(
        margin: EdgeInsets.symmetric(vertical: Constant().getViewHeight(context)/40),
        // margin: EdgeInsets.only(top: Constant().getViewHeight(context)/128, left: Constant().getViewHeight(context)/60, right: Constant().getViewHeight(context)/60),
        alignment: Alignment.topCenter,
        height: Constant().getViewHeight(context)/32,
        child: Image.asset(imageString)),
      Container(
        alignment: Alignment.topCenter,
        margin: EdgeInsets.only(top: Constant().getViewHeight(context)/20),
        child: GestureDetector(
          onTap: onTap,
          child: Container(
            alignment: Alignment.center,
            // padding: EdgeInsets.all(10),
            width: Constant().getViewWidth(context)/5,
            height: Constant().getViewWidth(context)/8,
            // decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.blueAccent),
            child: lightCell(Text(title, style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 14),), isBar: true),
          ),
        ),),
      Container(
        margin: EdgeInsets.only(top: Constant().getViewHeight(context)/360),
        alignment: Alignment.topCenter,
        height: Constant().getViewHeight(context)/16,
        child: Center(child: Image.asset(imageString),),),
    ],);
}

Widget ProfitBtn({required Function() onTap}) {
  var context = navigatorKey.currentContext!;
  return Stack(alignment: Alignment.topCenter,
    children: [
    Container(
      margin: EdgeInsets.symmetric(vertical: Constant().getViewHeight(context)/40),
      // margin: EdgeInsets.only(top: Constant().getViewHeight(context)/128, left: Constant().getViewHeight(context)/60, right: Constant().getViewHeight(context)/60),
      alignment: Alignment.topCenter,
      height: Constant().getViewHeight(context)/32,
      child: Image.asset('assets/img/img_btn_profit_b_01.png'),),
    Container(
      alignment: Alignment.topCenter,
      margin: EdgeInsets.only(top: Constant().getViewHeight(context)/20),
      child: GestureDetector(
        onTap: onTap,
        child: Container(
          alignment: Alignment.center,
          // padding: EdgeInsets.all(10),
          width: Constant().getViewWidth(context)/5,
          height: Constant().getViewWidth(context)/8,
          // decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.blueAccent),
          child: lightCell(Text('Profit', style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 14),), isBar: true),
        ),
      ),),
    Container(
      margin: EdgeInsets.only(top: Constant().getViewHeight(context)/360),
      alignment: Alignment.topCenter,
      height: Constant().getViewHeight(context)/16,
      child: Center(child: Image.asset('assets/img/img_btn_profit.png'),),),
  ],);
}

Widget AccountBtn({required Function() onTap}) {
  var context = navigatorKey.currentContext!;
  return Stack(alignment: Alignment.topCenter,
    children: [
    Container(
      alignment: Alignment.topCenter,
      margin: EdgeInsets.only(top: Constant().getViewHeight(context)/20),
      child: GestureDetector(
        onTap: onTap,
        child: Container(
          alignment: Alignment.center,
          // padding: EdgeInsets.all(10),
          width: Constant().getViewWidth(context)/5,
          height: Constant().getViewWidth(context)/8,
          // decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.blueAccent),
          child: lightCell(Text('Account', style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 14),), isBar: true),
        ),
      ),),
    Container(
      alignment: Alignment.topCenter,
      height: Constant().getViewHeight(context)/16,
      child: Center(child: Image.asset('assets/img/img_btn_account_01.png'),),),
  ],);
}

Widget ReferralBtn({required Function() onTap}) {
  var context = navigatorKey.currentContext!;
  return Stack(alignment: Alignment.topCenter,
    children: [
    Container(
      margin: EdgeInsets.symmetric(vertical: Constant().getViewHeight(context)/100),
      // margin: EdgeInsets.only(top: Constant().getViewHeight(context)/128, left: Constant().getViewHeight(context)/60, right: Constant().getViewHeight(context)/60),
      alignment: Alignment.topCenter,
      height: Constant().getViewHeight(context)/20,
      child: Image.asset('assets/img/img_btn_referral_b_01.png'),),
    Container(
      alignment: Alignment.topCenter,
      margin: EdgeInsets.only(top: Constant().getViewHeight(context)/20),
      child: GestureDetector(
        onTap: onTap,
        child: Container(
          alignment: Alignment.center,
          // padding: EdgeInsets.all(10),
          width: Constant().getViewWidth(context)/5,
          height: Constant().getViewWidth(context)/8,
          // decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.blueAccent),
          child: lightCell(Text('Referral', style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 14),), isBar: true),
        ),
      ),),
    Container(
      margin: EdgeInsets.symmetric(vertical: Constant().getViewHeight(context)/108),
      alignment: Alignment.topCenter,
      height: Constant().getViewHeight(context)/20,
      child: Center(child: Image.asset('assets/img/img_btn_referral_f_01.png'),),),
  ],);
}

Widget ContactBtn({required Function() onTap}) {
  var context = navigatorKey.currentContext!;
  return Stack(
    alignment: Alignment.topCenter,
    children: [
    Container(
      alignment: Alignment.topCenter,
      margin: EdgeInsets.only(top: Constant().getViewHeight(context)/20),
      child: GestureDetector(
        onTap: onTap,
        child: Container(
          alignment: Alignment.center,
          // padding: EdgeInsets.all(10),
          width: Constant().getViewWidth(context)/5,
          height: Constant().getViewWidth(context)/8,
          // decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.blueAccent),
          child: lightCell(Text('Contact', style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 14),), isBar: true),
        ),
      ),),
    Container(
      alignment: Alignment.topCenter,
      margin: EdgeInsets.only(top: Constant().getViewHeight(context)/180),
      height: Constant().getViewHeight(context)/18,
      child: Center(child: Image.asset('assets/img/img_btn_contact_01.png'),),),
  ],);
}
