import 'package:bobot01/constants/constants.dart';
import 'package:bobot01/constants/theme.dart';
import 'package:bobot01/ui/sign_in/login_viewmodel.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import '../constants/lightCell.dart';

class FloatingBar extends ConsumerStatefulWidget {
  @override
  ConsumerState<ConsumerStatefulWidget> createState() {
    return FloatingBarState();
  }
}

class FloatingBarState extends ConsumerState<FloatingBar>{
  int currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    List<BarItemData> barList = [
      BarItemData(label: 'Home', iconPathName: 'assets/icon_bar_home.png', size: 30),
      BarItemData(label: 'Bot', iconPathName: 'assets/icon_bar_robot.png', size: 40),
      BarItemData(label: 'Bobot Run', iconPathName: 'assets/icon_bar_game.png', size: 40),
      BarItemData(label: 'Follow', iconPathName: 'assets/icon_bar_order.png', size: 30),
      BarItemData(label: 'Profile', iconPathName: 'assets/icon_bar_profile.png', size: 26),
    ];

    return lightCell(Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: barList.asMap().entries.map((item) => BarItemView(context, item.key, item.value, currentIndex, (index){
      ref.read(bodyIndexProvider.notifier).setBodyIndex(item.key);
      setState(() { currentIndex = index; },);
    })).toList()), isBar: true);
  }

}

Widget BarItemView(BuildContext context, int index, BarItemData item, int currentIndex, Function(int index) onTap) {
  return GestureDetector(onTap: (){
    onTap(index);
  }, child: Container(padding: EdgeInsets.symmetric(horizontal: Constant().getViewWidth(context)/40), color: Colors.transparent, child: Column(children: [
    Expanded(flex:2, child: Container(
        alignment: Alignment.center,
        child: ImageIcon(AssetImage(item.iconPathName), color: index == currentIndex ? CustomTheme.fontPurple01Color : CustomTheme.darkPurpleColor, size: item.size,))),
    Expanded(child: Text(item.label, style: TextStyle(color: index == currentIndex ? CustomTheme.fontPurple01Color : CustomTheme.darkPurpleColor, fontSize: 12))),
  ],),),);
}

class BarItemData {
  String label;
  String iconPathName;
  double size;
  BarItemData({required this.label, required this.iconPathName, required this.size});
}