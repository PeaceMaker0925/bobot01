import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import '../../constants/theme.dart';
import '../../repository/api_response.dart';
import '../../constants/public_component.dart';
import '../../constants/width_infinty_button.dart';
import '../home/model/ForgetPasswordParameter.dart';
import '../navigation_viewcontroller.dart';
import 'login_viewmodel.dart';

//ForgetPWVerifyCodeScreen

class SetNewPWScreen extends ConsumerWidget {
  SetNewPWScreen({Key? key}) : super(key: key);

  TextEditingController textController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    print(ref.watch(accountProvider));
    print(ref.watch(passwordProvider));
    return buildFlexLayout(
        context,
        Column(
            children: [
              AppBar(
                elevation: 0,
                backgroundColor: CustomTheme.appBarGrayColor,
                title: const Text(
                  'Set New Password',
                  style: TextStyle(
                      fontSize: 20, fontWeight: FontWeight.w500, color: Colors.white, fontFamily: 'ubuntu'),
                ),
              ),
              Expanded(
                  flex: 3,
                  child: Column(
                      children: [
                        Form(
                          key: _formKey,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Padding(
                                  padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
                                  child: Text("New Password", style: TextStyle(color: Colors.white,fontSize: 18,fontFamily: 'ubuntu'),)
                              ),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
                                child:
                                TextFormField(
                                  controller: textController,
                                    validator: (value) {
                                      if (value == null || value.isEmpty) {
                                        return 'Please enter some text';
                                      }
                                      return null;
                                    },
                                    style: TextStyle(color: Colors.white),
                                    decoration: InputDecoration(
                                      hintText: 'Please enter your new password',
                                      hintStyle: const TextStyle(color: CustomTheme.labelWhiteColor),
                                      enabledBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10),
                                        borderSide: const BorderSide(
                                            width: 3, color: CustomTheme.labelWhiteColor),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10),
                                        borderSide: const BorderSide(
                                            width: 3, color: CustomTheme.labelWhiteColor),
                                      ),
                                      focusedErrorBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10),
                                        borderSide: const BorderSide(
                                            width: 3, color: CustomTheme.primaryRedColor),
                                      ),
                                      errorBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10),
                                        borderSide: const BorderSide(
                                            width: 3, color: CustomTheme.primaryRedColor),
                                      ),
                                    )
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(16, 16, 16, 0),
                                child:
                                WidthInfintyButton(
                                  onTap: () async {
                                    if(_formKey.currentState!.validate()){
                                      ref.watch(forgetPWProvider(ForgetPasswordParameter(
                                          password: textController.text, registerMode: 'MAIL', email: ref.watch(accountProvider), country: '', phone: ''
                                      )).future).then((event) {
                                        event.then((value) {
                                          print("${(value as ApiResponse).message}");

                                          Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => NavigationScreen()));

                                        });
                                      });
                                    }
                                  },
                                  fontColor: Colors.white,
                                  foregroundColor: CustomTheme.fontPurple01Color,
                                  fontSize: 18,
                                  text: "Continue",
                                ),
                              ),
                              const Padding(
                                  padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
                                  child: Text(
                                    "By clicking Continue, you agree to our Terms of Service.",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 12,
                                        fontFamily: 'ubuntu'
                                    ),
                                  )
                              ),
                            ],
                          ),
                        )
                      ])
              ),
              Expanded(flex: 5, child: Container())
            ]
        )
    );
  }

}
