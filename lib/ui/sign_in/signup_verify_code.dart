import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import '../../constants/theme.dart';
import '../../repository/api_response.dart';
import '../../constants/public_component.dart';
import '../../constants/width_infinty_button.dart';
import '../home/model/CheckVerifyCodeParameter.dart';
import '../home/model/SignUpParameter.dart';
import '../navigation_viewcontroller.dart';
import 'login_viewmodel.dart';

//ForgetPWVerifyCodeScreen

class SignUpVerifyCodeScreen extends ConsumerWidget {
  SignUpVerifyCodeScreen({Key? key}) : super(key: key);

  TextEditingController textController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    print(ref.watch(accountProvider));
    print(ref.watch(passwordProvider));
    return buildFlexLayout(
        context,
        Column(
            children: [
              AppBar(
                elevation: 0,
                backgroundColor: CustomTheme.appBarGrayColor,
                title: const Text(
                  'Verify',
                  style: TextStyle(
                      fontSize: 20, fontWeight: FontWeight.w500, color: Colors.white, fontFamily: 'ubuntu'),
                ),
              ),
              Expanded(
                  flex: 3,
                  child: Column(
                      children: [
                        Form(
                          key: _formKey,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Padding(
                                  padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
                                  child: Text("Verification code", style: TextStyle(color: Colors.white,fontSize: 18,fontFamily: 'ubuntu'),)
                              ),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
                                child:
                                TextFormField(
                                    controller: textController,
                                    validator: (value) {
                                      if (value == null || value.isEmpty) {
                                        return 'Please enter some text';
                                      }
                                      return null;
                                    },
                                    style: TextStyle(color: Colors.white),
                                    decoration: InputDecoration(
                                      hintText: 'Please enter your verification code',
                                      hintStyle: const TextStyle(color: CustomTheme.labelWhiteColor),
                                      enabledBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10),
                                        borderSide: const BorderSide(
                                            width: 3, color: CustomTheme.labelWhiteColor),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10),
                                        borderSide: const BorderSide(
                                            width: 3, color: CustomTheme.labelWhiteColor),
                                      ),
                                      focusedErrorBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10),
                                        borderSide: const BorderSide(
                                            width: 3, color: CustomTheme.primaryRedColor),
                                      ),
                                      errorBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10),
                                        borderSide: const BorderSide(
                                            width: 3, color: CustomTheme.primaryRedColor),
                                      ),
                                    )
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(16, 16, 16, 0),
                                child:
                                WidthInfintyButton(
                                  onTap: () async {
                                    if(_formKey.currentState!.validate()){
                                      ref.watch(checkVerifyProvider(CheckVerifyCodeParameter(
                                          verifyCode: textController.text, action: 'register', registerMode: 'MAIL', email: ref.watch(accountProvider), country: '', phone: ''
                                      )).future).then((event) {
                                        event.then((value) {
                                          print("${(value as ApiResponse).message}");
                                          if(value.message.compareTo("SUCCESS") == 0) {
                                            var params = SignUpParameter(memberId: value.data,
                                                registerMode: ref.watch(signUpInfoProvider).registerMode,
                                                email: ref.watch(signUpInfoProvider).email, country: ref.watch(signUpInfoProvider).country, phone: ref.watch(signUpInfoProvider).phone,
                                                password: ref.watch(signUpInfoProvider).password, name: "", inviteCode: ref.watch(signUpInfoProvider).inviteCode, imageUrl: "");
                                            ref.watch(signUpProvider(params).future).then((event) {event.then((value) {
                                              Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => NavigationScreen()), (Route<dynamic> route) => false);
                                            });});
                                          }
                                        });
                                      });
                                    }
                                  },
                                  fontColor: Colors.white,
                                  foregroundColor: CustomTheme.fontPurple01Color,
                                  fontSize: 18,
                                  text: "Continue",
                                ),
                              ),
                              const Padding(
                                  padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
                                  child: Text(
                                    "By clicking Continue, you agree to our Terms of Service.",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 12,
                                        fontFamily: 'ubuntu'
                                    ),
                                  )
                              ),
                            ],
                          ),
                        )
                      ])
              ),
              Expanded(flex: 5, child: Container())
            ]
        )
    );
  }

}
