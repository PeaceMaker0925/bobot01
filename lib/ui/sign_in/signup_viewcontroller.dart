import 'package:bobot01/ui/sign_in/signup_pw_viewcontroller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_fonts/google_fonts.dart';
import '../../constants/SignInWidgets.dart';
import '../../constants/circleStepView.dart';
import '../../constants/constants.dart';
import '../../constants/theme.dart';
import '../../constants/public_component.dart';
import '../../constants/width_infinty_button.dart';
import '../../main.dart';
import '../../repository/api_response.dart';
import '../home/model/CheckSignupParameter.dart';
import '../home/model/CheckVerifyCodeParameter.dart';
import '../home/model/SignUpParameter.dart';
import 'login_viewmodel.dart';

class SignUpScreen extends ConsumerStatefulWidget {
  SignUpScreen({Key? key}) : super(key: key);
  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _SignUpScreenState();
}

class _SignUpScreenState extends ConsumerState<SignUpScreen> {
  TextEditingController emailTextController = TextEditingController();
  TextEditingController verifyCodeTextController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  AsyncValue asyncResponse = const AsyncValue.loading();
  bool isLoading = false;
  bool isNextEnable = false;
  bool isEmailFormatWrong = false;
  bool isVerifyCodeWrong = false;
  bool isVerifyCodeRight = false;
  String memberId = "";
  FocusNode accountFocusNode = FocusNode();
  FocusNode verifyCodeFocusNode = FocusNode();

  @override
  void initState() {
    emailTextController.addListener(() {
      RegExp regEx = RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
      isEmailFormatWrong = !regEx.hasMatch(emailTextController.text) || emailTextController.text.contains(' ');
      setState(() {});
      ref.read(accountProvider.notifier).setAccount(emailTextController.text);
    });
  }

  void unFocusAllNode(){
    accountFocusNode.unfocus();
    verifyCodeFocusNode.unfocus();
  }

  @override
  Widget build(BuildContext context) {
    ref.listen(isLoadingProvider,(_,__){
      setState(() {
        isLoading = __;
      });
    });
    return buildFlexLayout(context, InkWell(onTap: () { unFocusAllNode(); }, child: Container(
      margin: EdgeInsets.symmetric(horizontal: Constant().getViewWidth(context)/20),
      child: Column(
        children: [
          Spacer(),
          Expanded(flex: 2, child: Container(alignment: Alignment.centerLeft, child: Row(children: [
            Expanded(flex: 1, child: CircleButton(0, ref)),
            Expanded(flex: 4, child: Container())
          ],))),
          Expanded(flex: 4, child: Container(color: Colors.transparent, alignment: Alignment.center, child: Column(children: [
            Expanded(flex: 2, child: Container(height: Constant().getViewHeight(context)/8, child: Image.asset('assets/icon.png'),)),
            Expanded(flex: 1, child: Text('Bobot', style: GoogleFonts.balooTamma(
              textStyle: TextStyle(fontSize: 26, color: CustomTheme.fontPurple01Color),
            ),))
          ],),)),
          Expanded(flex: 4, child: CircleStepView(step: 1)),
          Expanded(flex: 2, child: SignInTextField(emailTextController, GestureDetector(child: Container(
            margin: EdgeInsets.all(Constant().getViewHeight(context)/120),
            padding: EdgeInsets.all(Constant().getViewHeight(context)/80),
            decoration: BoxDecoration(
              color: CustomTheme.fontPurple01Color,
              borderRadius: BorderRadius.all(Radius.circular(35)),
            ),
            child: Text("Send", style: CustomTheme.textPrimary,),
          ), onTap: (){ checkSignUpEmail(); },), accountFocusNode, "Please_enter_your_email")),
          Expanded(flex: 1, child: Container(
            margin: EdgeInsets.symmetric(horizontal: Constant().getViewHeight(context)/40),
            child: Row(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.start, children: [if(isEmailFormatWrong)Text('Email Format Error', style: TextStyle(
              color: CustomTheme.fontRed01Color,)),],),
          )),
          Expanded(flex: 2, child: SignInTextField(verifyCodeTextController, GestureDetector(child: Container(
            margin: EdgeInsets.all(Constant().getViewHeight(context)/120),
            padding: EdgeInsets.all(Constant().getViewHeight(context)/80),
            decoration: BoxDecoration(
              color: CustomTheme.fontPurple01Color,
              borderRadius: BorderRadius.all(Radius.circular(35)),
            ),
            child: Text("Verify", style: CustomTheme.textPrimary,),
          ), onTap: (){ checkVerifyCode(); },), verifyCodeFocusNode, "Please_enter_email_verify_code")),
          Expanded(flex: 1, child: Container(
            margin: EdgeInsets.symmetric(horizontal: Constant().getViewHeight(context)/40),
            child: Row(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.start, children: [
              if(isVerifyCodeWrong)Text('VerifyCode Error or Expired', style: TextStyle(color: CustomTheme.fontRed01Color,)),
              if(isVerifyCodeRight)Text('VerifyCode Correct!', style: TextStyle(color: CustomTheme.fontGreen01Color,)),
            ],),
          )),
          Expanded(flex: 6, child: Container()),
          Expanded(flex: 2, child: WidthInfintyButton(
            onTap: () { pressNextBtn(); },
            fontColor: Colors.white,
            foregroundColor: CustomTheme.fontPurple01Color,
            fontSize: 18,
            text: "Next_Step",
          ),),
          Expanded(flex: 2, child: Container()),
        ]))), isLoading: isLoading);
  }

  void sendVerifyCode() {
    // ref.read(isLoadingProvider.notifier).setLoading(true);
    ref.read(signUpVerifyProvider(emailTextController.text).future).then((event){event.then((value) {
      // print("${(value as ApiResponse).message}");
      ref.read(isLoadingProvider.notifier).setLoading(false);
      // Navigator.push(context, MaterialPageRoute(builder: (context) => VerifyCodeScreen()));
      if ((value as ApiResponse).message == 'SUCCESS') {
        showResponseDialog(context, "Verify Code has been sent to your email.", (){});
      }else {
        showResponseDialog(context, "${(value).url}\n\n${(value).code}\n\n${(value).message}", (){});
      }
    });},);
  }
  void checkVerifyCode() {
    ref.read(isLoadingProvider.notifier).setLoading(true);
    ref.watch(checkVerifyProvider(CheckVerifyCodeParameter(
        verifyCode: verifyCodeTextController.text, action: 'register', registerMode: 'MAIL', email: emailTextController.text, country: '', phone: ''
    )).future).then((event) {
      event.then((value) {
        ref.read(isLoadingProvider.notifier).setLoading(false);
        print("${(value as ApiResponse).message}");
        if(value.message.compareTo("SUCCESS") == 0) {
          isNextEnable = true;
          memberId = (value).data;
          isVerifyCodeRight = true;
          isVerifyCodeWrong = false;
          setState(() {});
          // showResponseDialog(context, "${(value).url}\n\n${(value).code}\n\n${(value).message}", (){});
          // var params = SignUpParameter(memberId: value.data,
          //     registerMode: ref.watch(signUpInfoProvider).registerMode,
          //     email: ref.watch(signUpInfoProvider).email, country: ref.watch(signUpInfoProvider).country, phone: ref.watch(signUpInfoProvider).phone,
          //     password: ref.watch(signUpInfoProvider).password, name: "", inviteCode: ref.watch(signUpInfoProvider).inviteCode, imageUrl: "");
          // ref.watch(signUpProvider(params).future).then((event) {event.then((value) {
          //   Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => NavigationScreen()), (Route<dynamic> route) => false);
          // });});
        }
        else if (value.message == 'VERIFY_CODE_ERROR' || value.message == 'VERIFY_CODE_EXPIRED' || value.message == 'VALID_ERROR'){
          isVerifyCodeWrong = true;
          isVerifyCodeRight = false;
          setState(() {});
        }
        else {isVerifyCodeWrong = false; isVerifyCodeRight = false; showResponseDialog(context, "${(value).url}\n\n${(value).code}\n\n${(value).message}", (){});}
      });
    });
  }

  void checkSignUpEmail() {
    ref.read(isLoadingProvider.notifier).setLoading(true);
    ref.watch(checkSignUpProvider(CheckSignUpParameter(
        name: "email", param: emailTextController.text
    )).future).then((event) {
      event.then((value) {
        ref.read(isLoadingProvider.notifier).setLoading(false);
        if(value.message.compareTo("SUCCESS") == 0){
          sendVerifyCode();
        }
        else if (value.message == 'ALREADY_HAVE_EMAIL_ERROR'){
          isVerifyCodeWrong = false;
          setState(() {});
          showResponseDialog(context, title: "Oops!", "This email already has account.", (){ });
        }
        else {
          showResponseDialog(context, "${(value).url}\n\n${(value).code}\n\n${(value).message}", (){});
        }

      });
    });
  }

  void pressNextBtn() {
    ref.watch(signUpInfoProvider.notifier).setSignUpInfo(SignUpParameter(memberId: memberId,
        registerMode: "MAIL", email: emailTextController.text, country: "", phone: "",
        password: "",
        name: "", inviteCode: "", imageUrl: ""));
    if(isNextEnable){
      ref.watch(accountProvider.notifier).setAccount(emailTextController.text);
      Navigator.push(navigatorKey.currentContext!, MaterialPageRoute(builder: (context) => SignUpPWScreen()));
    }

  }
}
