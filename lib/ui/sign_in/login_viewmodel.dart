import 'package:bobot01/repository/api_response.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:uuid/uuid.dart';
import 'dart:io';
import '../../constants/shared_preference.dart';
import '../../repository/user_api_repository.dart';
import '../home/model/CheckSignupParameter.dart';
import '../home/model/CheckVerifyCodeParameter.dart';
import '../home/model/DeviceInfoParameter.dart';
import '../home/model/ForgetPasswordParameter.dart';
import '../home/model/LoginParameter.dart';
import '../home/model/SignUpParameter.dart';

final loginProvider = FutureProvider.autoDispose.family<Future<ApiResponse>, LoginParameter>((ref, loginParameter) async {
  return UserApiRepositoryImpl().login(loginParameter.account, loginParameter.password, loginParameter.verifyCode);
});

final loginVerifyProvider = FutureProvider.autoDispose.family<Future<ApiResponse>, String>((ref, account) async {
  return UserApiRepositoryImpl().getLoginVerifyCode(account);
});

final logOutProvider = FutureProvider.autoDispose<Future<ApiResponse>>((ref) async {
  return UserApiRepositoryImpl().logout();
});

final editMemberInfoProvider = FutureProvider.autoDispose<Future<ApiResponse>>((ref) async {
  return UserApiRepositoryImpl().editMemberInfo();
});

final checkSignUpProvider = FutureProvider.autoDispose.family<Future<ApiResponse>, CheckSignUpParameter>((ref, parameter)  async {
  return UserApiRepositoryImpl().checkSignup(parameter);
});

final saveDeviceInfoProvider = FutureProvider.autoDispose.family<Future<ApiResponse>, DeviceInfoParameter>((ref, deviceInfoParameter) async {
  return UserApiRepositoryImpl().saveDeviceInfo(deviceInfoParameter);
});

final forgetPWVerifyProvider = FutureProvider.autoDispose.family<Future<ApiResponse>, String>((ref, account) async {
  return UserApiRepositoryImpl().getForgetPswVerifyCode(account);
});

final signUpVerifyProvider = FutureProvider.autoDispose.family<Future<ApiResponse>, String>((ref, account) async {
  return UserApiRepositoryImpl().getSignUpVerifyCode(account);
});

final checkVerifyProvider = FutureProvider.autoDispose.family<Future<ApiResponse>, CheckVerifyCodeParameter>((ref, parameter) async {
  return UserApiRepositoryImpl().checkVerifyCode(parameter);
});

final forgetPWProvider = FutureProvider.autoDispose.family<Future<ApiResponse>, ForgetPasswordParameter>((ref, parameter) async {
  return UserApiRepositoryImpl().forgetPassword(parameter);
});

final signUpProvider = FutureProvider.autoDispose.family<Future<ApiResponse>, SignUpParameter>((ref, parameter) async {
  return UserApiRepositoryImpl().signup(parameter);
});
/// functions

Future<DeviceInfoParameter?> generateDeviceInfo() async {
  late DeviceInfoParameter parameter;
  DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();

  var uuid = const Uuid();
  var newUuid = '';
  BobotSharedPreferences().getDeviceToken().then((value) {
    if(value != 'deviceToken'){
      newUuid = value;
    } else {
      newUuid = uuid.v1();
      BobotSharedPreferences().saveDeviceToken(newUuid);
    }
  });

  try{
    if (Platform.isAndroid) {
      AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      parameter = DeviceInfoParameter(
          uuid: uuid.v1(),
          language: 'zh-TW',
          device: "Android",
          deviceInfo: "Android " + androidInfo.version.release.toString(),
          deviceToken: newUuid,
      );
      return parameter;
    } else if (Platform.isIOS) {
      IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      parameter = DeviceInfoParameter(
          uuid: iosInfo.identifierForVendor ?? "",
          language: 'zh-TW',
          device: "iOS",
          deviceInfo: iosInfo.systemVersion.toString(),
          deviceToken: newUuid,
      );
      return parameter;
    }
  } on PlatformException { print('Failed to get platform info'); }
  return null;
}

/// Holding some variables in Cache
final deviceInfoProvider = StateNotifierProvider<DeviceInfoNotifier, DeviceInfoParameter>((ref) {
  return DeviceInfoNotifier(ref);
});
class DeviceInfoNotifier extends StateNotifier<DeviceInfoParameter> {
  DeviceInfoNotifier(this.ref): super(const DeviceInfoParameter(uuid: '', language: '', device: '', deviceInfo: '', deviceToken: '',));
  final Ref ref;
  Future<void> setDeviceInfo(DeviceInfoParameter parameter) async {
    state = parameter;
  }
}

final bodyIndexProvider = StateNotifierProvider<BodyIndexNotifier, int>((ref) {
  return BodyIndexNotifier(ref);
});
class BodyIndexNotifier extends StateNotifier<int> {
  BodyIndexNotifier(this.ref): super(0);
  final Ref ref;
  Future<void> setBodyIndex(int parameter) async {
    state = parameter;
  }
}

final showToLogInProvider = StateNotifierProvider<ToLogInNotifier, bool>((ref) {
  return ToLogInNotifier(ref);
});
class ToLogInNotifier extends StateNotifier<bool> {
  ToLogInNotifier(this.ref): super(false);
  final Ref ref;
  Future<void> setShowToLogIn(bool parameter) async {
    state = parameter;
  }
}

final isLoadingProvider = StateNotifierProvider<LoadingNotifier, bool>((ref) {
  return LoadingNotifier(ref);
});
class LoadingNotifier extends StateNotifier<bool> {
  LoadingNotifier(this.ref): super(false);
  final Ref ref;
  Future<void> setLoading(bool parameter) async {
    state = parameter;
  }
}

final signUpInfoProvider = StateNotifierProvider<SignUpNotifier, SignUpParameter>((ref) {
  return SignUpNotifier(ref);
});
class SignUpNotifier extends StateNotifier<SignUpParameter> {
  SignUpNotifier(this.ref): super(const SignUpParameter(memberId: '', registerMode: '', email: '', country: '', phone: '', password: '', name: '', inviteCode: '', imageUrl: ''));
  final Ref ref;
  Future<void> setSignUpInfo(SignUpParameter parameter) async {
    state = parameter;
  }
}

final accountProvider = StateNotifierProvider<AccountNotifier, String>((ref) {
  return AccountNotifier(ref);
});
class AccountNotifier extends StateNotifier<String> {
  AccountNotifier(this.ref): super('');
  final Ref ref;
  Future<void> setAccount(String account) async {
    state = account;
  }
}

final passwordProvider = StateNotifierProvider<PasswordNotifier, String>((ref) {
  return PasswordNotifier(ref);
});
class PasswordNotifier extends StateNotifier<String> {
  PasswordNotifier(this.ref): super('');
  final Ref ref;
  Future<void> setPassword(String password) async {
    state = password;
  }
}