import 'package:bobot01/repository/api_response.dart';
import 'package:bobot01/ui/sign_in/signup_viewcontroller.dart';
import 'package:bobot01/ui/sign_in/verify_code_viewcontroller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:lottie/lottie.dart';
import '../../constants/SignInWidgets.dart';
import '../../constants/constants.dart';
import '../../constants/shared_preference.dart';
import '../../constants/theme.dart';
import '../../constants/public_component.dart';
import '../../constants/width_infinty_button.dart';
import '../../main.dart';
import '../home/model/LoginParameter.dart';
import '../navigation_viewcontroller.dart';
import '../profile/profile_viewmodel.dart';
import 'forget_password_viewcontroller.dart';
import 'login_viewmodel.dart';

class LoginScreen extends ConsumerStatefulWidget {
  LoginScreen({Key? key}) : super(key: key);
  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _LoginScreenState();
}

class _LoginScreenState extends ConsumerState<LoginScreen> {
  TextEditingController emailTextController = TextEditingController();
  TextEditingController passwordTextController = TextEditingController();
  TextEditingController verifyCodeTextController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  AsyncValue asyncResponse = const AsyncValue.loading();

  bool isLoading = false;
  bool isEmailFormatWrong = false;
  bool isAccountWrong = false;
  bool isPasswordWrong = false;
  bool isVerifyCodeWrong = false;
  bool isPasswordShow = true;
  FocusNode accountFocusNode = FocusNode();
  FocusNode passwordFocusNode = FocusNode();
  FocusNode verifyCodeFocusNode = FocusNode();

  @override
  void initState() {
    super.initState();
    emailTextController.addListener(() {
      RegExp regEx = RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
      isEmailFormatWrong = !regEx.hasMatch(emailTextController.text) || emailTextController.text.contains(' ');
      setState(() {});
      ref.read(accountProvider.notifier).setAccount(emailTextController.text);
    });

    passwordTextController.addListener(() {
      // print(passwordTextController.text);
      RegExp regEx = RegExp(r"(?=.*[a-z])(?=.*[A-Z])\w+");
      isPasswordWrong = !regEx.hasMatch(passwordTextController.text) || passwordTextController.text.contains(' ') || !pwLengthValid(passwordTextController.text);
      setState(() {});
      ref.read(passwordProvider.notifier).setPassword(passwordTextController.text);
    });

  }

  void unFocusAllNode(){
    accountFocusNode.unfocus();
    passwordFocusNode.unfocus();
    verifyCodeFocusNode.unfocus();
  }

  @override
  Widget build(BuildContext context) {
    ref.listen(isLoadingProvider,(_,__){
      setState(() {
        isLoading = __;
      });
    });
    return buildFlexLayout(context, Stack(children: [
        Container(alignment: Alignment.topLeft, color: Colors.white, child: Container(height: Constant().getViewHeight(context),width: Constant().getViewWidth(context), child: Lottie.asset(
              'assets/bobot_bg_01.json',
              width: Constant().getViewWidth(context),
              height: Constant().getViewHeight(context),
              alignment: Alignment.center,
              animate: true,
              onLoaded: (composition) {
              },
              fit: BoxFit.cover
        ),
    )), InkWell(onTap: () { unFocusAllNode(); }, child: Container(
      margin: EdgeInsets.symmetric(horizontal: Constant().getViewWidth(context)/20),
      child: Column(
        children: [
          Spacer(),
          Expanded(flex: 2, child: Container(alignment: Alignment.centerLeft, child: Row(children: [
            Expanded(flex: 1, child: GestureDetector(
                onTap: (){
                  Navigator.pushReplacement(navigatorKey.currentContext!, MaterialPageRoute(builder: (context) => NavigationScreen()));
                  ref.read(bodyIndexProvider.notifier).setBodyIndex(0);
                },
                child: Container(
                    width: Constant().getViewHeight(context)/14,
                    child: Image.asset("assets/btn/btn_back_01_nor.png")
                )
            )),
            Expanded(flex: 4, child: Container())
          ],))),
          Expanded(flex: 6, child: Container(color: Colors.transparent, alignment: Alignment.center, child: Column(children: [
            Expanded(flex: 2, child: Container(height: Constant().getViewHeight(context)/8, child: Image.asset('assets/icon.png'),)),
            Expanded(flex: 1, child: Text('Bobot', style: GoogleFonts.balooTamma(
              textStyle: TextStyle(fontSize: 46, color: CustomTheme.fontPurple01Color),
            ),))
          ],),)),
          Expanded(flex: 2, child: Container()),
          Expanded(flex: 2, child: SignInTextField(emailTextController, GestureDetector(child: Container(
            margin: EdgeInsets.all(Constant().getViewHeight(context)/120),
            padding: EdgeInsets.all(Constant().getViewHeight(context)/80),
            decoration: BoxDecoration(
              color: CustomTheme.fontPurple01Color,
              borderRadius: BorderRadius.all(Radius.circular(35)),
            ),
            child: Text("Send", style: CustomTheme.textPrimary,),
          ), onTap: (){ sendVerifyCode(); },), accountFocusNode, "Please enter your email")),
          Expanded(flex: 1, child: Container(
            margin: EdgeInsets.symmetric(horizontal: Constant().getViewHeight(context)/40),
            child: Row(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.start, children: [if(isEmailFormatWrong)Text('Email Format Error', style: TextStyle(
              color: CustomTheme.fontRed01Color,)),],),
          )),
          Expanded(flex: 2, child: SignInTextField(passwordTextController, GestureDetector(child: Container(
            child: Icon(isPasswordShow ? Icons.remove_red_eye_outlined : Icons.visibility_off_outlined, color: CustomTheme.fontPurple01Color, size: 30,),
          ), onTap: (){ setState(() {
            isPasswordShow = !isPasswordShow;
          }); },), passwordVisible: isPasswordShow, passwordFocusNode, "Please enter your password")),
          Expanded(flex: 1, child: Container(
            margin: EdgeInsets.symmetric(horizontal: Constant().getViewHeight(context)/40),
            child: Row(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
              if(isPasswordWrong)Text('Password or Account wrong', style: TextStyle(
                color: CustomTheme.fontRed01Color,)),
              Container(),
              GestureDetector(child:  Text('Forget Password', style: TextStyle(
                color: CustomTheme.fontPurple01Color,)),onTap: (){
                Navigator.push(navigatorKey.currentContext!, MaterialPageRoute(builder: (context) => ForgetPasswordScreen()));
              },)
            ],),
          )),
          Expanded(flex: 2, child: SignInTextField(verifyCodeTextController, Container(), verifyCodeFocusNode, "Please enter email verify code")),
          Expanded(flex: 1, child: Container(
            margin: EdgeInsets.symmetric(horizontal: Constant().getViewHeight(context)/40),
            child: Row(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.start, children: [
              if(isVerifyCodeWrong)Text('VerifyCode Error or Expired', style: TextStyle(
              color: CustomTheme.fontRed01Color,)),],),
          )),
          Expanded(flex: 3, child: Container()),
          Expanded(flex: 2, child: WidthInfintyButton(
            onTap: () {
              // showResponseDialog(context, "test", (){ Navigator.maybePop(navigatorKey.currentContext!); });
              pressLogInBtn(context);
              },
            fontColor: Colors.white,
            foregroundColor: CustomTheme.fontPurple01Color,
            fontSize: 18,
            text: "Log in",
          ),),

          Expanded(flex: 2, child: Container(
            child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              Text('No Account?', style: TextStyle(
                color: CustomTheme.fontPurple02Color,)),
              Container(width: Constant().getViewWidth(context)/40,),
              GestureDetector(child:  Text('Sign In', style: TextStyle(
                color: CustomTheme.fontPurple01Color,
                  decoration: TextDecoration.underline,
                fontWeight: FontWeight.w700
              ),), onTap: (){
                Navigator.push(navigatorKey.currentContext!, MaterialPageRoute(builder: (context) => SignUpScreen()));
              },)
            ],),
          )),
        ]
    ),))]), isLoading: isLoading);
  }
  void getUserInfoThenNext(BuildContext context) {
    ref.read(userInfoProvider.future).then((event) {
      event.then((value) {
        BobotSharedPreferences().saveMemberId(value.data["memberId"]);
        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => NavigationScreen()));
        ref.read(bodyIndexProvider.notifier).setBodyIndex(0);
      });
    });
  }
  void sendVerifyCode() {
    ref.read(isLoadingProvider.notifier).setLoading(true);
    ref.read(loginVerifyProvider(emailTextController.text).future).then((event){event.then((value) {
    // print("${(value as ApiResponse).message}");
    ref.read(isLoadingProvider.notifier).setLoading(false);
    // Navigator.push(context, MaterialPageRoute(builder: (context) => VerifyCodeScreen()));
    if ((value as ApiResponse).message == 'SUCCESS') {
      showResponseDialog(context, "Verify Code has been sent to your email.", (){});
    }else if((value).message == 'ACCOUNT_NOT_EXIST'){
      showResponseDialog(context, "This email had not been sign up.", (){});
    }else {
      showResponseDialog(context, "${(value).url}\n\n${(value).code}\n\n${(value).message}", (){});
    }

    });},);
  }

  void pressLogInBtn(BuildContext context) {
    if(emailTextController.text.isEmpty || passwordTextController.text.isEmpty || verifyCodeTextController.text.isEmpty){
      showResponseDialog(context, title: "Oops!", "Please enter the correct content.", (){});
      return;
    }
    ref.read(isLoadingProvider.notifier).setLoading(true);
    ref.refresh(loginProvider(LoginParameter(
        account: emailTextController.text,
        password: passwordTextController.text,
        verifyCode: verifyCodeTextController.text
    )).future).then((event) {
      event.then((value) {
        print("${(value as ApiResponse).message}");
        ref.read(isLoadingProvider.notifier).setLoading(false);
        if ((value as ApiResponse).message == 'SUCCESS') {
          final String token = (value as ApiResponse).data['token'];
          BobotSharedPreferences().saveUserToken(token);
          BobotSharedPreferences().saveCurrentAccount(ref.watch(accountProvider));
          ///GET USER INFO (MEMBER ID) THEN GO TO HOME PAGE.
          getUserInfoThenNext(context);
          }
        else if (value.message == 'ACCOUNT_NOT_EXIST' || value.message == 'ACCOUNT_STATUS_DISABLE'|| value.message == 'PASSWORD_ERROR') {
          isPasswordWrong = true;
          isVerifyCodeWrong = false;
          setState(() {});
        }
        else if (value.message == 'VERIFY_CODE_ERROR' || value.message == 'VERIFY_CODE_EXPIRED'){
          isVerifyCodeWrong = true;
          isPasswordWrong = false;
          setState(() {});
        }
        else {
          isVerifyCodeWrong = false;
          isPasswordWrong = false;
          setState(() {});
          showResponseDialog(context, "${(value as ApiResponse).message}\n${(value as ApiResponse).data}", (){ });
        }
      });
    });
  }

}

