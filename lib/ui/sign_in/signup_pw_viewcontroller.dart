import 'package:bobot01/ui/sign_in/signup_nick_viewcontroller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_fonts/google_fonts.dart';
import '../../constants/SignInWidgets.dart';
import '../../constants/circleStepView.dart';
import '../../constants/constants.dart';
import '../../constants/theme.dart';
import '../../constants/public_component.dart';
import '../../constants/width_infinty_button.dart';
import '../../main.dart';
import 'login_viewmodel.dart';

class SignUpPWScreen extends ConsumerStatefulWidget {
  SignUpPWScreen({Key? key}) : super(key: key);
  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _SignUpPWScreenState();
}

class _SignUpPWScreenState extends ConsumerState<SignUpPWScreen> {
  TextEditingController pwTextController = TextEditingController();
  TextEditingController pwConfirmTextController = TextEditingController();
  AsyncValue asyncResponse = const AsyncValue.loading();
  bool isLoading = false;
  bool isNextEnable = false;
  bool isPasswordShow = true;
  bool isPasswordWrong = false;
  bool isPasswordDiff = false;
  FocusNode pwFocusNode = FocusNode();
  FocusNode pwConfirmFocusNode = FocusNode();

  @override
  void initState() {
    pwTextController.addListener(() {
      RegExp regEx = RegExp(r"(?=.*[a-z])(?=.*[A-Z])\w+");
      isPasswordWrong = !regEx.hasMatch(pwTextController.text) || pwTextController.text.contains(' ') || !pwLengthValid(pwTextController.text);
      setState(() {});
    });

    pwConfirmTextController.addListener(() {
      isPasswordDiff = pwConfirmTextController.text != pwTextController.text;
      setState(() {

      });
    });
  }


  void unFocusAllNode(){
    pwFocusNode.unfocus();
    pwConfirmFocusNode.unfocus();
  }


  @override
  Widget build(BuildContext context) {

    return buildFlexLayout(context, InkWell(onTap: () { unFocusAllNode(); }, child: Container(
        margin: EdgeInsets.symmetric(horizontal: Constant().getViewWidth(context)/20),
        child: Column(
            children: [
              Spacer(),
              Expanded(flex: 2, child: Container(alignment: Alignment.centerLeft, child: Row(children: [
                Expanded(flex: 1, child: CircleButton(0, ref)),
                Expanded(flex: 4, child: Container())
              ],))),
              Expanded(flex: 4, child: Container(color: Colors.transparent, alignment: Alignment.center, child: Column(children: [
                Expanded(flex: 2, child: Container(height: Constant().getViewHeight(context)/8, child: Image.asset('assets/icon.png'),)),
                Expanded(flex: 1, child: Text('Bobot', style: GoogleFonts.balooTamma(
                  textStyle: TextStyle(fontSize: 26, color: CustomTheme.fontPurple01Color),
                ),))
              ],),)),
              Expanded(flex: 4, child: CircleStepView(step: 2)),
              Expanded(flex: 2, child: SignInTextField(pwTextController, GestureDetector(child: Container(
                child: Icon(isPasswordShow ? Icons.remove_red_eye_outlined : Icons.visibility_off_outlined, color: CustomTheme.fontPurple01Color, size: 30,),
              ), onTap: (){ setState(() {
                isPasswordShow = !isPasswordShow;
              }); },), passwordVisible: isPasswordShow, pwFocusNode, "Please_enter_your_password")),
              Expanded(flex: 1, child: Container(child: Opacity(opacity: isPasswordWrong ? 1 : 0, child: Text('Password Format Error', style: TextStyle(
                color: CustomTheme.fontRed01Color,))),),),
              Expanded(flex: 2, child: SignInTextField(pwConfirmTextController, GestureDetector(child: Container(
                child: Icon(isPasswordShow ? Icons.remove_red_eye_outlined : Icons.visibility_off_outlined, color: CustomTheme.fontPurple01Color, size: 30,),
              ), onTap: (){ setState(() {
                isPasswordShow = !isPasswordShow;
              }); },), passwordVisible: isPasswordShow, pwConfirmFocusNode, "Please_enter_your_password")),
              Expanded(flex: 1, child: Container(child: Opacity(opacity: isPasswordDiff ? 1 : 0, child: Text('Password Not Match', style: TextStyle(
                color: CustomTheme.fontRed01Color,))),),),
              Expanded(flex: 1, child: Container(alignment: Alignment.centerLeft, child: Text("您的密碼須包含：", style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 14)),)),
              Expanded(flex: 1, child: Container(alignment: Alignment.centerLeft, child: Text("密碼須包含大小寫英文字母", style: TextStyle(color: CustomTheme.fontPurple01Color, fontSize: 14)),)),
              Expanded(flex: 1, child: Container(alignment: Alignment.centerLeft, child: Text("密碼須包含數字", style: TextStyle(color: CustomTheme.fontPurple01Color, fontSize: 14)),)),
              Expanded(flex: 1, child: Container(alignment: Alignment.centerLeft, child: Text("密碼長度在8-20之內", style: TextStyle(color: CustomTheme.fontRed01Color, fontSize: 14)),)),
              Expanded(flex: 1, child: Container(alignment: Alignment.centerLeft, child: Text("重複密碼與密碼不相同", style: TextStyle(color: CustomTheme.fontRed01Color, fontSize: 14)),)),
              Expanded(flex: 1, child: Container()),
              Expanded(flex: 2, child: WidthInfintyButton(
                onTap: () { pressNextBtn(); },
                fontColor: Colors.white,
                foregroundColor: CustomTheme.fontPurple01Color,
                fontSize: 18,
                text: "Next_Step",
              ),),
              Expanded(flex: 2, child: Container()),
            ]))));
  }

  void pressNextBtn() {
    // ref.watch(signUpInfoProvider.notifier).setSignUpInfo(SignUpParameter(memberId: "",
    //     registerMode: "MAIL", email: emailTextController.text, country: "", phone: "",
    //     password: "",
    //     name: "", inviteCode: "UZAVW2BV46", imageUrl: ""));
    if(!isPasswordWrong && !isPasswordDiff){
      ref.watch(passwordProvider.notifier).setPassword(pwTextController.text);
      Navigator.push(navigatorKey.currentContext!, MaterialPageRoute(builder: (context) => SignUpNickScreen()));
    }

  }
}
