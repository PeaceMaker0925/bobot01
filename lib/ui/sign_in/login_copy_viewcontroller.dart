import 'package:bobot01/repository/api_response.dart';
import 'package:bobot01/ui/sign_in/verify_code_viewcontroller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_fonts/google_fonts.dart';
import '../../constants/SignInWidgets.dart';
import '../../constants/constants.dart';
import '../../constants/theme.dart';
import '../../constants/public_component.dart';
import '../../constants/width_infinty_button.dart';
import 'login_viewmodel.dart';

class LoginCopyScreen extends ConsumerStatefulWidget {
  LoginCopyScreen({Key? key}) : super(key: key);
  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _LoginScreenState();
}

class _LoginScreenState extends ConsumerState<LoginCopyScreen> {
  TextEditingController emailTextController = TextEditingController();
  TextEditingController passwordTextController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  AsyncValue asyncResponse = const AsyncValue.loading();

  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    emailTextController.addListener(() {
      ref.read(accountProvider.notifier).setAccount(emailTextController.text);
    });

    passwordTextController.addListener(() {
      ref.read(passwordProvider.notifier).setPassword(passwordTextController.text);
    });

  }

  @override
  Widget build(BuildContext context) {
    ref.listen(isLoadingProvider,(_,__){
      setState(() {
        isLoading = __;
      });
    });
    return buildFlexLayout(context, Container(
      margin: EdgeInsets.symmetric(horizontal: Constant().getViewWidth(context)/20),
      child: Column(
        children: [
          Expanded(flex: 1, child: Container(alignment: Alignment.centerLeft, child: CircleButton(0, ref),)),
          Expanded(flex: 2, child: Container(color: Colors.transparent, alignment: Alignment.center, child: Column(children: [
            Container(height: Constant().getViewHeight(context)/8, child: Image.asset('assets/icon.png'),),
            Text('Bobot', style: GoogleFonts.balooTamma(
              textStyle: TextStyle(fontSize: 32, color: CustomTheme.fontPurple01Color),
            ),)
          ],),)),
          Expanded(flex: 5, child: Container(

            // alignment: Alignment.centerLeft,
            child: Column(
              children: [
                Form(key: _formKey,
                  autovalidateMode: AutovalidateMode.always,
                  onChanged: (){Form.of(primaryFocus!.context!)!.save();},
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Email or phone number *", style: TextStyle(color: Colors.white,fontSize: 18,fontFamily: 'ubuntu'),),
                      TextFormField(
                          controller: emailTextController,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Please enter some text';
                            }
                            return null;
                          },
                          style: TextStyle(color: Colors.white),
                          decoration: InputDecoration(
                            // suffixIcon: const Icon(Icons.remove_red_eye_rounded,color: CustomTheme.labelWhiteColor,),
                            hintText: 'Please enter your email or phone number',
                            hintStyle: const TextStyle(color: CustomTheme.labelWhiteColor),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide: const BorderSide(
                                  width: 1, color: CustomTheme.labelWhiteColor),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide: const BorderSide(
                                  width: 1, color: CustomTheme.labelWhiteColor),
                            ),
                            focusedErrorBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide: const BorderSide(
                                  width: 1, color: CustomTheme.primaryRedColor),
                            ),
                            errorBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide: const BorderSide(
                                  width: 1, color: CustomTheme.primaryRedColor),
                            ),
                          )
                      ),
                      Text("Password *", style: TextStyle(color: Colors.white,fontSize: 18,fontFamily: 'ubuntu'),),
                      TextFormField(
                          controller: passwordTextController,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Please enter some text';
                            }
                            return null;
                          },
                          style: TextStyle(color: Colors.white),
                          decoration: InputDecoration(
                            suffixIcon: const Icon(Icons.remove_red_eye_rounded,color: CustomTheme.labelWhiteColor,),
                            hintText: 'Please enter your password',
                            hintStyle: const TextStyle(color: CustomTheme.labelWhiteColor),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide: const BorderSide(
                                  width: 1, color: CustomTheme.labelWhiteColor),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide: const BorderSide(
                                  width: 1, color: CustomTheme.labelWhiteColor),
                            ),
                            focusedErrorBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide: const BorderSide(
                                  width: 1, color: CustomTheme.primaryRedColor),
                            ),
                            errorBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide: const BorderSide(
                                  width: 1, color: CustomTheme.primaryRedColor),
                            ),
                          )
                      ),
                    ],
                  ),
                ),
                WidthInfintyButton(
                  onTap: () {
                    if(_formKey.currentState!.validate()){
                      ref.read(isLoadingProvider.notifier).setLoading(true);
                      ref.read(loginVerifyProvider(emailTextController.text).future).then(
                            (event){event.then((value) {
                          print("${(value as ApiResponse).message}");
                          ref.read(isLoadingProvider.notifier).setLoading(false);
                          Navigator.push(context, MaterialPageRoute(builder: (context) => VerifyCodeScreen()));
                          showResponseDialog(context, "${(value as ApiResponse).message}", (){});
                        });
                        },
                      );
                      // ref.read(loginVerifyProvider(emailTextController.text).future).then((event) {
                      //   event.then((value) {
                      //     print("${(value as ApiResponse).message}");
                      //     Navigator.push(context, MaterialPageRoute(builder: (context) => VerifyCodeScreen()));
                      //     showResponseDialog(context, "${(value as ApiResponse).message}", (){});
                      //   });
                      // });

                      // ref.watch(accountProvider.notifier).setAccount(emailTextController.text);
                      // ref.watch(passwordProvider.notifier).setPassword(passwordTextController.text);
                      //
                      // print(ref.watch(accountProvider));
                      // print(ref.watch(passwordProvider));

                    }
                  },
                  fontColor: Colors.white,
                  foregroundColor: CustomTheme.fontPurple01Color,
                  fontSize: 18,
                  text: "Log in",
                ),
                // Text("Sign up", style: TextStyle(color: Colors.yellow, fontSize: 12, fontFamily: 'ubuntu'),),
              ],
            ),
          )),
        ]
    ),), isLoading: isLoading);
  }
}
