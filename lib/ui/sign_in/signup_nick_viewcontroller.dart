import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_fonts/google_fonts.dart';
import '../../constants/SignInWidgets.dart';
import '../../constants/circleStepView.dart';
import '../../constants/constants.dart';
import '../../constants/theme.dart';
import '../../constants/public_component.dart';
import '../../constants/width_infinty_button.dart';
import '../../main.dart';
import '../home/model/SignUpParameter.dart';
import 'login_viewcontroller.dart';
import 'login_viewmodel.dart';

class SignUpNickScreen extends ConsumerStatefulWidget {
  SignUpNickScreen({Key? key}) : super(key: key);
  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _SignUpNickScreenState();
}

class _SignUpNickScreenState extends ConsumerState<SignUpNickScreen> {
  TextEditingController nickTextController = TextEditingController();
  TextEditingController inviteCodeTextController = TextEditingController();
  AsyncValue asyncResponse = const AsyncValue.loading();
  bool isLoading = false;
  bool isNextEnable = false;
  bool isPasswordShow = false;
  FocusNode nickNameFocusNode = FocusNode();
  FocusNode inviteCodeFocusNode = FocusNode();

  @override
  void initState() {

  }

  void unFocusAllNode(){
    nickNameFocusNode.unfocus();
    inviteCodeFocusNode.unfocus();
  }

  @override
  Widget build(BuildContext context) {
    ref.listen(isLoadingProvider,(_,__){
      setState(() {
        isLoading = __;
      });
    });
    return buildFlexLayout(context, InkWell(onTap: () { unFocusAllNode(); }, child: Container(
        margin: EdgeInsets.symmetric(horizontal: Constant().getViewWidth(context)/20),
        child: Column(
            children: [
              Spacer(),
              Expanded(flex: 2, child: Container(alignment: Alignment.centerLeft, child: Row(children: [
                Expanded(flex: 1, child: CircleButton(0, ref)),
                Expanded(flex: 4, child: Container())
              ],))),
              Expanded(flex: 4, child: Container(color: Colors.transparent, alignment: Alignment.center, child: Column(children: [
                Expanded(flex: 2, child: Container(height: Constant().getViewHeight(context)/8, child: Image.asset('assets/icon.png'),)),
                Expanded(flex: 1, child: Text('Bobot', style: GoogleFonts.balooTamma(
                  textStyle: TextStyle(fontSize: 26, color: CustomTheme.fontPurple01Color),
                ),))
              ],),)),
              Expanded(flex: 4, child: CircleStepView(step: 3)),
              Expanded(flex: 2, child: SignInTextField(nickTextController, Container(), nickNameFocusNode, "Please_enter_your_nickname")),
              Expanded(flex: 1, child: Container()),
              Expanded(flex: 2, child: SignInTextField(inviteCodeTextController, Container(), inviteCodeFocusNode, "Please_enter_your_inviteCode")),
              Expanded(flex: 1, child: Container()),
              Expanded(flex: 6, child: Container()),
              Expanded(flex: 2, child: WidthInfintyButton(
                onTap: () { pressNextBtn(); },
                fontColor: Colors.white,
                foregroundColor: CustomTheme.fontPurple01Color,
                fontSize: 18,
                text: "Finish_SignUp",
              ),),
              Expanded(flex: 2, child: Container(child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                Text('若您點擊繼續，即代表您同意', style: TextStyle(
                  color: CustomTheme.fontPurple02Color,)),
                Container(width: Constant().getViewWidth(context)/40,),
                GestureDetector(child:  Text('幣威隱私協議', style: TextStyle(
                    color: CustomTheme.fontPurple01Color,
                    decoration: TextDecoration.underline,
                    fontWeight: FontWeight.w700
                ),), onTap: (){
                  // Navigator.push(context, MaterialPageRoute(builder: (context) => SignUpScreen()));
                },)
              ],),)),
            ]))), isLoading: isLoading);
  }

  void pressNextBtn() {
    if(nickTextController.text.isNotEmpty){
      ref.read(isLoadingProvider.notifier).setLoading(true);
      var params = SignUpParameter(
          memberId: ref.watch(signUpInfoProvider).memberId,
          registerMode: ref.watch(signUpInfoProvider).registerMode,
          email: ref.watch(signUpInfoProvider).email,
          country: ref.watch(signUpInfoProvider).country,
          phone: ref.watch(signUpInfoProvider).phone,
          password: ref.watch(passwordProvider),
          name: nickTextController.text,
          inviteCode: ref.watch(signUpInfoProvider).inviteCode,
          imageUrl: ""
      );
      ref.watch(signUpProvider(params).future).then((event) {event.then((value) {
        ref.read(isLoadingProvider.notifier).setLoading(false);
        if(value.message.compareTo("SUCCESS") == 0) {
          Navigator.pushReplacement(navigatorKey.currentContext!, MaterialPageRoute(builder: (context) => LoginScreen()));
        }
        else {showResponseDialog(context, "SignUp Error\n"+"${(value).url}\n\n${(value).code}\n\n${(value).message}", (){});}
      });});
    }

  }
}
