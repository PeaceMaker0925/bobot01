import 'package:bobot01/ui/bot/bot_model/CreateBobotListParameter.dart';
import 'package:bobot01/ui/bot/bot_model/CreateBotParameter.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import '../../constants/constants.dart';
import '../../constants/theme.dart';
import '../../constants/public_component.dart';
import '../bot/bot_viewcontroller.dart';
import '../bot/bot_viewmodel.dart';
import '../bot/create_bot_viewcontroller.dart';
import '../navigation_viewcontroller.dart';
import 'copy_viewmodel.dart';

class CreateCopyBotScreen extends ConsumerStatefulWidget {
  const CreateCopyBotScreen({super.key});

  @override
  ConsumerState<CreateCopyBotScreen> createState() => _CreateCopyBotScreenState();
}

class _CreateCopyBotScreenState extends ConsumerState<CreateCopyBotScreen> {

  List<dynamic> connectedExList = List.empty(growable: true);
  String marketValue = "";
  String coinValue = "";
  String positionTypeValue = "";
  String rangeString = "";
  String strategyValue = "";
  String tradeTypeValue = "";
  String leverValue = leverList.first;
  bool isExConnected = false;
  TextEditingController positionTextController = TextEditingController();
  FocusNode positionFocusNode = FocusNode();

  @override
  void initState() {

    var info = ref.read(copyBotInfoProvider);
    marketValue = info.market;
    coinValue = info.coin;
    positionTypeValue = info.positionType;
    strategyValue = info.strategy;
    tradeTypeValue = info.tradeType;
    getRange();
    ref.read(connectedExProvider.future).then((event) {
      event.then((value) {
        List<dynamic> exList = (value).data;
        connectedExList.clear();
        exList.map((exInfo) {
          if(exInfo['bind'] == "true") connectedExList.add(exInfo['marketName']);
        }).toList();
        isExConnected = connectedExList.contains(marketValue);
        setState(() { });
        // showResponseDialog(context, "${(value).url}\n\n${(value).code}\n\n${(value).message}\n\n${(value).data}", (){});
      });
    });

  }

  @override
  Widget build(BuildContext context) {
    return buildFlexBodyLayout(context, InkWell(onTap: () { positionFocusNode.unfocus(); }, child: Container(margin: EdgeInsets.symmetric(
        horizontal: Constant().getViewWidth(context) / 20),
        child: Column(
            children: [
              Expanded(flex: 3, child: Container(child: Center(child: Text("CopyBobot", style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 18 , fontWeight: FontWeight.w700), )),)),
              Expanded(flex: 2, child: Row(children: [
                Expanded(flex: 6, child: Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.symmetric(horizontal: 15.0),
                  decoration: BoxDecoration(color: Colors.white, border: Border.all(color: CustomTheme.fontPurple02Color), borderRadius: BorderRadius.circular(35),),
                  child: Container(child: Text(marketValue)),
                )),
                Expanded(flex: 1, child: Container(child: Icon(
                  isExConnected ? Icons.check : Icons.dangerous ,
                  color: isExConnected ? Colors.greenAccent : Colors.redAccent ,
                ),))
              ],),),
              Spacer(),
              Expanded(flex: 2, child: Container(
                alignment: Alignment.center,
                padding: EdgeInsets.symmetric(horizontal: 15.0),
                decoration: BoxDecoration(color: Colors.white, border: Border.all(color: CustomTheme.fontPurple02Color), borderRadius: BorderRadius.circular(35),),
                child: Container(child: Text(coinValue)),
              ),),
              Expanded(flex: 2, child: Container(
                margin:EdgeInsets.only(bottom: Constant().getViewHeight(context)/140),
                alignment: Alignment.bottomLeft,
                child: Text("Strategy", textAlign: TextAlign.left, style: TextStyle(color: CustomTheme.fontPurple02Color),),
              )),
              Expanded(flex: 2, child: Container(alignment: Alignment.center, child: Row(children: [
                Expanded(flex: 8, child: selectStrategyCell("Auto")),
                Spacer(),
                Expanded(flex: 8, child: selectStrategyCell("Bull")),
                Spacer(),
                Expanded(flex: 8, child: selectStrategyCell("Bear")),
              ])

              )),
              Expanded(flex: 2, child: Container(
                margin:EdgeInsets.only(bottom: Constant().getViewHeight(context)/140),
                alignment: Alignment.bottomLeft,
                child: Text("PositionType", textAlign: TextAlign.left, style: TextStyle(color: CustomTheme.fontPurple02Color),),
              )),
              Expanded(flex: 2, child: Container(alignment: Alignment.center, child: Row(children: [
                Expanded(flex: 9, child: positionCell("Spot")),
                Spacer(),
                Expanded(flex: 9, child: positionCell("Contract")),
              ],),)),
              Expanded(flex: 2, child: Container(
                margin:EdgeInsets.only(bottom: Constant().getViewHeight(context)/140),
                alignment: Alignment.bottomLeft,
                child: Text("Range", textAlign: TextAlign.left, style: TextStyle(color: CustomTheme.fontPurple02Color),),
              )),
              Expanded(flex: 2, child: Container(alignment: Alignment.center, child: textFieldCell(rangeString)),),
              Expanded(flex: 2, child: Container(),),
              Expanded(flex: 6, child: CreateBotBtn(
                onTap: () async {
                  print("$marketValue,$coinValue,${positionTextController.text},${kindList.first},$tradeTypeValue,$leverValue");
                  ref.watch(createBotProvider(CreateBotParameter(
                      marketName: marketValue,
                      coinName: coinValue,
                      positionNum: positionTextController.text,
                      strategyType: kindList.first,
                      tradeType: tradeTypeValue,
                      lever: leverValue
                  )).future).then((event) {
                    event.then((value) {
                      if ((value).message == 'SUCCESS') {
                        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => NavigationScreen()));
                      }
                      showResponseDialog(context, "${(value).url}\n\n${(value).code}\n\n${(value).message}\n\n${(value).data}", (){});
                    });
                  });

                },
              ),),
              Expanded(flex: 6, child: Container()),
            ]))));
  }
  Widget selectStrategyCell(String item){
    bool isCurrent = strategyValue == item;
    return Container(
        alignment: Alignment.centerLeft,
        padding: EdgeInsets.symmetric(horizontal: 5.0),
        decoration: BoxDecoration(
            border: Border.all(color: CustomTheme.fontPurple02Color),
            borderRadius: BorderRadius.circular(35),
            color: isCurrent ? CustomTheme.fontPurple02Color : Colors.white
        ),
        child: Container(
          alignment: Alignment.center, child: Text(item , style: TextStyle(color: isCurrent ? Colors.white : CustomTheme.fontPurple02Color, fontSize: 12),),)
    );
  }

  Widget positionCell(String item){
    bool isCurrent = positionTypeValue == item;
    return Container(
        alignment: Alignment.centerLeft,
        padding: EdgeInsets.symmetric(horizontal: 5.0),
        decoration: BoxDecoration(
          border: Border.all(color: CustomTheme.fontPurple02Color),
          borderRadius: BorderRadius.circular(35),
          color: isCurrent ? CustomTheme.fontPurple02Color : Colors.white
        ),
        child: Container(
          alignment: Alignment.center, child: Text(item , style: TextStyle(color: isCurrent ? Colors.white : CustomTheme.fontPurple02Color, fontSize: 12),),)
    );
  }

  Widget textFieldCell(String rangeMin){
    return Container(
        alignment: Alignment.centerLeft,
        padding: EdgeInsets.symmetric(horizontal: 5.0),
        decoration: BoxDecoration(
          border: Border.all(color: CustomTheme.fontPurple02Color),
          borderRadius: BorderRadius.circular(35),
          color: Colors.white
        ),
        child: TextFormField(
          controller: positionTextController,
          textAlign: TextAlign.center,
          focusNode: positionFocusNode,
          textAlignVertical: TextAlignVertical.center,
          // inputFormatters: <TextInputFormatter>[
          //   FilteringTextInputFormatter.allow(RegExp("[0-9.]"))
          // ],
          decoration: InputDecoration(
            isDense: true,
            border: InputBorder.none,
            hintText: rangeMin,
            // suffixIcon: Icon(Icons.percent, size: Constant().getViewWidth(context)/30, color: Colors.white)
          ),
          style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 14),
          // focusNode: focusNode,
          // keyboardType: TextInputType.numberWithOptions(decimal: true)
        )
    );
  }

  void getRange() {
    ref.read(queryRangeProvider(QueryRangeParameter(exName: marketValue, coinName: coinValue, tradeType: tradeTypeValue)).future).then((event) {
      event.then((value) {
        // List<dynamic> list = (value).data;
        // coinList.clear();
        // list.map((exInfo) {
        //   coinList.add(exInfo);
        // }).toList();
        rangeString = (value).data["positionMin"];
        positionTextController.text = rangeString;
        setState(() { });
      });
    });
  }
}
