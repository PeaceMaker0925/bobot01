import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import '../../constants/constants.dart';
import '../../constants/lightCell.dart';
import '../../constants/public_component.dart';
import '../../constants/shared_preference.dart';
import '../../constants/theme.dart';
import '../bot/bot_model.dart';
import '../bot/create_bot_viewcontroller.dart';
import '../home/home_viewcontroller.dart';
import '../sign_in/login_viewmodel.dart';
import 'copy_viewmodel.dart';

class CopyScreen extends ConsumerStatefulWidget {
  const CopyScreen({Key? key}) : super(key: key);
  @override
  ConsumerState<CopyScreen> createState() => _CopyScreenState();
}

class _CopyScreenState extends ConsumerState<CopyScreen> {

  bool isLoading = false;
  List<CopyBotInfo> botList = List.empty(growable: true);
  List<CopyBotInfo> filteredList = List.empty(growable: true);
  List<String> coinList = List.empty(growable: true);
  int currentFilterNum = 0;
  String strategyValue = strategyList.first;
  String positionValue = positionList.first;
  String coinValue = "";
  @override
  void initState() {
    ref.read(copyBotListProvider.future).then((event) {
      event.then((value) {
        List<dynamic> BobotList = value.data;
        BobotList.map((info) {
          botList.add(CopyBotInfo(
              market: info['marketName'],
              coin: info['coin'],
              tradeType: info['tradeType'],
              robotType: info['robotType'],
              positionType: info['positionType'],
              incomeRate: info['incomeRate'].toString(),
              openDays: info['openDays'].toString(),
              followNum: info['followNum'].toString(),
              riskRating: info['riskRating']
          ));
        }).toList();
        filterBobotList();
        setState(() {});
      });
    });
    getCoinList();
  }

  @override
  Widget build(BuildContext context) {

    ref.listen(isLoadingProvider,(_,__){
      setState(() {
        isLoading = __;
      });
    });
    return buildFlexBodyLayout(context, Container(margin: EdgeInsets.symmetric(
        horizontal: Constant().getViewWidth(context) / 20),
        child: Stack(children: [
        Container(margin: EdgeInsets.only(top: Constant().getViewHeight(context) * 72/100), alignment: Alignment.bottomLeft, height: Constant().getViewHeight(context)/8, child: Image.asset('assets/img/img_bg_bobot_01.png'),),
        Column(
            children: [
              Expanded(flex: 3, child: Container(child: buildAppBar(title: "Marketplace", ref: ref)),),
              Expanded(flex: 2, child: Container(alignment: Alignment.center, child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                Expanded(flex: 1, child: Text("Strategy Type", style: CustomTheme.textPrimaryPurple02,)),
                Expanded(flex: 3, child: buildStrategyRow())
              ],)),),
              Spacer(),
              Expanded(flex: 2, child: Container(alignment: Alignment.center, child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                Expanded(flex: 1, child: Text("Position Type", style: CustomTheme.textPrimaryPurple02)),
                Expanded(flex: 3, child: buildPositionRow())
              ],)),),
              Spacer(),
              Expanded(flex: 2, child:
                Container(alignment: Alignment.center, child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                Expanded(flex: 1, child: Text("Coin Type", style: CustomTheme.textPrimaryPurple02)),
                Expanded(flex: 3, child: Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.symmetric(horizontal: 15.0),
                  decoration: BoxDecoration(color: Colors.white, border: Border.all(color: CustomTheme.fontPurple02Color), borderRadius: BorderRadius.circular(35),),
                  child: coinList.length == 0 ? Container() : DropdownButtonHideUnderline(child: DropdownButton2<String>(
                    isExpanded: true,
                    value: coinValue,
                    icon: const Icon(Icons.keyboard_arrow_down, color: CustomTheme.fontPurple02Color),
                    onChanged: (String? value) {
                      // This is called when the user selects an item.
                      setState(() {
                        coinValue = value!;
                        filterBobotList();
                      });
                    },
                    items: coinList.map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value, style: TextStyle(color: CustomTheme.fontPurple02Color),),
                      );
                    }).toList(),
                  )),
                ))
              ],)),),
              Expanded(flex: 1, child: Container()),
              Expanded(flex: 24, child: Container(child:
                SingleChildScrollView(child: Column(children: filteredList.map((botInfo) => buildBotListColumn(botInfo)).toList(),)),)),
              Expanded(flex: 2, child: Container()),
            ])])));
  }


  Widget buildBotListColumn(CopyBotInfo info) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: Constant().getViewHeight(context)/140),
      child: lightCell(Container(padding: EdgeInsets.symmetric(vertical: Constant().getViewHeight(context)/140),child: Row(children: [
        Expanded(flex: 3, child: Column(children: [
          riskBobotImg(info.riskRating),
          Container(
            alignment: Alignment.center,
            margin: EdgeInsets.all(2),
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(width: 1, color: CustomTheme.fontPurple01Color),
              borderRadius: BorderRadius.all(Radius.circular(35)),
            ),
            child: Text(info.positionType, textAlign: TextAlign.left, style: TextStyle(fontSize: 12, color: CustomTheme.fontPurple01Color), maxLines: 1),
          )
        ],),),
        Expanded(flex: 5, child: Column(children: [
          Container(alignment: Alignment.topLeft, child: Text(info.robotType, textAlign: TextAlign.left, style: TextStyle(fontSize: 16, color: CustomTheme.fontPurple02Color), maxLines: 1, overflow: TextOverflow.ellipsis,),),
          Container(alignment: Alignment.topLeft, child: Text(info.coin, style: TextStyle(fontSize: 12, color: CustomTheme.fontPurple01Color), maxLines: 1, overflow: TextOverflow.ellipsis,)),
          Container(alignment: Alignment.topLeft, child: Text("RiskRating "+info.riskRating, style: TextStyle(fontSize: 12, color: CustomTheme.fontPurple01Color), maxLines: 1, overflow: TextOverflow.ellipsis,)),
        ],)),

        Expanded(flex: 3, child: Column(children: [
          Container(alignment: Alignment.topLeft, child: Text("Day "+info.openDays, textAlign: TextAlign.left, style: TextStyle(fontSize: 16, color: CustomTheme.fontPurple02Color), maxLines: 1, overflow: TextOverflow.ellipsis,),),
          Container(alignment: Alignment.topLeft, child: Text("Follows "+info.followNum, style: TextStyle(fontSize: 12, color: CustomTheme.fontPurple01Color), maxLines: 1, overflow: TextOverflow.ellipsis,)),
        ],)),

        Expanded(flex: 6, child: Column(children: [
          RedGreenCell(info.incomeRate)
        ],)),
        Expanded(flex: 3, child: GestureDetector(onTap: (){
          ///need login
          BobotSharedPreferences().getUserToken().then((value) {
            setState(() {
              value != "userToken"
                  ? toFollow(info)
                  : ref.read(showToLogInProvider.notifier).setShowToLogIn(!ref.watch(showToLogInProvider));
            });
          });
        }, child: Container(
          padding: EdgeInsets.all(4),
          alignment: Alignment.center,
          decoration: BoxDecoration(
            color: CustomTheme.fontPurple01Color,
            borderRadius: BorderRadius.all(Radius.circular(15)),
          ),
          child: Text("Follow", style: CustomTheme.textSmallPrimary,),
        ))),
      ],),)),);
  }

  Widget riskBobotImg(String riskRating) {
    String imgPath = "";
    switch(int.parse(riskRating)){
      case 1:
      case 2:
      case 3:
        imgPath = 'assets/img/img_bobot_green_01.png';
        break;
      case 4:
      case 5:
      case 6:
      case 7:
        imgPath = 'assets/img/img_bobot_nor_01.png';
        break;
      case 8:
      case 9:
      case 10:
        imgPath = 'assets/img/img_bobot_red_01.png';
        break;
    }
    return Container(child: Image.asset(imgPath, height: Constant().getViewHeight(context)/30,),);
  }

  Widget buildStrategyRow() {
    return Container(alignment: Alignment.center, child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: strategyList.map((item) {
      bool isCurrent = strategyValue == item;
      return Expanded(child: GestureDetector(onTap: (){
        strategyValue = item;
        filterBobotList();
        setState(() {});
      }, child: Container(
        alignment: Alignment.centerLeft,
        padding: EdgeInsets.symmetric(horizontal: 5.0),
        margin: EdgeInsets.symmetric(horizontal: 5.0),
        decoration: BoxDecoration(
            border: Border.all(color: CustomTheme.fontPurple02Color),
            borderRadius: BorderRadius.circular(35),
            color: isCurrent ? CustomTheme.fontPurple02Color : Colors.white
        ),
        child: Container(
          alignment: Alignment.center,
          child: Text(item , style: TextStyle(
              color: isCurrent ? Colors.white : CustomTheme.fontPurple02Color,
              fontSize: 12),
          ),),
      ),));
    }
     ).toList(),),);
  }
  Widget buildPositionRow() {
    return Container(alignment: Alignment.center, child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: positionList.map((item) {
      bool isCurrent = positionValue == item;
      return Expanded(child: GestureDetector(onTap: (){
        positionValue = item;
        filterBobotList();
        setState(() {});
      }, child: Container(
        alignment: Alignment.centerLeft,
        padding: EdgeInsets.symmetric(horizontal: 5.0),
        margin: EdgeInsets.symmetric(horizontal: 5.0),
        decoration: BoxDecoration(
            border: Border.all(color: CustomTheme.fontPurple02Color),
            borderRadius: BorderRadius.circular(35),
            color: isCurrent ? CustomTheme.fontPurple02Color : Colors.white
        ),
        child: Container(
          alignment: Alignment.center,
          child: Text(item , style: TextStyle(
              color: isCurrent ? Colors.white : CustomTheme.fontPurple02Color,
              fontSize: 12),
          ),),
      ),));
    }
     ).toList(),),);
  }

  void getCoinList() {
    ref.read(copyBotCoinListProvider.future).then((event) {
      event.then((value) {
        List<dynamic> list = (value).data;
        coinList.clear();
        list.map((exInfo) {
          coinList.add(exInfo);
        }).toList();
        coinValue = coinList.first;
        filterBobotList();

        setState(() { });
      });
    });
  }

  void filterBobotList() {
    filteredList.clear();
    filteredList.addAll(botList);
    filteredList.retainWhere((info){
      return (info.positionType).contains(positionValue) && (info.robotType).contains(strategyValue) && (info.coin).contains(coinValue);
    });
  }

  void toFollow(CopyBotInfo info) {
    ref.read(copyBotInfoProvider.notifier).setCopyBotInfo(CopyBotInfoParameter(
        market: info.market, coin: info.coin, positionType: info.positionType, strategy: info.robotType, tradeType: info.tradeType));
    ref.read(bodyIndexProvider.notifier).setBodyIndex(7);
  }

}
