


import 'package:equatable/equatable.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../repository/api_response.dart';
import '../../repository/copy_api_repository.dart';

final copyBotListProvider = FutureProvider.autoDispose<Future<ApiResponse>>((ref) async {
  return CopyApiRepositoryImpl().queryCopyBot();
});

final copyBotCoinListProvider = FutureProvider.autoDispose<Future<ApiResponse>>((ref) async {
  return CopyApiRepositoryImpl().queryCopyBotCoin();
});


/// Holding some variables in Cache
final copyBotInfoProvider = StateNotifierProvider<CopyBotInfoNotifier, CopyBotInfoParameter>((ref) {
  return CopyBotInfoNotifier(ref);
});
class CopyBotInfoNotifier extends StateNotifier<CopyBotInfoParameter> {
  CopyBotInfoNotifier(this.ref): super(const CopyBotInfoParameter(market: '', coin: '', positionType: '', strategy: '', tradeType: ''));
  final Ref ref;
  Future<void> setCopyBotInfo(CopyBotInfoParameter parameter) async {
    state = parameter;
  }
}

class CopyBotInfoParameter extends Equatable {
  final String market;
  final String coin;
  final String positionType;
  final String strategy;
  final String tradeType;

  const CopyBotInfoParameter({
    required this.market,
    required this.coin,
    required this.positionType,
    required this.strategy,
    required this.tradeType,
  });

  @override
  List<Object?> get props => [market, coin, positionType, strategy, tradeType];
}