import 'dart:async';

import 'package:bobot01/constants/constants.dart';
import 'package:bobot01/ui/sign_in/login_viewcontroller.dart';
import 'package:bobot01/ui/sign_in/login_viewmodel.dart';
import 'package:bobot01/websocket/websocket_manager.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:lottie/lottie.dart';
import 'package:uuid/uuid.dart';
import '../constants/lightCell.dart';
import '../constants/shared_preference.dart';
import '../constants/theme.dart';
import '../datetime/date_manager.dart';
import '../main.dart';
import 'floating_bar.dart';
import 'game/game_model/game_parameter.dart';
import 'game/game_viewmodel.dart';
import 'menu/menu_viewcontroller.dart';
import 'dart:convert';

class NavigationScreen extends ConsumerStatefulWidget {
  @override
  ConsumerState<NavigationScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends ConsumerState<NavigationScreen> with SingleTickerProviderStateMixin, WidgetsBindingObserver{

  late final _tabController = TabController(length: routeList.length, vsync: this);
  bool isLogin = false;
  late Timer _timer;

  /// WebSocket連線
  initWebSocket() {
    WebSocketManager().initWebSocket(
      onOpen: () {
        // 心跳機制
        debugPrint('MainScreen主頁 onOpen');
        WebSocketManager().initHeartBeat();
      },
      onMessage: (data) {
        print('wc::${data}');
        if(data.toString().contains("hb")){
          print("sever send hb for check");
        }
        else{
          final wcResponse = json.decode(data.toString());
          final topic = wcResponse['topic'];
          switch (topic) {
            case 'bobot3d_lottery_time':
              final msg = WcLotteryTimeInfo.decode(data);
              ref.read(WcLotteryTimeProvider.notifier).state = msg;
              ref.read(fomo3DGolbalTimerProvider.notifier).updateTime();
              // final time = DateTime.fromMillisecondsSinceEpoch(msg.lotteryTime);
              // final endTime = DateManager().getDateWith12HourFormat2(time);
              // print('endTime:${endTime}');
              break;
            case 'bobot3d_end_response':
              final msg = WcEndResponseInfo.decode(data);
              ref.read(WcEndResponseProvider.notifier).state = msg;
              // 刷新 history
              _getHistory();
              break;
            case 'logout':
              ///another user login
              ref.read(logOutProvider.future).then((event) => { event.then((value) => {
                if ((value).message ==  'SUCCESS') {
                  CleanData(),
                  Navigator.pushReplacement(navigatorKey.currentContext!, MaterialPageRoute(builder: (context) => LoginScreen()),)
                }
              })});
              break;
            // case 'hb':
            //   print("ba boom!!");
            //   break;
            default :
              print("unkown topic: "+data);
              break;
          }
        }
      },
      onError: (error) {
        debugPrint(error.toString());
      },
    );
  }

  void _getHistory() {
    final time = DateManager().getDateWith12HourFormat(DateTime.now());
    ref.read(getHistoryLotteryProvider(GetHistoryParameter(date: time)));
  }

  @override
  void initState() {
    _timer = Timer.periodic(Duration(seconds: 1), (timer) async {
      ref.read(fomo3DGolbalTimerProvider.notifier).reduceTime();
    });

    initWebSocket();

    generateDeviceInfo().then((deviceInfo) {
      print("deviceInfo : $deviceInfo");
      ref.watch(deviceInfoProvider.notifier).setDeviceInfo(deviceInfo!).then((value) => {
        if(isLogin){
          ref.watch(saveDeviceInfoProvider(ref.watch(deviceInfoProvider)).future).then((event) {event.then((value) {
              debugPrint("${(value).url}\n\n${(value).code}\n\n${(value).message}\n\n${(value).data}");
          });})
        }
      });
    });

    /// Lifecycle
    WidgetsBinding.instance.addObserver(this);

    BobotSharedPreferences().getUserToken().then((value) {
      setState(() {
        isLogin = value != "userToken";
      });
    });

    super.initState();
  }

  @override
  void dispose() {
    WebSocketManager().closeSocket();
    /// Lifecycle
    WidgetsBinding.instance.removeObserver(this);
    _timer.cancel();
    _tabController.dispose();
    super.dispose();
  }

  /// Lifecycle
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    switch(state){
      case AppLifecycleState.resumed:
        initWebSocket();
        break;
      case AppLifecycleState.paused:
        WebSocketManager().closeSocket();
        break;
      case AppLifecycleState.inactive:
      case AppLifecycleState.detached:
        break;
    }
  }

  @override
  Widget build(BuildContext context) {

    ref.listen(bodyIndexProvider, (previous, next) {
      setState(() {
        // _selectedIndex = next;
        Future.delayed(Duration.zero,(){
          _tabController.animateTo(next, duration: Duration(milliseconds: 5000), curve: Curves.bounceInOut);
        });
      });
    });

    ref.listen(showToLogInProvider, (previous, next) {
      // if(!ref.watch(showToLogInProvider))return;
      setState(() {
        showDialog(context: context, builder: (_) =>  AlertDialog(
          backgroundColor: Colors.transparent,
          content: Container(
            alignment: Alignment.center,
            height: Constant().getViewHeight(context)/3,
            // width: Constant().getViewWidth(context) * 0.8,
            child: toLogInDialog(),),
        ));
      });
    });

    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: PreferredSize(preferredSize: Size.fromHeight(0), child: AppBar(elevation: 0,backgroundColor: Colors.transparent)),
        key: scaffoldStateKey,
        body: Stack(children: [
          Container(alignment: Alignment.topLeft, color: Colors.white, child: Stack(children: [
              Container(height: Constant().getViewHeight(context),width: Constant().getViewWidth(context), child: Lottie.asset(
                  'assets/bobot_bg_01.json',
                  width: Constant().getViewWidth(context),
                  height: Constant().getViewHeight(context),
                  alignment: Alignment.center,
                  animate: true,
                  onLoaded: (composition) {
                  },
                  fit: BoxFit.cover
                ),
              ),
              Container(margin: EdgeInsets.only(top: Constant().getViewHeight(context)/20), height: Constant().getViewHeight(context)/2, child: Image.asset("assets/bobot_bg_title.png"),),
          ],)),
          TabBarView(
              physics: NeverScrollableScrollPhysics(),
              controller: _tabController,
              children: routeList),
          Positioned(bottom: Constant().getViewHeight(context)/40, child: Container(
            width: Constant().getViewWidth(context) - Constant().getViewHeight(context)/40,
            margin: EdgeInsets.symmetric(horizontal: Constant().getViewHeight(context)/80),
            height: 86,
            child: FloatingBar(),
          ))

        ],),
        endDrawer: Drawer(child: MenuScreen(), backgroundColor: Colors.transparent ,)
    );
  }
  Widget toLogInDialog(){
    return lightCell(isBar: true, Container(
      margin: EdgeInsets.symmetric(horizontal: Constant().getViewWidth(context)/40),
      child: Column(
          children: [
            Spacer(),
            Expanded(flex: 3, child: Container(height: Constant().getViewHeight(context)/8, child: Image.asset('assets/icon.png'),)),
            Spacer(),
            Expanded(flex: 1, child: Row(children: [
              Spacer(),
              Expanded(flex: 8, child: GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                  Navigator.pushReplacement(navigatorKey.currentContext!, MaterialPageRoute(builder: (context) => LoginScreen()));
                },
                child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: CustomTheme.fontPurple01Color,
                    borderRadius: BorderRadius.all(Radius.circular(35)),
                  ),
                  child: Text("Log in Account", style: CustomTheme.textPrimary,),
                ),),),
              Spacer()
            ],)),
            Spacer()
          ]
      ),));
  }


}

void CleanData(){
  BobotSharedPreferences().saveUserToken("userToken");
  BobotSharedPreferences().saveMemberId(Uuid().v1());
  BobotSharedPreferences().saveDeviceToken('deviceToken');
}

int mapIndex(int selectedIndex) {
  switch (selectedIndex) {
    case 1:
      return 1;
    case 2:
      return 2;
    case 3:
      return 3;
    case 4:
      return 4;
    case 5:
      return 1;
    default:
      return 0;
  }
}

String getIndexName(int tab) {
  switch (tab) {
    case 1:
      return 'Bot';
    case 2:
      return 'Game';
    case 3:
      return 'Order';
    case 4:
      return 'Profile';
    default:
      return 'Home';
  }
}