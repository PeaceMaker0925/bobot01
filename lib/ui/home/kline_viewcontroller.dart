import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:lottie/lottie.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

import '../../constants/constants.dart';
import '../../constants/public_component.dart';
import '../../constants/theme.dart';
import '../../repository/home_api_repository.dart';
import 'home_viewmodel.dart';
import 'kline_model/bollinger_bands_indicator.dart';
import 'kline_model/candle.dart';
import 'kline_model/candle_ticker_model.dart';
import 'kline_model/candlesticks.dart';
import 'kline_model/indicator.dart';
import 'kline_model/weighted_moving_average.dart';
import 'kline_widget/toolbar_action.dart';

class KlineScreen extends ConsumerStatefulWidget {

  @override
  ConsumerState<KlineScreen> createState() => _KlineScreenState();
}

class _KlineScreenState extends ConsumerState<KlineScreen> {
  BinanceRepository repository = BinanceRepository();
  List<Candle> candles = [];
  WebSocketChannel? _channel;
  String currentInterval = "1m";

  String currentSymbol = "";
  List<Indicator> indicators = [
    BollingerBandsIndicator(
      length: 20,
      stdDev: 2,
      upperColor: const Color(0xFF2962FF),
      basisColor: const Color(0xFFFF6D00),
      lowerColor: const Color(0xFF2962FF),
    ),
    WeightedMovingAverageIndicator(
      length: 100,
      color: Colors.green.shade600,
    ),
  ];


  @override
  void initState() {
    currentSymbol = ref.read(symbolProvider.notifier).state+"USDT";
    fetchCandles(currentSymbol, currentInterval);
    super.initState();
  }


  @override
  void dispose() {
    if (_channel != null) _channel!.sink.close();
    super.dispose();
  }

  Future<void> fetchCandles(String symbol, String interval) async {
    print(symbol);
    // close current channel if exists
    if (_channel != null) {
      _channel!.sink.close();
      _channel = null;
    }
    // clear last candle list
    setState(() {
      candles = [];
      currentInterval = interval;
    });

    try {
      // load candles info
      final data =
        await repository.fetchCandles(symbol: symbol, interval: interval);
      // connect to binance stream
      _channel =
          repository.establishConnection(symbol.toLowerCase(), currentInterval);
      // update candles
      setState(() {
        candles = data;
        currentInterval = interval;
        currentSymbol = symbol;
      });
    } catch (e) {
      // handle error
      print(e.toString());
      return;
    }
  }

  void updateCandlesFromSnapshot(AsyncSnapshot<Object?> snapshot) {
    if (candles.isEmpty) return;
    if (snapshot.data != null) {
      final map = jsonDecode(snapshot.data as String) as Map<String, dynamic>;
      if (map.containsKey("k") == true) {
        final candleTicker = CandleTickerModel.fromJson(map);

        // cehck if incoming candle is an update on current last candle, or a new one
        if (candles[0].date == candleTicker.candle.date &&
            candles[0].open == candleTicker.candle.open) {
          // update last candle
          candles[0] = candleTicker.candle;
        }
        // check if incoming new candle is next candle so the difrence
        // between times must be the same as last existing 2 candles
        else if (candleTicker.candle.date.difference(candles[0].date) ==
            candles[0].date.difference(candles[1].date)) {
          // add new candle to list
          candles.insert(0, candleTicker.candle);
        }
      }
    }
  }

  Future<void> loadMoreCandles() async {
    try {
      // load candles info
      final data = await repository.fetchCandles(
          symbol: currentSymbol,
          interval: currentInterval,
          endTime: candles.last.date.millisecondsSinceEpoch);
      candles.removeLast();
      setState(() {
        candles.addAll(data);
      });
    } catch (e) {
      // handle error
      return;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(preferredSize: Size.fromHeight(0), child: AppBar(elevation: 0,backgroundColor: Colors.transparent)),
      body: Builder(
    builder: (context) => Stack(children: [
        Container(alignment: Alignment.topLeft, color: Colors.white, child: Stack(children: [
          Container(height: Constant().getViewHeight(context),width: Constant().getViewWidth(context), child: Lottie.asset(
              'assets/bobot_bg_01.json',
              width: Constant().getViewWidth(context),
              height: Constant().getViewHeight(context),
              alignment: Alignment.center,
              animate: true,
              onLoaded: (composition) {
              },
              fit: BoxFit.cover
          ),
          ),
          Container(margin: EdgeInsets.only(top: Constant().getViewHeight(context)/20), height: Constant().getViewHeight(context)/2, child: Image.asset("assets/bobot_bg_title.png"),),
        ],)),
          Container(
          color: Colors.transparent,
          margin: EdgeInsets.symmetric(horizontal: Constant().getViewWidth(context) / 20,),
          width: Constant().getViewWidth(context),
          height: Constant().getViewHeight(context) - Scaffold.of(context).appBarMaxHeight!.toInt(),
          child: Column(children: [
            buildAppBar(title: currentSymbol, ref: ref, backPage: -1),
            Expanded(flex: 6, child: Center(
              child: StreamBuilder(
                stream: _channel == null ? null : _channel!.stream,
                builder: (context, snapshot) {
                  updateCandlesFromSnapshot(snapshot);
                  return Candlesticks(
                    key: Key(currentSymbol + currentInterval),
                    indicators: indicators,
                    candles: candles,
                    onLoadMoreCandles: loadMoreCandles,
                    onRemoveIndicator: (String indicator) {
                      setState(() {
                        indicators = [...indicators];
                        indicators
                            .removeWhere((element) => element.name == indicator);
                      });
                    },
                    actions: [
                      // ToolBarAction(
                      //   onPressed: () {
                      //     showDialog(
                      //       context: context,
                      //       builder: (context) {
                      //         return Center(
                      //           child: Container(
                      //             width: 200,
                      //             color: Theme.of(context).backgroundColor,
                      //             child: Wrap(
                      //               children: intervals
                      //                   .map((e) => Padding(
                      //                 padding: const EdgeInsets.all(8.0),
                      //                 child: SizedBox(
                      //                   width: 50,
                      //                   height: 30,
                      //                   child: RawMaterialButton(
                      //                     elevation: 0,
                      //                     fillColor:
                      //                     const Color(0xFF494537),
                      //                     onPressed: () {
                      //                       fetchCandles(currentSymbol, e);
                      //                       Navigator.of(context).pop();
                      //                     },
                      //                     child: Text(
                      //                       e,
                      //                       style: const TextStyle(
                      //                         color: Color(0xFFF0B90A),
                      //                       ),
                      //                     ),
                      //                   ),
                      //                 ),
                      //               ))
                      //                   .toList(),
                      //             ),
                      //           ),
                      //         );
                      //       },
                      //     );
                      //   },
                      //   child: Text(
                      //     currentInterval,
                      //   ),
                      // ),
                      // ToolBarAction(
                      //   width: 100,
                      //   onPressed: () {
                      //     showDialog(
                      //       context: context,
                      //       builder: (context) {
                      //         return SymbolsSearchModal(
                      //           symbols: symbols,
                      //           onSelect: (value) {
                      //             fetchCandles(value, currentInterval);
                      //           },
                      //         );
                      //       },
                      //     );
                      //   },
                      //   child: Text(
                      //     currentSymbol,
                      //   ),
                      // )
                    ],
                  );
                },
              ),
            )),
            Expanded(flex: 2, child: Column(
              children: [
                Expanded(flex: 8, child: GestureDetector(child: Container(
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        border: Border.all(color: CustomTheme.fontPurple01Color),
                        borderRadius: BorderRadius.circular(35),
                        color: Colors.white
                    ),
                    child: Text("CALL", style: CustomTheme.textPrimaryPurple01,)),
                  onTap: (){

                  },
                )),
                Spacer(),
                Expanded(flex: 8, child: GestureDetector(child: Container(
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(35),
                        color: CustomTheme.fontPurple01Color
                    ),
                    child: Text("PUT", style: CustomTheme.textPrimary,)),
                  onTap: (){

                  },
                )),
              ],
            )),
            Spacer(),
          ])
    )]),));
  }

}