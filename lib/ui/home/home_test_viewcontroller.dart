import 'dart:convert';

import 'package:bobot01/constants/theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../constants/constants.dart';
import '../../constants/lightCell.dart';
import '../../constants/shared_preference.dart';
import '../../main.dart';
import '../../repository/api_response.dart';
import '../../constants/public_component.dart';
import '../sign_in/forget_password_viewcontroller.dart';
import '../sign_in/login_viewmodel.dart';
import '../sign_in/signup_viewcontroller.dart';
import 'home_viewmodel.dart';
import '../sign_in/login_viewcontroller.dart';
import 'model/CheckSignupParameter.dart';

class HomeTestScreen extends ConsumerStatefulWidget {
  const HomeTestScreen({super.key});

  @override
  ConsumerState<HomeTestScreen> createState() => _HomeTestScreenState();
}

class _HomeTestScreenState extends ConsumerState<HomeTestScreen> {
  String currentAccount = ' - Log out - ';
  bool isLoading = false;

  @override
  void initState() {
    BobotSharedPreferences().getCurrentAccount().then((value) {
      setState(() {
        currentAccount = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    ref.listen(isLoadingProvider,(_,__){
      setState(() {
        isLoading = __;
      });
    });
    return buildFlexBodyLayout(context, Container(
        color: Colors.transparent,
        margin: EdgeInsets.symmetric(
        horizontal: Constant().getViewWidth(context) / 20),
        child: Column(
            children: [
              Expanded(flex: 1, child: Container(),),
              Expanded(flex: 3, child: GestureDetector(
                onTap: (){
                  Navigator.push(navigatorKey.currentContext!, MaterialPageRoute(builder: (context) => LoginScreen()));
                },
                child: Container(
                  // height: 60,
                  alignment: Alignment.center,
                  // decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.blueAccent),
                  child: lightCell(Text('Login / get verify code', style: CustomTheme.textSmallSecond,)),
                ),
              ),),
              Expanded(flex: 1, child: Text('Current Account: $currentAccount', style: TextStyle(color: Colors.white),)),
              Expanded(flex: 2, child: GestureDetector(
                onTap: (){
                  ref.watch(logOutProvider.future).then((event) {
                    event.then((value) {
                      if ((value).message == 'SUCCESS') {
                        BobotSharedPreferences().saveCurrentAccount(' - Log out - ');
                      }
                      showResponseDialog(context, (value).message, (){});
                    });
                  });
                },
                child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.blueAccent),
                  child: Text('Log Out'),
                ),
              ),),
              Expanded(flex: 1, child: Container(),),
              Expanded(flex: 2, child: GestureDetector(
                onTap: (){
                  ref.watch(saveDeviceInfoProvider(ref.watch(deviceInfoProvider)).future).then((event) {
                    event.then((value) {
                      showResponseDialog(context, "${(value as ApiResponse).message}", (){});
                    });
                  });
                },
                child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.blueAccent),
                  child: Text('save deviceInfo'),
                ),
              ),),
              Expanded(flex: 1, child: Container(),),
              Expanded(flex: 2, child: GestureDetector(
                onTap: (){
                  ref.watch(checkSignUpProvider(const CheckSignUpParameter(
                      name: "email", param: "ericshih0925@gmail.com"
                  )).future).then((event) {
                    event.then((value) {
                      showResponseDialog(context, "${(value).url}\n\n${(value).code}\n\n${(value).message}", (){});
                    });
                  });
                },
                child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.blueAccent),
                  child: Text('Check Sign Up : Email'),
                ),
              ),),
              Expanded(flex: 1, child: Container(),),
              Expanded(flex: 2, child: GestureDetector(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) => ForgetPasswordScreen()));
                },
                child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.blueAccent),
                  child: Text('Forget Password / get verify code'),
                ),
              ),),
              Expanded(flex: 1, child: Container(),),
              Expanded(flex: 2, child: GestureDetector(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) => SignUpScreen()));
                },
                child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.blueAccent),
                  child: Text('Sign Up'),
                ),
              ),),
              Expanded(flex: 1, child: Container(),),
              Expanded(flex: 2, child: GestureDetector(
                onTap: (){
                  //editMemberInfoProvider
                  ref.watch(editMemberInfoProvider.future).then((event) {
                    event.then((value) {
                      showResponseDialog(context, "${(value).url}\n\n${(value).code}\n\n${(value).message}", (){});
                    });
                  });
                },
                child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.greenAccent),
                  child: Text('Edit Name 123'),
                ),
              ),),
              Spacer(),
              Expanded(flex: 2, child: GestureDetector(
                onTap: (){
                  ref.watch(queryAssetDepositProvider.future).then((event) {
                    event.then((value) {
                      JsonEncoder encoder = JsonEncoder.withIndent('  ');
                      String prettyprint = encoder.convert((value).data);
                      showResponseDialog(context, "${(value).url}\n\n${(value).code}\n\n${(value).message}\n\n$prettyprint", (){});
                    });
                  });
                },
                child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.orangeAccent),
                  child: Text('Query Deposit'),
                ),
              ),),
              Spacer(),
              Expanded(flex: 2, child: GestureDetector(
                onTap: (){
                  ref.watch(queryAssetWithdrawProvider.future).then((event) {
                    event.then((value) {
                      JsonEncoder encoder = JsonEncoder.withIndent('  ');
                      String prettyprint = encoder.convert((value).data);
                      showResponseDialog(context, "${(value).url}\n\n${(value).code}\n\n${(value).message}\n\n$prettyprint", (){});
                    });
                  });
                },
                child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.orangeAccent),
                  child: Text('Query Withdraw'),
                ),
              ),),
              Spacer(),
              Expanded(flex: 2, child: GestureDetector(
                onTap: (){
                  ref.watch(queryIndexPriceProvider.future).then((event) {
                    event.then((value) {
                      JsonEncoder encoder = JsonEncoder.withIndent('  ');
                      String prettyprint = encoder.convert((value).data);
                      showResponseDialog(context, "${(value).url}\n\n${(value).code}\n\n${(value).message}\n\n$prettyprint", (){});
                    });
                  });
                },
                child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.orangeAccent),
                  child: Text('Index Price'),
                ),
              ),),
              Spacer(),
              Expanded(flex: 2, child: GestureDetector(
                onTap: (){
                  ref.watch(queryIndexPopProvider.future).then((event) {
                    event.then((value) {
                      JsonEncoder encoder = JsonEncoder.withIndent('  ');
                      String prettyprint = encoder.convert((value).data);
                      showResponseDialog(context, "${(value).url}\n\n${(value).code}\n\n${(value).message}\n\n$prettyprint", (){});
                    });
                  });
                },
                child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.orangeAccent),
                  child: Text('Index POP'),
                ),
              ),),
              Spacer(),
              Expanded(flex: 2, child: GestureDetector(
                onTap: (){
                  ref.watch(queryIncomeProvider.future).then((event) {
                    event.then((value) {
                      JsonEncoder encoder = JsonEncoder.withIndent('  ');
                      String prettyprint = encoder.convert((value).data);
                      showResponseDialog(context, "${(value).url}\n\n${(value).code}\n\n${(value).message}\n\n$prettyprint", (){});
                    });
                  });
                },
                child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.orangeAccent),
                  child: Text('Query Income'),
                ),
              ),),
              Spacer(),
              Expanded(flex: 2, child: GestureDetector(
                onTap: (){
                  ref.read(isLoadingProvider.notifier).setLoading(true);
                  ref.watch(queryAllBotInfoProvider.future).then((event) {
                    event.then((value) {
                      ref.read(isLoadingProvider.notifier).setLoading(false);
                      JsonEncoder encoder = JsonEncoder.withIndent('  ');
                      String prettyprint = encoder.convert((value).data);
                      showResponseDialog(context, "${(value).url}\n\n${(value).code}\n\n${(value).message}\n\n$prettyprint", (){});
                    });
                  });
                },
                child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.orangeAccent),
                  child: Text('Query All Robot'),
                ),
              ),),
              Spacer()
            ])), isLoading: isLoading);
  }
}
