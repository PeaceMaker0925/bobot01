import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:stomp_dart_client/stomp.dart';
import 'package:stomp_dart_client/stomp_config.dart';

import '../../repository/api_response.dart';
import '../../repository/home_api_repository.dart';

final queryAssetDepositProvider = FutureProvider.autoDispose<Future<ApiResponse>>((ref) async {
  return HomeApiRepositoryImpl().queryAssetDeposit();
});

final queryAssetWithdrawProvider = FutureProvider.autoDispose<Future<ApiResponse>>((ref) async {
  return HomeApiRepositoryImpl().queryAssetWithdraw();
});

final queryIndexPriceProvider = FutureProvider.autoDispose<Future<ApiResponse>>((ref) async {
  return HomeApiRepositoryImpl().queryIndexPrice();
});

final queryEnergyBalanceProvider = FutureProvider.autoDispose<Future<ApiResponse>>((ref) async {
  return HomeApiRepositoryImpl().queryEnergyBalance();
});

final queryIndexPopProvider = FutureProvider.autoDispose<Future<ApiResponse>>((ref) async {
  return HomeApiRepositoryImpl().queryIndexPop();
});

final queryIncomeProvider = FutureProvider.autoDispose<Future<ApiResponse>>((ref) async {
  return HomeApiRepositoryImpl().queryIncome();
});

final queryIncomeHomeProvider = FutureProvider.autoDispose<Future<ApiResponse>>((ref) async {
  return HomeApiRepositoryImpl().queryIncomeHome();
});

final queryAllBotInfoProvider = FutureProvider.autoDispose<Future<ApiResponse>>((ref) async {
  return HomeApiRepositoryImpl().queryAllBotInfo();
});

/// Holding some variables in Cache
final showMenuProvider = StateNotifierProvider<showMenuNotifier, bool>((ref) {
  return showMenuNotifier(ref);
});
class showMenuNotifier extends StateNotifier<bool> {
  showMenuNotifier(this.ref): super(false);
  final Ref ref;
  Future<void> setShowMenu(bool parameter) async {
    state = parameter;
  }
}

final symbolProvider = StateNotifierProvider<SymbolNotifier, String>((ref) {
  return SymbolNotifier(ref);
});
class SymbolNotifier extends StateNotifier<String> {
  SymbolNotifier(this.ref): super('');
  final Ref ref;
  Future<void> setSymbol(String parameter) async {
    state = parameter;
  }
}


final messageProvider = StreamProvider.autoDispose<String>((ref) async* {

  // connect("ws://autotrader-dev.dbithk.com/gateway/app/websocket",
  //     onConnect: (_,__){ print("onConnect");},
  //     port: 8080,
  //     onDisconnect: (_){print("onDisconnect");}
  // ).then((stompClient) {
  //   stompClient.subscribeString("123", "/topic/one", (Map<String, String> headers, String message) {
  //     print("receive message: " + message);
  //   });
  //   // stompClient.sendString("/app/test", "hello world");
  // }, onError: (error) {
  //   print("connect failed");
  // });


});
