import 'package:bobot01/constants/theme.dart';
import 'package:bobot01/websocket/websocket_manager.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:stomp_dart_client/stomp.dart';
import '../../constants/constants.dart';
import '../../constants/lightCell.dart';
import '../../constants/shared_preference.dart';
import '../../main.dart';
import '../../constants/public_component.dart';
import '../sign_in/login_viewmodel.dart';
import 'home_model.dart';
import 'home_viewmodel.dart';
import 'kline_viewcontroller.dart';

class HomeScreen extends ConsumerStatefulWidget {
  const HomeScreen({super.key});

  @override
  ConsumerState<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends ConsumerState<HomeScreen> {
  bool isLogin = false;
  bool isLoading = false;
  bool isExpand = false;
  List<PriceInfo> priceList = List.empty(growable: true);
  String total = "0.0";
  String energy = "";
  String withholding = "";
  String totalProfit = "";
  String todayProfit = "";
  String returnValue = "0";
  late StompClient stompClient;

  @override
  void initState() {
    BobotSharedPreferences().getUserToken().then((value) {
      setState(() {
        isLogin = value != "userToken";
      });
      if(isLogin){
        loadindData();
      }
    });
    _getPirceList();
  }

  void _getPirceList() {
    ref.read(queryIndexPriceProvider.future).then((event) {
      event.then((value) {
        List<dynamic> pageList = value.data['pageList'];
        pageList.map((info) {
          priceList.add(PriceInfo(
              logoUrl: info['logoUrl'],
              symbolName: info['symbolName'],
              symbol: info['symbol'],
              price: info['price'],
              priceChangePercent: info['priceChangePercent']
          ));
        }).toList();
        // print("priceList = $priceList");
        setState(() {});
        // JsonEncoder encoder = JsonEncoder.withIndent('  ');
        // String prettyprint = encoder.convert((value).data);
        // showResponseDialog(context, "${(value).url}\n\n${(value).code}\n\n${(value).message}\n\n$prettyprint", (){});
      });
    });
  }
  void loadindData() {

    ref.read(queryEnergyBalanceProvider.future).then((event) {
      event.then((value) {
        total = value.data['total'];
        energy = value.data['energyAvailable'];
        withholding = value.data['withholding'];
        setState(() {});
      });
    });

    ref.read(queryIncomeProvider.future).then((event) {
      event.then((value) {
        totalProfit = value.data['incomeTotal'];
        todayProfit = value.data['todayIncome'];
        setState(() {});
      });
    });

    ///1201
    ref.read(queryIncomeHomeProvider.future).then((event) {
      event.then((value) {
        // totalProfit = value.data['incomeTotal'];
        // todayProfit = value.data['todayIncome'];
        setState(() {});
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    ref.listen(isLoadingProvider,(_,__){
      setState(() {
        isLoading = __;
      });
    });

    return buildFlexBodyLayout(context, Container(
        color: Colors.transparent,
        margin: EdgeInsets.symmetric(
          horizontal: Constant().getViewWidth(context) / 20,
        ),
        child: Column(children: [
          buildAppBar(title: "", ref: ref),
          Expanded(flex: isExpand ? 7 : 5, child: Container(alignment: Alignment.center, color: Colors.transparent, child: Column(children: [
            Container(height: Constant().getViewHeight(context)/40, child: CircleAvatar(
              backgroundColor: CustomTheme.fontPurple01Color,
              radius: 40,
            )),
            Container(width: 10, height: 10, color: CustomTheme.fontPurple01Color,),
            Expanded(flex: 8, child: Container(
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: CustomTheme.fontPurple01Color,
                borderRadius: BorderRadius.all(Radius.circular(35)),
              ),
              child: HomeBotContent(total, energy, withholding, totalProfit, todayProfit, returnValue),
            ))
          ],),)),
          Expanded(flex: 1, child: Container(color: Colors.transparent, child: Row(children: [
            Spacer(),
            Expanded(flex: 3, child: Container(alignment: Alignment.centerLeft, child: Text("Token", style: TextStyle(fontSize: 16, color: CustomTheme.fontPurple02Color)))),
            Expanded(flex: 3, child: Container(alignment: Alignment.center, child: Text("Price", style: TextStyle(fontSize: 16, color: CustomTheme.fontPurple02Color)),)),
            Expanded(flex: 3, child: Container(alignment: Alignment.centerRight, child: Text("24%", style: TextStyle(fontSize: 16, color: CustomTheme.fontPurple02Color)))),
            Spacer()
          ],),)),
          Expanded(flex: 12, child: Container(child: SingleChildScrollView(child: Column(children: [
            Column(
              children: priceList.map((info) => buildPriceListColumn(info)).toList(),
            ),
            Container(height: Constant().getViewHeight(context)/8,)
          ],),),)),
        ])), isLoading: isLoading);
  }

  Widget buildPriceListColumn(PriceInfo info) {
    return GestureDetector(child: Container(
      margin: EdgeInsets.symmetric(vertical: Constant().getViewHeight(context)/140),
      child: lightCell(Container(padding: EdgeInsets.symmetric(vertical: Constant().getViewHeight(context)/140),child: Row(children: [
        Expanded(flex: 3, child: CachedNetworkImage(
          imageUrl: info.logoUrl,
          errorWidget: (context, url, error) => Image.asset('assets/error_url.png'),
          height: Constant().getViewHeight(context)/30,
        )),
        Expanded(flex: 6, child: Column(children: [
          Container(alignment: Alignment.topLeft, child: Text(info.symbolName, textAlign: TextAlign.left, style: TextStyle(fontSize: 16, color: CustomTheme.fontPurple02Color), maxLines: 1, overflow: TextOverflow.ellipsis,),),
          Container(alignment: Alignment.topLeft, child: Text(info.symbol, style: TextStyle(fontSize: 12, color: CustomTheme.fontPurple01Color), maxLines: 1, overflow: TextOverflow.ellipsis,)),
        ],)),
        Spacer(),
        Expanded(flex: 6, child: Container(alignment: Alignment.centerLeft, child: Text(info.price, textAlign: TextAlign.center, style: TextStyle(fontSize: 16, color: CustomTheme.fontPurple02Color),)),),
        Expanded(flex: 7 , child: RedGreenCell(info.priceChangePercent)),
      ],),)),),onTap: (){
      ref.read(symbolProvider.notifier).setSymbol(info.symbol);
      Navigator.push(navigatorKey.currentContext!, MaterialPageRoute(builder: (context) => KlineScreen()),);
    },);
  }

  Widget HomeBotContent(String total, String energyAvailable, String withholding, String totalProfit, String todayProfit, String returnValue,) {
    return Column(children: [
      Expanded(flex: 3, child: Container(margin: EdgeInsets.all(10), alignment: Alignment.center,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(topLeft: Radius.circular(25),topRight: Radius.circular(25),bottomLeft: Radius.circular(10),bottomRight: Radius.circular(10),),),
            child: Column(children: [
              SizedBox(height: Constant().getViewHeight(context)/80,),
              Container(height: Constant().getViewHeight(context)/60, child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                Expanded(flex: 4, child: Container()),
                Expanded(flex: 1, child: CircleAvatar(backgroundColor: CustomTheme.fontPurple01Color, radius: 40,)),
                Expanded(flex: 1, child: CircleAvatar(backgroundColor: CustomTheme.fontPurple01Color, radius: 40,)),
                Expanded(flex: 4, child: Container())
              ],)),
              SizedBox(height: Constant().getViewHeight(context)/80,),
              Container(height: Constant().getViewHeight(context)/50, child: Text("Energy account total (USDT)",style: TextStyle(fontSize: 14, color: CustomTheme.fontPurple01Color))),
              SizedBox(height: Constant().getViewHeight(context)/80,),
              Expanded(flex: 3, child: Column(children: [
                Container(
                  padding: EdgeInsets.only(left: Constant().getViewWidth(context)/10),
                  child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                  Container(alignment: Alignment.center, child: Text(_buildTotalNum(double.parse(total)), style: TextStyle(fontSize: 24, color: CustomTheme.fontPurple02Color, fontWeight: FontWeight.w700)),),
                  GestureDetector(child: Container(
                    width: Constant().getViewWidth(context)/10,
                    color: Colors.transparent,
                    alignment: Alignment.centerRight,
                    child: Icon(isExpand ? Icons.arrow_drop_up : Icons.arrow_drop_down),), onTap: (){
                    isExpand = !isExpand;
                    setState(() {});
                  },)
                ],),),
                if(isExpand)Container(
                  alignment: Alignment.center,
                  margin: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                  child: Divider(height: 1, color:CustomTheme.fontPurple01Color,),
                ),
                if(isExpand)Expanded(child: Container(
                alignment: Alignment.center,
                child: Row(children: [
                  Spacer(),
                  Expanded(flex: 6, child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
                    Row(children: [
                      Container(margin: EdgeInsets.only(right: 2), height: 15, width: 3, color: CustomTheme.fontPurple01Color,),
                      Expanded(child: Text("Energy account balance", style: TextStyle(fontSize: 12, color: CustomTheme.fontPurple02Color, overflow: TextOverflow.ellipsis))),
                    ],),
                    Container(margin: EdgeInsets.only(left: 4), alignment:Alignment.centerLeft, child: Text(energy, style: TextStyle(fontSize: 14, color: CustomTheme.fontPurple01Color),),)
                  ],)),
                  Spacer(),
                  Expanded(flex: 6, child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
                    Row(children: [
                      Container(margin: EdgeInsets.only(right: 2), height: 15, width: 3, color: CustomTheme.fontPurple01Color,),
                      Expanded(child: Text("Retained amount", style: TextStyle(fontSize: 12, color: CustomTheme.fontPurple02Color))),
                    ],),
                    Container(margin: EdgeInsets.only(left: 4), alignment:Alignment.centerLeft, child: Text(withholding, style: TextStyle(fontSize: 14, color: CustomTheme.fontPurple01Color)),)
                  ],)),
                  Spacer(),
                ],)))
                ],)),]))),
      Container(height: Constant().getViewHeight(context)/20, child: Row(children: [
        Spacer(),
        Expanded(flex: 6, child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          Row(children: [
            Container(margin: EdgeInsets.only(right: 5), height: 15, width: 3, color: CustomTheme.cellYellowColor,),
            Text("Total profit",style: TextStyle(fontSize: 12, color: Colors.white)),
          ],),
          Container(margin: EdgeInsets.only(left: 8), alignment:Alignment.centerLeft, child: Text(totalProfit, style: TextStyle(fontSize: 14, color: Colors.white),),)
        ],)),
        Spacer(),
        Expanded(flex: 6, child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          Row(children: [
            Container(margin: EdgeInsets.only(right: 5), height: 15, width: 3, color: CustomTheme.cellYellowColor,),
            Text("Today profit",style: TextStyle(fontSize: 12, color: Colors.white)),
          ],),
          Container(margin: EdgeInsets.only(left: 8), alignment:Alignment.centerLeft, child: Text(todayProfit, style: TextStyle(fontSize: 14, color: Colors.white)),)
        ],)),
        Spacer(),
                // Expanded(flex: 4, child: GestureDetector(child: Stack(children: [
                //   Container(alignment:Alignment.center, child: Image.asset('assets/btn_deposit.png')),
                //   Container(alignment:Alignment.center, child: Text("Deposit", style: TextStyle(color: CustomTheme.fontPurple02Color),),)
                // ],), onTap: (){},)),
        Expanded(flex: 6, child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          Row(children: [
            Container(margin: EdgeInsets.only(right: 5), height: 15, width: 3, color: CustomTheme.fontGreen01Color,),
            Text("Return",style: TextStyle(fontSize: 12, color: Colors.white)),
          ],),
          Container(margin: EdgeInsets.only(left: 8), alignment:Alignment.centerLeft, child: Text(todayProfit, style: TextStyle(fontSize: 14, color: Colors.white)),)
        ],)),
        Spacer()
      ],)),
      Container(height: Constant().getViewHeight(context)/80,)
    ]);
  }
}


Widget RedGreenCell(String percent) {
  double number = double.parse(percent);
  return Container(
    padding: EdgeInsets.all(Constant().getViewHeight(navigatorKey.currentContext!)/100),
    margin: EdgeInsets.symmetric(horizontal: Constant().getViewHeight(navigatorKey.currentContext!)/200),
    decoration: BoxDecoration(
    color: number > 0 ? CustomTheme.fontGreen01Color : CustomTheme.fontRed01Color,
    borderRadius: BorderRadius.all(Radius.circular(16)),
  ),child: Text(textAlign: TextAlign.center,"${_buildOperationAssert(number)}${_buildDayChangeNum(number)}%", style: CustomTheme.textPrimary,),);
}
String _buildOperationAssert(num tokenPercent) {
  if (tokenPercent.isNegative) {
    return '';
  } else {
    return '+';
  }
}

String _buildDayChangeNum(num tokenPercent) {
  int decimalNum = 0;
  if(tokenPercent/10 > 1) decimalNum ++;
  if(tokenPercent/100 > 1) decimalNum ++;
  if(tokenPercent/1000 > 1) decimalNum ++;
  if(tokenPercent/10000 > 1) decimalNum ++;
  String value = tokenPercent.toStringAsFixed(2-decimalNum);
  return value;
}

String _buildTotalNum(num total) {
  int floatNum = 0;
  if((total*10)%10 > 0) floatNum ++;
  if((total*100)%10 > 0) floatNum ++;
  if((total*1000)%10 > 0) floatNum ++;
  if((total*10000)%10 > 0) floatNum ++;
  if((total*100000)%10 > 0) floatNum ++;
  String value = total.toStringAsFixed(floatNum);
  return value;
}