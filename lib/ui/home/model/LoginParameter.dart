import 'package:equatable/equatable.dart';



class LoginParameter extends Equatable {
  final String account;
  final String password;
  final String verifyCode;

  const LoginParameter({
    required this.account,
    required this.password,
    required this.verifyCode,
  });

  @override
  List<Object?> get props => [account, password, verifyCode];
}


class TestParameter extends Equatable {
  final String ts;

  const TestParameter({
    required this.ts,
  });

  @override
  List<Object?> get props => [ts];
}