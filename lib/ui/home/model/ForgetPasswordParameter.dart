import 'package:equatable/equatable.dart';

class ForgetPasswordParameter extends Equatable {
  final String registerMode;
  final String email;
  final String country;
  final String phone;
  final String password;

  const ForgetPasswordParameter({
    required this.registerMode,
    required this.email,
    required this.country,
    required this.phone,
    required this.password,
  });

  @override
  List<Object?> get props => [registerMode, email, country, phone, password];
}