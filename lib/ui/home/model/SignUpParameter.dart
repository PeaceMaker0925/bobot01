import 'package:equatable/equatable.dart';

class SignUpParameter extends Equatable {
  final String memberId;
  final String registerMode;
  final String email;
  final String country;
  final String phone;
  final String password;
  final String name;
  final String inviteCode;
  final String imageUrl;

  const SignUpParameter({
    required this.memberId,
    required this.registerMode,
    required this.email,
    required this.country,
    required this.phone,
    required this.password,
    required this.name,
    required this.inviteCode,
    required this.imageUrl,
  });

  @override
  List<Object?> get props => [memberId, registerMode, email, country, phone, password, name, inviteCode, imageUrl];
}