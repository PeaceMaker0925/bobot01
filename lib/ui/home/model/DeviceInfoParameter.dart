import 'package:equatable/equatable.dart';

class DeviceInfoParameter extends Equatable {
  final String uuid;
  final String language;
  final String device;
  final String deviceInfo;
  final String deviceToken;

  const DeviceInfoParameter({
    required this.uuid,
    required this.language,
    required this.device,
    required this.deviceInfo,
    required this.deviceToken,
  });

  @override
  List<Object?> get props => [uuid, language, device, deviceInfo, deviceToken];
}