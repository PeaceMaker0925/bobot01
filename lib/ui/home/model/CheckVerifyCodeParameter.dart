import 'package:equatable/equatable.dart';

class CheckVerifyCodeParameter extends Equatable {
  final String action;
  final String registerMode;
  final String email;
  final String country;
  final String phone;
  final String verifyCode;

  const CheckVerifyCodeParameter({
    required this.action,
    required this.registerMode,
    required this.email,
    required this.country,
    required this.phone,
    required this.verifyCode,
  });

  @override
  List<Object?> get props => [action, registerMode, email, country, phone, verifyCode];
}