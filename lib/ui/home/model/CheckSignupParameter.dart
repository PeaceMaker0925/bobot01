import 'package:equatable/equatable.dart';



class CheckSignUpParameter extends Equatable {
  final String name;
  final String param;

  const CheckSignUpParameter({
    required this.name,
    required this.param,
  });

  @override
  List<Object?> get props => [name, param];
}