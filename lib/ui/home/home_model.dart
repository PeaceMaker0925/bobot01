class PriceInfo {
  String logoUrl;
  String symbolName;
  String symbol;
  String price;
  String priceChangePercent;

  PriceInfo({
    required this.logoUrl,
    required this.symbolName,
    required this.symbol,
    required this.price,
    required this.priceChangePercent,
  });
}