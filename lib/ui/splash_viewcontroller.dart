import 'dart:async';

import 'package:bobot01/constants/theme.dart';
import 'package:bobot01/ui/sign_in/login_viewcontroller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

import '../constants/constants.dart';
import '../constants/shared_preference.dart';
import 'navigation_viewcontroller.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> with TickerProviderStateMixin{
  late AnimationController _controller;
  Future<String> haveLogin = BobotSharedPreferences().getUserToken().then((value) => value);

  @override
  void initState() {
    super.initState();
    // Future.delayed(const Duration(seconds: 2), () {
    //   Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => NavigationScreen()),);
    // });

    _controller = AnimationController(
      vsync: this,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(color: CustomTheme.fontPurple01Color, child: Lottie.asset(
        'assets/bobot_loading_02.json',
        width: Constant().getViewWidth(context),
        height: Constant().getViewHeight(context),
        alignment: Alignment.center,
        controller: _controller,
        animate: true,
        onLoaded: (composition) {
          _controller
            ..duration = composition.duration
            ..forward().whenComplete(() => //{}
            Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => NavigationScreen()))
            );
        },
        fit: BoxFit.contain
    ));
  }

Widget widgetHaveLogIn(Widget yesWidget, Widget noWidget){
  return FutureBuilder<dynamic>(
      future: haveLogin,
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        if(snapshot.hasData) {
          return snapshot.data != "userToken" ? yesWidget : noWidget;
        } else {
          return Container();
        }
      });
}
}