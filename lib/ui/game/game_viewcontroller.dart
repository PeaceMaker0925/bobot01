import 'package:bobot01/constants/shared_preference.dart';
import 'package:bobot01/constants/theme.dart';
import 'package:bobot01/ui/game/game_model/game_parameter.dart';
import 'package:bobot01/websocket/websocket_manager.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:lottie/lottie.dart';

import '../../constants/constants.dart';
import '../../constants/public_component.dart';
import '../../datetime/date_manager.dart';
import '../profile/profile_viewcontroller.dart';
import '../sign_in/login_viewmodel.dart';
import 'game_viewmodel.dart';

class GameScreen extends ConsumerStatefulWidget {
  const GameScreen({Key? key}) : super(key: key);
  @override
  ConsumerState<GameScreen> createState() => _GameScreenState();
}

class _GameScreenState extends ConsumerState<GameScreen> with TickerProviderStateMixin{
  List<Tab> _gameTabs = [Tab(text: 'Battery Data'), Tab(text: 'Personal Data')];
  late TabController _gameTabController = TabController(length: _gameTabs.length, vsync: this);
  String accountID = '';
  bool isLogin = false;
  int buyNum = 0;
  double buyNumPrice = 0;

  @override
  void initState() {
    _getUserToken();
    _getAnimateTime();
    _getAccountID();
    _getUserBalance();
    _getHistory();
    super.initState();
  }

  void _getUserToken() async {
    ///need login
    BobotSharedPreferences().getUserToken().then((value) {
      setState(() {
        isLogin = value != "userToken";
      });
    });
  }

  void _getAnimateTime() async {
    final time = DateManager().getDateWith12HourFormat(DateTime.now());
    ref.read(getGameTimeProvider(GetGameTimeParameter(date: time)));
  }

  void _getAccountID() async {
    accountID = await BobotSharedPreferences().getMemberId();
  }

  void _getUserBalance() async {
    final time = DateManager().getDateWith12HourFormat(DateTime.now());
    ref.read(getBalanceProvider(GetBalanceParameter(date: time, accountID: await BobotSharedPreferences().getMemberId())));
  }

  void _getHistory() {
    final time = DateManager().getDateWith12HourFormat(DateTime.now());
    ref.read(getHistoryLotteryProvider(GetHistoryParameter(date: time)));
  }

  @override
  Widget build(BuildContext context) {
    return buildFlexBodyLayout(context, Container(
      margin: EdgeInsets.symmetric(horizontal: Constant().getViewWidth(context) / 20),
      child: Column(children: [
          Expanded(flex: 2, child: Stack(children: [
          GestureDetector( onTap: () {
              showGameHistoryDialog(context, ref);
            },
            child: SizedBox(width: Constant().getViewWidth(context)/5, height: Constant().getViewWidth(context)/5,
                child: buildAccountCellBtn(title: 'History', imageString: 'assets/img/img_bobot_history_01.png', onTap: (){}
              ))
          ),
          Container(height: Constant().getViewWidth(context)/5,
            child: Column(children: [
              buildAppBar(title: "Game", ref: ref),
              Expanded(child: Align(alignment: Alignment.bottomCenter, child: Text('Contract will drain in', style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 16, fontWeight: FontWeight.w800))))
            ]))
        ])),
        SizedBox(height: Constant().getViewHeight(context)/120),
      Expanded(flex: 1, child: Consumer(builder: (context, ref, _){
          int timestamp = ref.watch(fomo3DGolbalTimerProvider);
          final differentTime = DateManager().differentWithNow(timestamp);
          print(differentTime.inSeconds);
          final countDownTime = DateManager().getDateWith12HourFormat3(differentTime);
          return Text('${countDownTime}', style: TextStyle(color: CustomTheme.fontPurple01Color, fontSize: 28, fontWeight: FontWeight.w600));
        })),
        // SizedBox(height: 10),
        _buildBobotAnimate(),
        // SizedBox(height: 10),
        Text('10,000,00 KM', style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 28, fontWeight: FontWeight.w600)),
        SizedBox(height: Constant().getViewHeight(context)/120),
        GestureDetector( onTap: () async {
          if(!isLogin){
            ref.read(showToLogInProvider.notifier).setShowToLogIn(!ref.watch(showToLogInProvider));
            return;
          }
          final time = DateManager().getDateWith12HourFormat(DateTime.now());
          ref.read(createOrderProvider(CreateOrderParameter(date: time, accountID: accountID)));
        },
          child: Image.asset('assets/img/img_battery_02.png', height: Constant().getViewWidth(context)/6),
        ),
        SizedBox(height: Constant().getViewHeight(context)/120),
        Expanded(flex: 1, child: buildTabBar(context)),
        Divider(height: 0, thickness: 1, color: CustomTheme.labelGrayColor, indent: 15, endIndent: 15),
        SizedBox(height: Constant().getViewHeight(context)/120),
        Expanded(flex: 8, child: TabBarView(controller: _gameTabController, children: [
          Padding(padding: EdgeInsets.symmetric(horizontal: 20), child: _buildLeftTab()),
          Padding(padding: EdgeInsets.symmetric(horizontal: 20), child: _buildRightTab()),
        ]))
      ]),
    ));
  }

  Widget _buildBobotAnimate() {
    final animateTime = ref.watch(aninateTimeProvider);
    int timestamp = ref.watch(fomo3DGolbalTimerProvider);
    final differentTime = DateManager().differentWithNow(timestamp);
    final second = differentTime.inSeconds;
    if (second >= animateTime!.animateMedium) {
      return lottieAnimate('assets/bobot_running_1x_01.json');
    } else if (second >= animateTime.animateFast && second < animateTime.animateMedium) {
      return lottieAnimate('assets/bobot_running_1.5x_01.json');
    } else if (second < animateTime.animateFast && second > 0) {
      return lottieAnimate('assets/bobot_running_2x_01.json');
    } else  {
      return lottieAnimate('assets/bobot_winning_02.json');
    }
  }

  Widget lottieAnimate(String animatePath) {
    return Lottie.asset(
        animatePath,
        width: double.maxFinite,
        height: 150,
        alignment: Alignment.center,
        repeat: true,
        animate: true,
        fit: BoxFit.contain
    );
  }

  Widget _buildLeftTab() {
    final price = ref.watch(WcLotteryTimeProvider);
    return Column(children: [
      Spacer(),
      Row(children: [
        Expanded(child: Text('Battery Price', style: TextStyle(color: CustomTheme.secondBlueColor, fontSize: 14, fontWeight: FontWeight.w600), textAlign: TextAlign.start)),
        Expanded(child: Text('${price!.amountPrice}', style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 14, fontWeight: FontWeight.w600), textAlign: TextAlign.end)),
      ]),
      Spacer(),
      Row(children: [
        Expanded(child: Text('Buy (grain)', style: TextStyle(color: CustomTheme.secondBlueColor, fontSize: 14, fontWeight: FontWeight.w600), textAlign: TextAlign.start)),
        Expanded(child: Row(children: [
          Expanded(child: GestureDetector( onTap: () {(buyNum == 0) ? buyNum = 0 : buyNum--; setState((){}); },
            child: Image.asset('assets/btn/button_reduce_01.png'),
          )),
          Expanded(child: Text('${buyNum}', style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 30, fontWeight: FontWeight.w600), textAlign: TextAlign.center)),
          Expanded(child: GestureDetector( onTap: () { buyNum++; setState((){}); },
            child: Image.asset('assets/btn/button_add_01.png'),
          ))
        ])),
      ]),
      Spacer(),
      Consumer(builder: (context, ref, _){
        buyNumPrice = price.amountPrice.toDouble() * buyNum;
        return Row(children: [
          Expanded(child: Text('Purchase Price', style: TextStyle(color: CustomTheme.secondBlueColor, fontSize: 14, fontWeight: FontWeight.w600), textAlign: TextAlign.start)),
          Expanded(child: Text('${buyNumPrice}', style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 14, fontWeight: FontWeight.w600), textAlign: TextAlign.end, maxLines: 1, overflow: TextOverflow.ellipsis)),
        ]);
      }),
      Spacer(),
      buildConfirmBtn(context, 'Buy', () async {
        // showGameBuyDialog(context, '1', '1000', '1000', () => Navigator.pop(context), () {});
        WebSocketManager().reconnect();
        // WebSocketManager().closeSocket();
      })
    ]);
  }

  Widget _buildRightTab() {
    return Consumer(builder: (context, ref, _){
      final personalData = ref.watch(personalDataProvider);
      return Column(children: [
        Spacer(),
        Row(children: [
          Expanded(child: Text('Battery Balance', style: TextStyle(color: CustomTheme.secondBlueColor, fontSize: 14, fontWeight: FontWeight.w600), textAlign: TextAlign.start)),
          Expanded(child: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
            Text('${personalData!.batteryBalance}', style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 20, fontWeight: FontWeight.w600), textAlign: TextAlign.center),
            SizedBox(width: 10),
            Text('grain', style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 14, fontWeight: FontWeight.w600), textAlign: TextAlign.center)
          ])),
        ]),
        Spacer(),
        Row(children: [
          Expanded(child: Text('Cost Battery', style: TextStyle(color: CustomTheme.secondBlueColor, fontSize: 14, fontWeight: FontWeight.w600), textAlign: TextAlign.start)),
          Expanded(child: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
            Text('${personalData.costBattery}', style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 20, fontWeight: FontWeight.w600), textAlign: TextAlign.center),
            SizedBox(width: 10),
            Text('grain', style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 14, fontWeight: FontWeight.w600), textAlign: TextAlign.center)
          ])),      ]),
        Spacer(),
        Row(children: [
          Expanded(child: Text('Interest Income', style: TextStyle(color: CustomTheme.secondBlueColor, fontSize: 14, fontWeight: FontWeight.w600), textAlign: TextAlign.start)),
          Expanded(child: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
            Text('${personalData.interestIncome}', style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 20, fontWeight: FontWeight.w600), textAlign: TextAlign.center),
            SizedBox(width: 10),
            Text('USDT', style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 14, fontWeight: FontWeight.w600), textAlign: TextAlign.center)
          ])),      ]),
        Spacer(),
        Row(children: [
          Expanded(child: Text('Commission Income', style: TextStyle(color: CustomTheme.secondBlueColor, fontSize: 14, fontWeight: FontWeight.w600), textAlign: TextAlign.start)),
          Expanded(child: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
            Text('${personalData.commissionIncome}', style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 20, fontWeight: FontWeight.w600), textAlign: TextAlign.center),
            SizedBox(width: 10),
            Text('USDT', style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 14, fontWeight: FontWeight.w600), textAlign: TextAlign.center)
          ])),      ]),
        SizedBox(height: 150)
      ]);
    });
  }

  Widget buildTabBar(BuildContext context) {
    return Consumer(builder: (context, ref, _) {
      return Container(width: Constant().getViewWidth(context), child: TabBar(
        indicatorColor: CustomTheme.fontPurple01Color,
        indicatorSize: TabBarIndicatorSize.tab,
        indicatorWeight: 4,
        labelColor: CustomTheme.fontPurple02Color,
        labelStyle: CustomTheme.textPrimary,
        labelPadding: EdgeInsets.zero,
        unselectedLabelColor: CustomTheme.labelGrayColor,
        padding: EdgeInsets.symmetric(horizontal: Constant().getViewWidth(context)/30),
        controller: _gameTabController,
        tabs: _gameTabs,
        onTap: (index) { _gameTabController.animateTo(index);},
      ));
    });
  }
}