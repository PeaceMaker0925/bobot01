import 'dart:async';

import 'package:bobot01/repository/activation_api_repository.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import '../../repository/api_response.dart';
import '../../repository/game_api_repository.dart';
import 'game_model/game_parameter.dart';

final createOrderProvider = FutureProvider.family.autoDispose<Future<ApiResponse>, CreateOrderParameter>((ref, param) async {
  return GameApiRepositoryImpl().createOrder(param);
});

final buyBatteryProvider = FutureProvider.family.autoDispose<Future<ApiResponse>, BuyBatteryParameter>((ref, param) async {
  return GameApiRepositoryImpl().buyBattery(param);
});

final getBalanceProvider = FutureProvider.family.autoDispose<Future<ApiResponse>, GetBalanceParameter>((ref, param) async {
  print("getBalanceProvider$param");
  final response = await GameApiRepositoryImpl().getUserBalance(param);
  ref.read(personalDataProvider.notifier).state = GamePersonalData(
      batteryBalance: response.data['balance'],
      costBattery: response.data['amount'],
      interestIncome: response.data['dividends_sum'],
      commissionIncome: response.data['up_line_dividends_sum']
  );
  return GameApiRepositoryImpl().getUserBalance(param);
});

final getHistoryLotteryProvider = FutureProvider.family.autoDispose<Future<ApiResponse>, GetHistoryParameter>((ref, param) async {
  AsyncValue.loading();
  final response = await GameApiRepositoryImpl().getHistoryLottery(param);
  final content = response.data['content'][1];
  ref.read(historyDataProvider.notifier).state = GameHistoryInfo(
      sumKm: content['lotteryBonusPool'],
      sumBattery: content['lotteryBonusPool'],
      costPrice: content['bonusPool_Winner'],
      sumParticipate: content['numPeople'],
      winner: content['last_player'],
  );
  return GameApiRepositoryImpl().getHistoryLottery(param);
});

final getGameTimeProvider = FutureProvider.family.autoDispose<Future<ApiResponse>, GetGameTimeParameter>((ref, param) async {
  final response = await GameApiRepositoryImpl().getGameTimeConfig(param);
  ref.read(aninateTimeProvider.notifier).state = AnimateTimeInfo(
      animateFast: response.data['runner_switch_speed_1'],
      animateMedium: response.data['runner_switch_speed_2'],
      animateSlow: response.data['runner_switch_speed_3']
  );
  return GameApiRepositoryImpl().getGameTimeConfig(param);
});

/// 目前查看的Wc EndResponse
final WcEndResponseProvider = StateProvider<WcEndResponseInfo?>((ref) {
  return null;
});

/// 目前查看的Wc End time&price
final WcLotteryTimeProvider = StateProvider<WcLotteryTimeInfo?>((ref) {
  return null;
});

/// 目前查看的Personal Data
final personalDataProvider = StateProvider<GamePersonalData?>((ref) {
  return null;
});

/// 目前查看的History Data
final historyDataProvider = StateProvider<GameHistoryInfo?>((ref) {
  return null;
});

/// 目前的Animatte Time
final aninateTimeProvider = StateProvider<AnimateTimeInfo?>((ref) {
  return AnimateTimeInfo(animateSlow: 0, animateMedium: 0, animateFast: 0);
});

/// Fomo3D count down timer
final fomo3DGolbalTimerProvider = StateNotifierProvider<fomo3DGolbalTimerNotifier, int>((ref) {
    int countDownTime = ref.watch(WcLotteryTimeProvider)!.lotteryTime;
    return fomo3DGolbalTimerNotifier(
        countDownTime: countDownTime
    );
  },
);
class fomo3DGolbalTimerNotifier extends StateNotifier<int> {
  final int countDownTime;
  fomo3DGolbalTimerNotifier({required this.countDownTime}) : super(0);

  Future<void> updateTime() async {
    state = countDownTime;
  }

  Future<void> reduceTime() async {
    state--;
  }
}