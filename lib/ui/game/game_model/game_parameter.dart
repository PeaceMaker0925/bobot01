import 'dart:convert';

import 'package:equatable/equatable.dart';

class CreateOrderParameter extends Equatable {
  final String date;
  final String accountID;

  const CreateOrderParameter({
    required this.date,
    required this.accountID,
  });

  @override
  List<Object?> get props => [date, accountID];
}

class BuyBatteryParameter extends Equatable {
  final String accountID;
  final int amount;
  final int balance;

  const BuyBatteryParameter({
    required this.accountID,
    required this.amount,
    required this.balance
  });

  @override
  List<Object?> get props => [accountID, amount, balance];
}

class GetBalanceParameter extends Equatable {
  final String date;
  final String accountID;

  const GetBalanceParameter({
    required this.date,
    required this.accountID,
  });

  @override
  List<Object?> get props => [date, accountID];
}

class GetHistoryParameter extends Equatable {
  final String date;

  const GetHistoryParameter({
    required this.date,
  });

  @override
  List<Object?> get props => [date];
}

class GetGameTimeParameter extends Equatable {
  final String date;

  const GetGameTimeParameter({
    required this.date,
  });

  @override
  List<Object?> get props => [date];
}

// enum WcMode {
//   bobot3d_end_response,
//   bobot3d_lottery_time
// }

/// topic: WcMode
class WcLotteryTimeInfo {
  final String topic;
  final num amountPrice;
  final int lotteryTime;

  WcLotteryTimeInfo({
    required this.topic,
    required this.amountPrice,
    required this.lotteryTime,
  });
  factory WcLotteryTimeInfo.fromJson(Map<String, dynamic> jsonData) {
    return WcLotteryTimeInfo(
      topic: jsonData['topic'],
      amountPrice: jsonData['amount_price'],
      lotteryTime: jsonData['lottery_time'],
    );
  }

  static Map<String, dynamic> toMap(WcLotteryTimeInfo wcFomo3dInfo) {
    return {
      'topic': wcFomo3dInfo.topic,
      'amount_price': wcFomo3dInfo.amountPrice,
      'lottery_time': wcFomo3dInfo.lotteryTime,
    };
  }

  static String encode(WcLotteryTimeInfo tradeInfoList) => json.encode(
      WcLotteryTimeInfo.toMap(tradeInfoList)
  );

  static WcLotteryTimeInfo decode(dynamic wcFomo3dInfo) {
    Map<String, dynamic> map = json.decode(wcFomo3dInfo.toString());
    return WcLotteryTimeInfo.fromJson(map);
  }
}
class WcEndResponseInfo {
  final String topic;
  String last_player;
  String createTime;
  String completeTime;
  int lotteryBonusPool;
  int numPeople;

  WcEndResponseInfo({
    required this.topic,
    required this.last_player,
    required this.createTime,
    required this.completeTime,
    required this.lotteryBonusPool,
    required this.numPeople,
  });
  factory WcEndResponseInfo.fromJson(Map<String, dynamic> jsonData) {
    return WcEndResponseInfo(
      topic: jsonData['topic'],
      last_player: jsonData['last_player'],
      createTime: jsonData['createTime'],
      completeTime: jsonData['completeTime'],
      lotteryBonusPool: jsonData['lotteryBonusPool'],
      numPeople: jsonData['numPeople'],
    );
  }

  static Map<String, dynamic> toMap(WcEndResponseInfo wcEndResponseInfo) {
    return {
      'topic': wcEndResponseInfo.topic,
      'last_player': wcEndResponseInfo.last_player,
      'createTime': wcEndResponseInfo.createTime,
      'completeTime': wcEndResponseInfo.completeTime,
      'lotteryBonusPool': wcEndResponseInfo.lotteryBonusPool,
      'numPeople': wcEndResponseInfo.numPeople,
    };
  }

  static String encode(WcEndResponseInfo wcEndResponseInfo) => json.encode(
      WcEndResponseInfo.toMap(wcEndResponseInfo)
  );

  static WcEndResponseInfo decode(dynamic wcEndResponseInfo) {
    Map<String, dynamic> map = json.decode(wcEndResponseInfo.toString());
    return WcEndResponseInfo.fromJson(map);
  }
}

class GamePersonalData {
  double batteryBalance;
  int costBattery;
  double interestIncome;
  double commissionIncome;

  GamePersonalData({
    required this.batteryBalance,
    required this.costBattery,
    required this.interestIncome,
    required this.commissionIncome,
  });
}

class GameHistoryInfo {
  int sumKm;
  int sumBattery;
  double costPrice;
  int sumParticipate;
  String winner;

  GameHistoryInfo({
    required this.sumKm,
    required this.sumBattery,
    required this.costPrice,
    required this.sumParticipate,
    required this.winner
  });
}

class AnimateTimeInfo {
  int? animateWin = 0;
  int animateSlow;
  int animateMedium;
  int animateFast;

  AnimateTimeInfo({
    this.animateWin,
    required this.animateSlow,
    required this.animateMedium,
    required this.animateFast,
  });
}

