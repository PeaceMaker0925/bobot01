import 'package:bobot01/constants/lightCell.dart';
import 'package:bobot01/ui/navigation_viewcontroller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../constants/constants.dart';
import '../../constants/shared_preference.dart';
import '../../constants/theme.dart';
import '../../main.dart';
import '../sign_in/login_viewcontroller.dart';
import '../sign_in/login_viewmodel.dart';

final int menuIndex = 16;

class MenuScreen extends ConsumerStatefulWidget {
  const MenuScreen({Key? key}) : super(key: key);

  @override
  _MenuScreenState createState() => _MenuScreenState();
}

class _MenuScreenState extends ConsumerState<MenuScreen> {

  bool isLogin = false;
  @override
  void initState() {
    BobotSharedPreferences().getUserToken().then((value) {
      setState(() {
        isLogin = value != "userToken";
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.transparent,
        body: SafeArea(
        child: lightCell(Container(margin: EdgeInsets.symmetric(horizontal: Constant().getViewHeight(context)/40), child: Column(children: [
      Expanded(flex: 2, child: Container()),
      Expanded(flex: 3, child: Container(height: Constant().getViewHeight(context)/20 , alignment: Alignment.center, child: Image.asset('assets/img/img_bobot_nor_01.png'),),),
      SizedBox(height: 5,),
      Expanded(flex: 3, child: Text("Bobot", style: TextStyle(fontSize: 28, color: CustomTheme.fontPurple01Color, fontWeight: FontWeight.w700))),
      Expanded(flex: 2, child: GestureDetector(child: Row(children: [
        Expanded(flex: 1, child: Container(height: Constant().getViewHeight(context)/20 , margin: EdgeInsets.all(Constant().getViewHeight(context)/80), child: Image.asset('assets/icon/icon_news_01.png'),),),
        Expanded(flex: 4, child: Container(child: Text("Activity", style: TextStyle(fontSize: 16, color: CustomTheme.fontPurple01Color, fontWeight: FontWeight.w700)))),
      ],),onTap: (){
        ref.read(bodyIndexProvider.notifier).setBodyIndex(menuIndex);
        scaffoldStateKey.currentState!.isEndDrawerOpen ? scaffoldStateKey.currentState!.closeEndDrawer() : scaffoldStateKey.currentState!.openEndDrawer();

      },)),
      Container(height: 1, alignment: Alignment.center, color: CustomTheme.fontPurple04Color,),
      Expanded(flex: 2, child: GestureDetector(child: Row(children: [
        Expanded(flex: 1, child: Container(height: Constant().getViewHeight(context)/20 , margin: EdgeInsets.all(Constant().getViewHeight(context)/80), child: Image.asset('assets/icon/icon_setting_01.png'),),),
        Expanded(flex: 4, child: Container(child: Text("Settings", style: TextStyle(fontSize: 16, color: CustomTheme.fontPurple01Color, fontWeight: FontWeight.w700)))),
      ],),onTap: (){
      ref.read(bodyIndexProvider.notifier).setBodyIndex(menuIndex+1);
      scaffoldStateKey.currentState!.isEndDrawerOpen ? scaffoldStateKey.currentState!.closeEndDrawer() : scaffoldStateKey.currentState!.openEndDrawer();

      })),
      Container(height: 1, alignment: Alignment.center, color: CustomTheme.fontPurple04Color,),
      Expanded(flex: 2, child: GestureDetector(child: Row(children: [
        Expanded(flex: 1, child: Container(height: Constant().getViewHeight(context)/20 , margin: EdgeInsets.all(Constant().getViewHeight(context)/80), child: Image.asset('assets/icon/icon_about_01.png'),),),
        Expanded(flex: 4, child: Container(child: Text("About Us", style: TextStyle(fontSize: 16, color: CustomTheme.fontPurple01Color, fontWeight: FontWeight.w700)))),
      ],),onTap: (){
        ref.read(bodyIndexProvider.notifier).setBodyIndex(menuIndex+2);
        scaffoldStateKey.currentState!.isEndDrawerOpen ? scaffoldStateKey.currentState!.closeEndDrawer() : scaffoldStateKey.currentState!.openEndDrawer();

      })),
      Container(height: 1, alignment: Alignment.center, color: CustomTheme.fontPurple04Color,),
      Expanded(flex: 2, child: GestureDetector(child: Row(children: [
        Expanded(flex: 1, child: Container(height: Constant().getViewHeight(context)/20 , margin: EdgeInsets.all(Constant().getViewHeight(context)/80), child: Image.asset('assets/icon/icon_sign out_01.png'),),),
        Expanded(flex: 4, child: Container(child: Text(isLogin ? "Sign Out" : "Log In", style: TextStyle(fontSize: 16, color: CustomTheme.fontPurple01Color, fontWeight: FontWeight.w700)))),
      ],),onTap: (){
        /// remove device api
        if(isLogin){
          ref.read(logOutProvider.future).then((event) => { event.then((value) => {
            if ((value).message ==  'SUCCESS') {
              CleanData(),
              Navigator.pushReplacement(navigatorKey.currentContext!, MaterialPageRoute(builder: (context) => LoginScreen()),)
            }
          })});
        }else {
          Navigator.pushReplacement(navigatorKey.currentContext!, MaterialPageRoute(builder: (context) => LoginScreen()),);
        }

      })),
      Expanded(flex: 6, child: Container())
    ],),
    ))));
  }
}