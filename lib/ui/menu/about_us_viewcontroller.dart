import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../constants/constants.dart';
import '../../constants/public_component.dart';
import '../../constants/theme.dart';

class AboutUsScreen extends ConsumerStatefulWidget {
  const AboutUsScreen({Key? key}) : super(key: key);

  @override
  _AboutUsScreenState createState() => _AboutUsScreenState();
}

class _AboutUsScreenState extends ConsumerState<AboutUsScreen> {

  String versionNum = "1.0.0";

  @override
  Widget build(BuildContext context) {
    return buildFlexBodyLayout(context, Container(
        color: Colors.transparent,
        margin: EdgeInsets.symmetric(horizontal: Constant().getViewWidth(context) / 20,),
        child: Column(children: [
          buildAppBar(title: "About Us", ref: ref, backPage: 0),
          Spacer(),
          Expanded(flex: 6, child: Container(margin: EdgeInsets.symmetric(horizontal: Constant().getViewWidth(context) / 20,),color: Colors.transparent, alignment: Alignment.center, child: Column(children: [
            Expanded(flex: 2, child: Container(height: Constant().getViewHeight(context)/12, child: Image.asset('assets/icon.png'),)),
            Expanded(flex: 1, child: Text('Bobot', style: GoogleFonts.balooTamma(
              textStyle: TextStyle(fontSize: 36, color: CustomTheme.fontPurple01Color),
            ),))
          ],),)),
          Spacer(),
          Expanded(flex: 8, child: Container(height: Constant().getViewHeight(context)/40,margin: EdgeInsets.symmetric(horizontal: Constant().getViewWidth(context) / 20,),alignment: Alignment.center, color: Colors.transparent, child: Column(children: [
            Expanded(flex: 1, child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
              Text("版本號", style: CustomTheme.textPrimaryPurple02,),
              Text(versionNum, style: CustomTheme.textPrimaryPurple02,),
            ],)),
              Expanded(flex: 1, child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
              Text("版本更新", style: CustomTheme.textPrimaryPurple02,),
              GestureDetector(child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(35),
                      color: CustomTheme.darkPurpleColor
                  ),
                  padding: EdgeInsets.symmetric(horizontal: 14),
                  child: Text("已是最新版本", style: CustomTheme.textSmallPrimary,)
              ),
                onTap: (){

                },)
            ],)),
              Expanded(flex: 1, child: Opacity(opacity: 0,
              child: GestureDetector(child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                Text("版本日誌", style: CustomTheme.textPrimaryPurple02,),
                Container(height: Constant().getViewWidth(context) / 16, child: Image.asset('assets/btn/btn_arrow_01_right.png'),)
              ],),onTap: (){

              },),)),
              Expanded(flex: 1, child: Opacity(opacity: 0,
                  child: GestureDetector(child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
              Text("隱私協議", style: CustomTheme.textPrimaryPurple02,),
              Container(height: Constant().getViewWidth(context) / 16, child: Image.asset('assets/btn/btn_arrow_01_right.png'),)
            ],),onTap: (){

            },)))
          ],))
          ),

          Expanded(flex: 12, child: Container())
        ])));
  }
}