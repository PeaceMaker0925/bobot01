import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../constants/constants.dart';
import '../../../constants/public_component.dart';
import '../../../constants/theme.dart';

class ActivityScreen extends ConsumerStatefulWidget {
  const ActivityScreen({Key? key}) : super(key: key);

  @override
  _ActivityState createState() => _ActivityState();
}

class _ActivityState extends ConsumerState<ActivityScreen> {
  @override
  Widget build(BuildContext context) {
    return buildFlexBodyLayout(context, Container(
        color: Colors.transparent,
        margin: EdgeInsets.symmetric(horizontal: Constant().getViewWidth(context) / 20,),
        child: Stack(children: [
          Container(margin: EdgeInsets.only(top: Constant().getViewHeight(context) * 38/100), alignment: Alignment.center, height: Constant().getViewHeight(context)/10, child: Image.asset('assets/img/img_bobot_nervous_01.png'),),
          Column(children: [
            Expanded(flex: 2, child: Container(child: buildAppBar(title: "Activity", ref: ref, backPage: 0))),
            Expanded(flex: 26, child: Container(alignment: Alignment.center, color: Colors.transparent, child: Container(child: Center(child:
            Text("Coming Soon", style: CustomTheme.textPrimaryPurple02,),),))
            )])])));
  }
}