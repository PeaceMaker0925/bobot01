import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../constants/constants.dart';
import '../../../constants/public_component.dart';
import '../../../constants/theme.dart';
import '../../sign_in/login_viewmodel.dart';
import '../menu_model.dart';
import '../menu_viewcontroller.dart';

class SettingsScreen extends ConsumerStatefulWidget {
  const SettingsScreen({Key? key}) : super(key: key);

  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends ConsumerState<SettingsScreen> {

  String currentLang = "繁體中文";
  String currentCoin = "TWD";
  int listLength = 6;

  @override
  Widget build(BuildContext context) {
    List<SettingsItem> settingsItemList = [
      SettingsItem(title: 'Language', action: () {  }, hint: currentLang ),
      SettingsItem(title: 'Currency', action: () {  }, hint: currentCoin ),
      SettingsItem(title: 'Notifications', action: () { ref.read(bodyIndexProvider.notifier).setBodyIndex(menuIndex+6); }, hint: '' ),
      SettingsItem(title: 'API Setting', action: () { ref.read(bodyIndexProvider.notifier).setBodyIndex(menuIndex+3); }, hint: '' ),
      SettingsItem(title: 'Change Password', action: () { ref.read(bodyIndexProvider.notifier).setBodyIndex(23); }, hint: '' ),
      SettingsItem(title: 'Device management', action: () { ref.read(bodyIndexProvider.notifier).setBodyIndex(24); }, hint: '' ),
    ];
    return buildFlexBodyLayout(context, Container(
        color: Colors.transparent,
        margin: EdgeInsets.symmetric(horizontal: Constant().getViewWidth(context) / 20,),
        child: Column(children: [
            Expanded(flex: 2, child: Container(child: buildAppBar(title: "Settings", ref: ref, backPage: 0))),
            Expanded(flex: 18, child: Container(alignment: Alignment.center, child: Column(children: settingsItemList.asMap().entries.map((value){
              return buildSettingsItem(value);
            }).toList(),))
            )
        ])));
  }
  Widget buildSettingsItem(MapEntry<int, SettingsItem> item){
    return Container(
      margin: EdgeInsets.symmetric(horizontal: Constant().getViewWidth(context) / 20,),
      padding: EdgeInsets.only(top: Constant().getViewWidth(context) / 40,),
      child: Column(children: [
        GestureDetector(child: Row(children: [
          Expanded(flex: 8, child: Text(item.value.title, style: CustomTheme.textPrimaryPurple02,)),
          Expanded(flex: 5, child: Text(item.value.hint, style: TextStyle(fontSize: 14, color: CustomTheme.fontPurple01Color), textAlign: TextAlign.right,)),
          Expanded(flex: 1, child: Container(height: Constant().getViewWidth(context) / 16, child: Image.asset('assets/btn/btn_arrow_01_right.png'),)),
        ],),onTap: (){item.value.action();},),
        item.key != listLength-1 ? Container(margin: EdgeInsets.only(top: Constant().getViewWidth(context) / 40,),
          height: 1, alignment: Alignment.center, color: CustomTheme.fontPurple04Color,) : Container()
      ],),
    );
  }
}