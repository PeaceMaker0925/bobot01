import 'package:bobot01/ui/sign_in/signup_nick_viewcontroller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_fonts/google_fonts.dart';
import '../../../constants/SignInWidgets.dart';
import '../../../constants/constants.dart';
import '../../../constants/public_component.dart';
import '../../../constants/theme.dart';
import '../../../constants/width_infinty_button.dart';

class ChangePWScreen extends ConsumerStatefulWidget {
  ChangePWScreen({Key? key}) : super(key: key);
  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _ChangePWScreenState();
}

class _ChangePWScreenState extends ConsumerState<ChangePWScreen> {
  TextEditingController old_pwTextController = TextEditingController();
  TextEditingController pwTextController = TextEditingController();
  TextEditingController pwConfirmTextController = TextEditingController();
  AsyncValue asyncResponse = const AsyncValue.loading();
  bool isLoading = false;
  bool isNextEnable = false;
  bool isPasswordShow = true;
  bool isPasswordWrong = false;
  bool isOldPasswordWrong = false;
  bool isPasswordDiff = false;
  FocusNode old_pwFocusNode = FocusNode();
  FocusNode pwFocusNode = FocusNode();
  FocusNode pwConfirmFocusNode = FocusNode();

  @override
  void initState() {
    old_pwTextController.addListener(() {
      RegExp regEx = RegExp(r"(?=.*[a-z])(?=.*[A-Z])\w+");
      isOldPasswordWrong = !regEx.hasMatch(old_pwTextController.text) || old_pwTextController.text.contains(' ') || !pwLengthValid(old_pwTextController.text);
      setState(() {});
    });

    pwTextController.addListener(() {
      RegExp regEx = RegExp(r"(?=.*[a-z])(?=.*[A-Z])\w+");
      isPasswordWrong = !regEx.hasMatch(pwTextController.text) || pwTextController.text.contains(' ') || !pwLengthValid(pwTextController.text);
      setState(() {});
    });

    pwConfirmTextController.addListener(() {
      isPasswordDiff = pwConfirmTextController.text != pwTextController.text;
      setState(() {

      });
    });
  }


  void unFocusAllNode(){
    pwFocusNode.unfocus();
    old_pwFocusNode.unfocus();
    pwConfirmFocusNode.unfocus();
  }

  @override
  Widget build(BuildContext context) {
    return buildFlexBodyLayout(context, InkWell(onTap: () { unFocusAllNode(); }, child: Container(
        margin: EdgeInsets.symmetric(horizontal: Constant().getViewWidth(context)/20),
        // color: Colors.transparent,
        child: Column(
            children: [
              buildAppBar(title: "Change Password", ref: ref, backPage: 17),
              Spacer(),
              Expanded(flex: 4, child: Container(color: Colors.transparent, alignment: Alignment.center, child: Column(children: [
                Expanded(flex: 2, child: Container(height: Constant().getViewHeight(context)/80, child: Image.asset('assets/icon.png'),)),
                Expanded(flex: 1, child: Text('Bobot', style: GoogleFonts.balooTamma(
                  textStyle: TextStyle(fontSize: 26, color: CustomTheme.fontPurple01Color),
                ),))
              ],),)),
              Spacer(),
              Expanded(flex: 2, child: SignInTextField(old_pwTextController, GestureDetector(child: Container(
                child: Icon(isPasswordShow ? Icons.remove_red_eye_outlined : Icons.visibility_off_outlined, color: CustomTheme.fontPurple01Color, size: 30,),
              ), onTap: (){ setState(() {
                isPasswordShow = !isPasswordShow;
              }); },), passwordVisible: isPasswordShow, old_pwFocusNode, "請輸入原始登入密碼")),
              Expanded(flex: 1, child: Container(child: Opacity(opacity: isOldPasswordWrong ? 1 : 0, child: Text('Password Format Error', style: TextStyle(
                color: CustomTheme.fontRed01Color,))),),),
              Expanded(flex: 2, child: SignInTextField(pwTextController, GestureDetector(child: Container(
                child: Icon(isPasswordShow ? Icons.remove_red_eye_outlined : Icons.visibility_off_outlined, color: CustomTheme.fontPurple01Color, size: 30,),
              ), onTap: (){ setState(() {
                isPasswordShow = !isPasswordShow;
              }); },), passwordVisible: isPasswordShow, pwFocusNode, "請輸入新密碼")),
              Expanded(flex: 1, child: Container(child: Opacity(opacity: isPasswordWrong ? 1 : 0, child: Text('Password Format Error', style: TextStyle(
                color: CustomTheme.fontRed01Color,))),),),
              Expanded(flex: 2, child: SignInTextField(pwConfirmTextController, GestureDetector(child: Container(
                child: Icon(isPasswordShow ? Icons.remove_red_eye_outlined : Icons.visibility_off_outlined, color: CustomTheme.fontPurple01Color, size: 30,),
              ), onTap: (){ setState(() {
                isPasswordShow = !isPasswordShow;
              }); },), passwordVisible: isPasswordShow, pwConfirmFocusNode, "請再次輸入新密碼")),
              Expanded(flex: 1, child: Container(child: Opacity(opacity: isPasswordDiff ? 1 : 0, child: Text('Password Not Match', style: TextStyle(
                color: CustomTheme.fontRed01Color,))),),),
              Expanded(flex: 1, child: Container(alignment: Alignment.centerLeft, child: Text("您的密碼須包含：", style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 14)),)),
              Expanded(flex: 1, child: Container(alignment: Alignment.centerLeft, child: Text("密碼須包含大小寫英文字母及數字", style: TextStyle(color: CustomTheme.fontPurple01Color, fontSize: 14)),)),
              Expanded(flex: 1, child: Container(alignment: Alignment.centerLeft, child: Text("密碼長度在8-20之內", style: TextStyle(color: CustomTheme.fontRed01Color, fontSize: 14)),)),
              Spacer(),
              Expanded(flex: 2, child: WidthInfintyButton(
                onTap: () { pressNextBtn(); },
                fontColor: Colors.white,
                foregroundColor: CustomTheme.fontPurple01Color,
                fontSize: 18,
                text: "完成修改密碼",
              ),),
              Expanded(flex: 6, child: Container()),
            ]))));
  }

  void pressNextBtn() {
    // ref.watch(signUpInfoProvider.notifier).setSignUpInfo(SignUpParameter(memberId: "",
    //     registerMode: "MAIL", email: emailTextController.text, country: "", phone: "",
    //     password: "",
    //     name: "", inviteCode: "UZAVW2BV46", imageUrl: ""));
    if(!isPasswordWrong && !isOldPasswordWrong && !isPasswordDiff){
      // ref.watch(forgetPWProvider(ForgetPasswordParameter(
      //     password: textController.text, registerMode: 'MAIL', email: ref.watch(accountProvider), country: '', phone: ''
      // )).future).then((event) {
      //   event.then((value) {
      //     print("${(value as ApiResponse).message}");
      //
      //     Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => NavigationScreen()), (Route<dynamic> route) => false);
      //
      //   });
      // });
    }

  }
}
