import 'package:bobot01/repository/api_response.dart';
import 'package:bobot01/ui/menu/settings/settings_viewmodel.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../constants/constants.dart';
import '../../../constants/public_component.dart';
import '../../../constants/theme.dart';
import '../menu_model.dart';
import '../menu_viewcontroller.dart';

class NotifyScreen extends ConsumerStatefulWidget {
  const NotifyScreen({Key? key}) : super(key: key);

  @override
  _NotifyScreenState createState() => _NotifyScreenState();
}

class _NotifyScreenState extends ConsumerState<NotifyScreen> {

  bool isEnable_lowEnergy = false;
  bool isEnable_lessPosition = false;
  bool isEnable_getProfit = false;
  bool isEnable_system = false;
  int listLength = 4;


  @override
  void initState() {
    ref.read(notifyInfoProvider.future).then((event) => {event.then((value) => {

    })});
  }

  void setNotifyEnable(ApiResponse value){
    setState(() {
      isEnable_lowEnergy = value.data["lowEnergyStatus"];
      isEnable_lessPosition = value.data["positionEnoughStatus"];
      isEnable_getProfit = value.data["receiveIncomeStatus"];
      isEnable_system = value.data["systemStatus"];
    });
  }

  @override
  Widget build(BuildContext context) {
    List<NotifyItem> notifyItemList = [
      NotifyItem(title: '低能量通知', action: () {

        ref.read(updateNotifyProvider(UpdateNotifyParameter(
            lowEnergyStatus: !isEnable_lowEnergy ? "ENABLE" : "DISABLE",
            positionEnoughStatus: isEnable_lessPosition ? "ENABLE" : "DISABLE",
            receiveIncomeStatus: isEnable_getProfit ? "ENABLE" : "DISABLE",
            systemStatus: isEnable_system ? "ENABLE" : "DISABLE"
        )).future).then((event) => { event.then((value) => {setNotifyEnable(value)}) });

        }, hint: "當能量低於 50 USDT 通知我" , isEnable: isEnable_lowEnergy),
      NotifyItem(title: '倉位不足通知', action: () {
        ref.read(updateNotifyProvider(UpdateNotifyParameter(
            lowEnergyStatus: isEnable_lowEnergy ? "ENABLE" : "DISABLE",
            positionEnoughStatus: !isEnable_lessPosition ? "ENABLE" : "DISABLE",
            receiveIncomeStatus: isEnable_getProfit ? "ENABLE" : "DISABLE",
            systemStatus: isEnable_system ? "ENABLE" : "DISABLE"
        )).future).then((event) => { event.then((value) => {setNotifyEnable(value)}) });

        }, hint: "當倉位低於幣威設定最小值通知我" , isEnable: isEnable_lessPosition),
      NotifyItem(title: '獲得收益通知', action: () {
        ref.read(updateNotifyProvider(UpdateNotifyParameter(
            lowEnergyStatus: isEnable_lowEnergy ? "ENABLE" : "DISABLE",
            positionEnoughStatus: isEnable_lessPosition ? "ENABLE" : "DISABLE",
            receiveIncomeStatus: !isEnable_getProfit ? "ENABLE" : "DISABLE",
            systemStatus: isEnable_system ? "ENABLE" : "DISABLE"
        )).future).then((event) => { event.then((value) => {setNotifyEnable(value)}) });

        }, hint: '當我獲得收益時通知我' , isEnable: isEnable_getProfit),
      NotifyItem(title: '系統通知', action: () {
        ref.read(updateNotifyProvider(UpdateNotifyParameter(
            lowEnergyStatus: isEnable_lowEnergy ? "ENABLE" : "DISABLE",
            positionEnoughStatus: isEnable_lessPosition ? "ENABLE" : "DISABLE",
            receiveIncomeStatus: isEnable_getProfit ? "ENABLE" : "DISABLE",
            systemStatus: !isEnable_system ? "ENABLE" : "DISABLE"
        )).future).then((event) => { event.then((value) => {setNotifyEnable(value)}) });

        }, hint: '接收來自系統通知推播，包含公告、活動等等' , isEnable: isEnable_system),
    ];
    return buildFlexBodyLayout(context, Container(
        color: Colors.transparent,
        margin: EdgeInsets.symmetric(horizontal: Constant().getViewWidth(context) / 20,),
        child: Column(children: [
          buildAppBar(title: "Notifications", ref: ref, backPage: menuIndex+1),
          Expanded(flex: 18, child: Container(alignment: Alignment.center, child: Column(children: notifyItemList.asMap().entries.map((value){
            return buildNotifyItem(value);
          }).toList(),))
          )
        ])));
  }
  Widget buildNotifyItem(MapEntry<int, NotifyItem> item){
    return Container(
      alignment: Alignment.centerLeft,
      margin: EdgeInsets.symmetric(horizontal: Constant().getViewWidth(context) / 20,),
      padding: EdgeInsets.only(top: Constant().getViewWidth(context) / 40,),
      child: Column(children: [
        GestureDetector(child: Row(children: [
          Expanded(flex: 12, child: Column(children: [
            Container(alignment: Alignment.centerLeft, child: Text(item.value.title, style: CustomTheme.textPrimaryPurple02, textAlign: TextAlign.start,)),
            Container(alignment: Alignment.centerLeft, child: Text(item.value.hint, style: TextStyle(fontSize: 14, color: CustomTheme.fontPurple02Color), textAlign: TextAlign.left,))
          ])),
          Spacer(),
          Expanded(flex: 2, child: Container(height: Constant().getViewWidth(context) / 10, child: Image.asset(item.value.isEnable?'assets/btn/btn_open_01_hover.png':'assets/btn/btn_close_01_nor.png'),)),
        ],),onTap: (){item.value.action();},),
        item.key != listLength-1 ? Container(margin: EdgeInsets.only(top: Constant().getViewWidth(context) / 40,),
          height: 1, alignment: Alignment.center, color: CustomTheme.fontPurple04Color,) : Container()
      ],),
    );
  }
}