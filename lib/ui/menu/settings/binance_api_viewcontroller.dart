import 'package:bobot01/ui/menu/settings/settings_viewmodel.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../constants/public_component.dart';
import '../../../constants/constants.dart';
import '../../../constants/theme.dart';
import '../../../main.dart';
import '../../sign_in/login_viewmodel.dart';
import '../menu_viewcontroller.dart';

class BinanceApiScreen extends ConsumerStatefulWidget {
  const BinanceApiScreen({Key? key}) : super(key: key);

  @override
  _BinanceApiScreenState createState() => _BinanceApiScreenState();
}

class _BinanceApiScreenState extends ConsumerState<BinanceApiScreen> {
  TextEditingController accessKeyTextController = TextEditingController();
  TextEditingController SecretKeyTextController = TextEditingController();
  FocusNode accessKeyFocusNode = FocusNode();
  FocusNode secretKeyFocusNode = FocusNode();

  void unFocusAllNode(){
    accessKeyFocusNode.unfocus();
    secretKeyFocusNode.unfocus();
  }

  @override
  Widget build(BuildContext context) {
    return buildFlexBodyLayout(context, InkWell(onTap: () { unFocusAllNode(); }, child: Container(
        color: Colors.transparent,
        margin: EdgeInsets.symmetric(horizontal: Constant().getViewWidth(context) / 20,),
        child: Column(children: [
          Expanded(flex: 2, child: Container(child: buildAppBar(title: "BINANCE API", ref: ref, backPage: menuIndex+3))),
          Expanded(flex: 1, child: Container(alignment: Alignment.centerLeft, child: Text("1.請於幣安確認以下功能：", style: CustomTheme.textPrimaryPurple02,))),
          Expanded(flex: 1, child: Container(alignment: Alignment.centerLeft, child: Text("確認已啟用合約帳戶\n確認合約交易中偏好設置→倉位模式為單向持倉", style: CustomTheme.textPrimaryPurple02Thin,))),
          Expanded(flex: 1, child: Container(alignment: Alignment.centerLeft, child: Text("2.在幣安創建API，接著點擊權限設置：", style: CustomTheme.textPrimaryPurple02,))),
          Expanded(flex: 1, child: Container(alignment: Alignment.centerLeft, child: Text("啟用讀取、允許現貨及槓桿交易、允許合約、允許萬向劃轉，並保存", style: CustomTheme.textPrimaryPurple02Thin,))),
          Expanded(flex: 1, child: Container(alignment: Alignment.centerLeft, child: Text("3.分別複製API Key：", style: CustomTheme.textPrimaryPurple02,))),
          Expanded(flex: 1, child: Container(alignment: Alignment.centerLeft, child: Text("請注意Secret Key只會出現一次，請務必保管好您的Secret Key", style: CustomTheme.textPrimaryPurple02Thin,))),
          Expanded(flex: 1, child: Container(alignment: Alignment.centerLeft, child: Text("4.輸入或者黏貼您的API Key", style: CustomTheme.textPrimaryPurple02,))),
          Expanded(flex: 1, child: Container(alignment: Alignment.centerLeft, child: Text("Access Key", style: CustomTheme.textPrimaryPurple02,))),
          Expanded(flex: 1, child: Container(alignment: Alignment.centerLeft, child: textFieldCell(accessKeyTextController, "請輸入Access Key", accessKeyFocusNode)),),
          Expanded(flex: 1, child: Container(alignment: Alignment.centerLeft, child: Text("Secret Key", style: CustomTheme.textPrimaryPurple02,))),
          Expanded(flex: 1, child: Container(alignment: Alignment.centerLeft, child: textFieldCell(SecretKeyTextController,"請輸入Secret Key", secretKeyFocusNode)),),
          Expanded(flex: 1, child: Container(),),
          Expanded(flex: 1, child: GestureDetector(child: Container(
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(35),
                  color: CustomTheme.fontPurple01Color
              ),
              child: Text("綁定API", style: CustomTheme.textPrimary14,)),
          onTap: (){
            ref.read(bindApiProvider(ApiKeyParameter(
                marketName: "Binance",
                accessKey: accessKeyTextController.text,
                secretKey: SecretKeyTextController.text
            )).future).then((event) {
              event.then((value) {
                if(value.message.compareTo("SUCCESS") == 0) {
                  // Navigator.pop(context);
                  ref.read(bodyIndexProvider.notifier).setBodyIndex(menuIndex+5);
                }else showResponseDialog(context, "${(value).url}\n\n${(value).code}\n\n${(value).message}\n\n${(value).data}", (){});
              });
            });

            //ref.read(bodyIndexProvider.notifier).setBodyIndex(menuIndex+5);
          },),),
          Expanded(flex: 3, child: Container(),),
        ]))));
  }
  Widget textFieldCell(TextEditingController controller, String hint, FocusNode focusNode){
    return Container(
        alignment: Alignment.centerLeft,
        padding: EdgeInsets.symmetric(horizontal: 5.0),
        decoration: BoxDecoration(
            border: Border.all(color: CustomTheme.fontPurple02Color),
            borderRadius: BorderRadius.circular(35),
            color: Colors.white
        ),
        child: TextFormField(
          controller: controller,
          focusNode: focusNode,
          textAlign: TextAlign.center,
          textAlignVertical: TextAlignVertical.center,
          decoration: InputDecoration(
            isDense: true,
            border: InputBorder.none,
            hintText: hint,
            hintStyle: TextStyle(color: CustomTheme.fourthGrayColor)
          ),
          style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 14),
        )
    );
  }

}