

import 'package:equatable/equatable.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../repository/api_response.dart';
import '../../../repository/settings_api_repository.dart';

final agentApiProvider = FutureProvider.autoDispose<Future<ApiResponse>>((ref) async {
  return SettingsApiRepositoryImpl().queryApiInfo();
});

final bindApiProvider = FutureProvider.autoDispose.family<Future<ApiResponse>, ApiKeyParameter>((ref, parameter) async {
  return SettingsApiRepositoryImpl().bindApi(parameter);
});

final cleanApiProvider = FutureProvider.autoDispose.family<Future<ApiResponse>, String>((ref, marketName) async {
  return SettingsApiRepositoryImpl().cleanApi(marketName);
});

final notifyInfoProvider = FutureProvider.autoDispose<Future<ApiResponse>>((ref) async {
  return SettingsApiRepositoryImpl().queryNotifyInfo();
});

final updateNotifyProvider = FutureProvider.autoDispose.family<Future<ApiResponse>, UpdateNotifyParameter>((ref, parameter) async {
  return SettingsApiRepositoryImpl().updateNotify(parameter);
});

final changePWProvider = FutureProvider.autoDispose.family<Future<ApiResponse>, UpdateNotifyParameter>((ref, parameter) async {
  return SettingsApiRepositoryImpl().updateNotify(parameter);
});

class ApiKeyParameter extends Equatable {
  final String marketName;
  final String accessKey;
  final String secretKey;

  const ApiKeyParameter({
    required this.marketName,
    required this.accessKey,
    required this.secretKey,
  });

  @override
  List<Object?> get props => [marketName, accessKey, secretKey];
}

class UpdateNotifyParameter extends Equatable {
  final String lowEnergyStatus;
  final String positionEnoughStatus;
  final String receiveIncomeStatus;
  final String systemStatus;

  const UpdateNotifyParameter({
    required this.lowEnergyStatus,
    required this.positionEnoughStatus,
    required this.receiveIncomeStatus,
    required this.systemStatus,
  });

  @override
  List<Object?> get props => [lowEnergyStatus, positionEnoughStatus, receiveIncomeStatus, systemStatus];
}

class ChangePasswordParameter extends Equatable {
  final String lowEnergyStatus;
  final String positionEnoughStatus;
  final String receiveIncomeStatus;
  final String systemStatus;

  const ChangePasswordParameter({
    required this.lowEnergyStatus,
    required this.positionEnoughStatus,
    required this.receiveIncomeStatus,
    required this.systemStatus,
  });

  @override
  List<Object?> get props => [lowEnergyStatus, positionEnoughStatus, receiveIncomeStatus, systemStatus];
}