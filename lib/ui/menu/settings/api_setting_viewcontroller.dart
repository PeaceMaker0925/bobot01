import 'package:bobot01/ui/menu/settings/settings_viewmodel.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../constants/constants.dart';
import '../../../constants/public_component.dart';
import '../../../constants/theme.dart';
import '../../bot/create_bot_viewcontroller.dart';
import '../../sign_in/login_viewmodel.dart';
import '../menu_model.dart';
import '../menu_viewcontroller.dart';

class APISettingScreen extends ConsumerStatefulWidget {

  @override
  _APISettingScreenState createState() => _APISettingScreenState();
}

class _APISettingScreenState extends ConsumerState<APISettingScreen> {

  String bind_Binance = "Not Bind Yet";
  String bind_Huobi = "Not Bind Yet";


  @override
  void initState() {

    ref.read(agentApiProvider.future).then((event) {
      event.then((value) {
        List<dynamic> list = value.data;
        bind_Binance = list.first['bind'] == true ? "Has Binded" : "Not Bind Yet";
        // todayProfit = value.data['todayIncome'];
        // lastDayProfit = value.data['yesterdayIncome'];
        setState(() {});
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    List<ApiItem> apiItemList = [
      ApiItem(title: marketList.last, action: () {  }, hint: bind_Huobi, imgPath: 'assets/huobi_icon.png' ),
      ApiItem(title: marketList.first, action: () {
        bind_Binance == "Not Bind Yet"
        ? ref.read(bodyIndexProvider.notifier).setBodyIndex(menuIndex+4)
        : ref.read(bodyIndexProvider.notifier).setBodyIndex(menuIndex+5);
        }, hint: bind_Binance, imgPath: 'assets/binance_icon.png' ),
    ];
    return buildFlexBodyLayout(context, Container(
        color: Colors.transparent,
        margin: EdgeInsets.symmetric(horizontal: Constant().getViewWidth(context) / 20,),
        child: Column(children: [
          buildAppBar(title: "API Setting", ref: ref, backPage: menuIndex+1),
          Expanded(flex: 18, child: Container(alignment: Alignment.center, margin: EdgeInsets.symmetric( vertical: Constant().getViewWidth(context) / 20) , child: Column(children: apiItemList.asMap().entries.map((value){
            return buildApiItem(value);
          }).toList(),))
          )])));
  }

  Widget buildApiItem(MapEntry<int, ApiItem> item){
    return Container(
      margin: EdgeInsets.symmetric(horizontal: Constant().getViewWidth(context) / 20),
      padding: EdgeInsets.only(top: Constant().getViewWidth(context) / 40,),
      child: Column(children: [
        GestureDetector(child: Row(children: [
          Expanded(flex: 2, child: Container(height: Constant().getViewWidth(context) / 16, child: Image.asset(item.value.imgPath),)),
          Expanded(flex: 6, child: Text(item.value.title, style: CustomTheme.textPrimaryPurple02,)),
          Expanded(flex: 5, child: Text(item.value.hint, style: TextStyle(fontSize: 14, color: CustomTheme.fontPurple01Color), textAlign: TextAlign.right,)),
          Expanded(flex: 1, child: Container(height: Constant().getViewWidth(context) / 16, child: Image.asset('assets/btn/btn_arrow_01_right.png'),)),
        ],),onTap: (){item.value.action();},),
        item.key != 1 ? Container(margin: EdgeInsets.only(top: Constant().getViewWidth(context) / 40,),
          height: 1, alignment: Alignment.center, color: CustomTheme.fontPurple04Color,) : Container()
      ],),
    );
  }
}