import 'package:bobot01/ui/menu/settings/settings_viewmodel.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../constants/lightCell.dart';
import '../../../constants/public_component.dart';
import '../../../constants/constants.dart';
import '../../../constants/theme.dart';
import '../../../constants/width_infinty_button.dart';
import '../../../main.dart';
import '../../sign_in/login_viewmodel.dart';
import '../menu_viewcontroller.dart';

class BinanceBindScreen extends ConsumerStatefulWidget {
  const BinanceBindScreen({Key? key}) : super(key: key);

  @override
  _BinanceBingScreenState createState() => _BinanceBingScreenState();
}

class _BinanceBingScreenState extends ConsumerState<BinanceBindScreen> {

  String accessKey = "D0A**********************DUO";
  String secretKey = "qoD**********************NeF";

  @override
  void initState() {

    ref.read(agentApiProvider.future).then((event) {
      event.then((value) {
        List<dynamic> list = value.data;
        dynamic apiKey = list.first['apikey'] ;
        print(apiKey);
        accessKey = apiKey["accessKey"];
        secretKey = apiKey["secretKey"];
        // todayProfit = value.data['todayIncome'];
        // lastDayProfit = value.data['yesterdayIncome'];
        setState(() {});
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return buildFlexBodyLayout(context, Container(
        color: Colors.transparent,
        margin: EdgeInsets.symmetric(horizontal: Constant().getViewWidth(context) / 20,),
        child: Column(children: [
          buildAppBar(title: "BINANCE API", ref: ref, backPage: menuIndex+3),
          Expanded(flex: 6, child: Container(alignment: Alignment.centerLeft, child: Text("為了保護您的資料安全以及降低錯誤風險，目前僅開放刪除API綁定功能。若您欲修改API，請先刪除再進行綁定"
            "\n\n您的API Key 將用於同步持倉和交易數據。API key 會保存在我們加密的服務器上且我們會保證其安全"
              "\n\n以下是您的API Key：", style: CustomTheme.textPrimaryPurple02Thin,))),
          Expanded(flex: 8, child: Container(
            alignment: Alignment.center,
            child: lightCell(Container(
              margin: EdgeInsets.all(Constant().getViewWidth(context)/40),
              child: Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
                Expanded(flex: 2, child: Container(alignment: Alignment.centerLeft, child: Text("Access Key", style: CustomTheme.textPrimaryPurple02,))),
                Expanded(flex: 2, child: Container(alignment: Alignment.centerLeft, child: Text(accessKey, style: CustomTheme.textPrimaryPurple02,))),
                Expanded(flex: 2, child: Container(alignment: Alignment.centerLeft, child: Text("Secret Key", style: CustomTheme.textPrimaryPurple02,))),
                Expanded(flex: 2, child: Container(alignment: Alignment.centerLeft, child: Text(secretKey, style: CustomTheme.textPrimaryPurple02,))),
                Expanded(flex: 3, child: GestureDetector(child: Container(
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(35),
                        color: CustomTheme.fontPurple01Color
                    ),
                    child: Text("確定已延展API", style: CustomTheme.textPrimary14,)),
                  onTap: (){
                    showResponseDialog(context, title: "延展API", "確認以至交易所延展API?\n若沒有延長API，屆時機器人教無法運作",
                            (){ }, img: 'assets/img/img_bobot_nervous_01.png');
                  },),),
              ],),)),)),
          Expanded(flex: 4, child: Container()),
          Expanded(flex: 2, child: GestureDetector(child: Container(
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(35),
                  color: CustomTheme.fontPurple01Color
              ),
              child: Text("刪除API", style: CustomTheme.textPrimary14,)),
            onTap: (){
              showResponseDialog(context, title: "刪除API", "確認刪除此API？刪除後您需重新綁定才能繼續使用量化功能",
                      (){ }, img: 'assets/img/img_bobot_no_01.png');
            },),),
          Expanded(flex: 4, child: Container(),),
        ])));
  }
  Widget textFieldCell(TextEditingController controller, String hint){
    return Container(
        alignment: Alignment.centerLeft,
        padding: EdgeInsets.symmetric(horizontal: 5.0),
        decoration: BoxDecoration(
            border: Border.all(color: CustomTheme.fontPurple02Color),
            borderRadius: BorderRadius.circular(35),
            color: Colors.white
        ),
        child: TextFormField(
          controller: controller,
          textAlign: TextAlign.center,
          textAlignVertical: TextAlignVertical.center,
          decoration: InputDecoration(
              isDense: true,
              border: InputBorder.none,
              hintText: hint,
              hintStyle: TextStyle(color: CustomTheme.fourthGrayColor)
          ),
          style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 14),
        )
    );
  }

  Widget ExtendDialog(){
    return lightCell(isBar: true, Container(
      margin: EdgeInsets.symmetric(horizontal: Constant().getViewWidth(context)/40),
      child: Column(
          children: [
            Spacer(),
            Expanded(flex: 2, child: Container(height: Constant().getViewHeight(context)/8, child: Image.asset('assets/img/img_bobot_nervous_01.png'),)),
            Expanded(flex: 2, child: Container(alignment: Alignment.center, child: Text("延展API", style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 14)),)),
            Expanded(flex: 2, child: Container(alignment: Alignment.center, child: Text(
              "",
              style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 14),
              textAlign: TextAlign.center,
            ),)),
            Expanded(flex: 1, child: Row(children: [
              Spacer(),
              Expanded(flex: 2, child: GestureDetector(
                onTap: () { Navigator.pop(context); },
                child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: CustomTheme.fontPurple01Color,
                    borderRadius: BorderRadius.all(Radius.circular(35)),
                  ),
                  child: Text("Confirm", style: CustomTheme.textPrimary,),
                ),),),
              Spacer()
            ],)),
            Spacer()
          ]
      ),));
  }

  Widget DeleteDialog(){
    return lightCell(isBar: true, Container(
      margin: EdgeInsets.symmetric(horizontal: Constant().getViewWidth(context)/40),
      child: Column(
          children: [
            Spacer(),
            Expanded(flex: 2, child: Container(height: Constant().getViewHeight(context)/8, child: Image.asset('assets/img/img_bobot_no_01.png'),)),
            Expanded(flex: 2, child: Container(alignment: Alignment.center, child: Text("刪除API", style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 14)),)),
            Expanded(flex: 2, child: Container(alignment: Alignment.center, child: Text(
              "確認刪除此API？刪除後您需重新綁定才能繼續使用量化功能",
              style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 14),
              textAlign: TextAlign.center,
            ),)),
            Expanded(flex: 1, child: Row(children: [
              Expanded(flex: 6, child: GestureDetector(
                onTap: () { Navigator.pop(context); },
                child: Container(
                  alignment: Alignment.center,
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(width: 1, color: CustomTheme.fontPurple01Color),
                  borderRadius: BorderRadius.all(Radius.circular(35)),
                ),
                child: Text("Cancel", style: CustomTheme.textPrimaryPurple01,),
              ),),),
              Spacer(),
              Expanded(flex: 6, child: GestureDetector(
                onTap: () { pressConfirmBtn(); },
                child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: CustomTheme.fontPurple01Color,
                    borderRadius: BorderRadius.all(Radius.circular(35)),
                  ),
                  child: Text("Confirm", style: CustomTheme.textPrimary,),
                ),),)
            ],)),
            Spacer()
          ]
      ),));
  }

  void pressConfirmBtn() {
    ref.watch(cleanApiProvider("Binance").future).then((event) {
      event.then((value) {
        if(value.message.compareTo("SUCCESS") == 0) {
          Navigator.pop(context);
          ref.read(bodyIndexProvider.notifier).setBodyIndex(menuIndex+3);
        }else showResponseDialog(context, "${(value).url}\n\n${(value).code}\n\n${(value).message}\n\n${(value).data}", (){});
      });
    });
  }

}