
class SettingsItem {
  String title;
  String hint;
  Function() action;
  SettingsItem({
    required this.title,
    required this.hint,
    required this.action
  });
}

class NotifyItem {
  String title;
  String hint;
  Function() action;
  bool isEnable;
  NotifyItem({
    required this.title,
    required this.hint,
    required this.action,
    required this.isEnable
  });
}

class ApiItem {
  String imgPath;
  String title;
  String hint;
  Function() action;
  ApiItem({
    required this.imgPath,
    required this.title,
    required this.hint,
    required this.action
  });
}