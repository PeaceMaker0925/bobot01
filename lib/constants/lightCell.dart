import 'package:bobot01/constants/theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import '../main.dart';
import 'constants.dart';

Widget lightCell(Widget widget, {bool isBar = false}){
  var context = navigatorKey.currentContext!;
  return Container(
    alignment: Alignment.center,
    decoration: BoxDecoration(
      color: CustomTheme.darkPurpleColor,
      border: Border.all(color: CustomTheme.darkPurpleColor),
      borderRadius: BorderRadius.all(Radius.circular(isBar ? 50.0 : 20),),
    ),
    child: Container(
      alignment: Alignment.center,
      padding: EdgeInsets.all(Constant().getViewWidth(context)/80),
      margin: EdgeInsets.only(left: isBar ? 3 : 1, top: 1, bottom: Constant().getViewWidth(context)/60, right: Constant().getViewWidth(context)/80, ),
      decoration: BoxDecoration(
        color: isBar ? Colors.white : CustomTheme.cellBackGroundColor,
        boxShadow: [BoxShadow(
          color: CustomTheme.lightPurpleColor,
          spreadRadius: 2,
          blurRadius: 0,
          offset: Offset(2, 2), // changes position of shadow
        ),
        ],
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(isBar ? 50.0 : 15),
            topRight: Radius.circular(isBar ? 50.0 : 15),
            bottomLeft: Radius.circular(isBar ? 50.0 : 15),
            bottomRight: Radius.circular(isBar ? 50.0 : 20)),),
      child: widget,
    ),);
}