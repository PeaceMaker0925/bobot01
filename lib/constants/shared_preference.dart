import 'package:shared_preferences/shared_preferences.dart';
import 'package:uuid/uuid.dart';

const userToken = "userToken";
const deviceToken = "deviceToken";
const currentAccount = "currentAccount";
const memberId = "memberId";
const language = 'language';
const notiToken = 'NotiToken';

class BobotSharedPreferences {

  static Future<SharedPreferences> _getPreferences() async {
    return await SharedPreferences.getInstance();
  }

  Future<String> getUserToken() async {
    final prefs = await _getPreferences();
    return prefs.getString(userToken) ?? 'userToken';
  }

  Future<void> saveUserToken(String token) async {
    final prefs = await _getPreferences();
    prefs.setString(userToken, token);
  }

  Future<String> getNotiToken() async {
    final prefs = await _getPreferences();
    return prefs.getString(notiToken) ?? '';
  }

  Future<void> setNotiToken(String token) async {
    final prefs = await _getPreferences();
    prefs.setString(notiToken, token);
  }

  static Future<void> setLanguage(String lang) async {
    final prefs = await _getPreferences();
    prefs.setString(language, lang);
  }

  static Future<String> getLanguage() async {
    final prefs = await _getPreferences();
    return prefs.getString(language) ?? 'zh-TW';  }

  Future<String> getMemberId() async {
    final prefs = await _getPreferences();
    return prefs.getString(memberId) ?? Uuid().v1();
  }

  Future<void> saveMemberId(String id) async {
    final prefs = await _getPreferences();
    prefs.setString(memberId, id);
  }

  Future<String> getDeviceToken() async {
    final prefs = await _getPreferences();
    return prefs.getString(deviceToken) ?? 'deviceToken';
  }

  Future<void> saveDeviceToken(String deviceToken) async {
    final prefs = await _getPreferences();
    prefs.setString(deviceToken, deviceToken);
  }

  Future<String> getCurrentAccount() async {
    final prefs = await _getPreferences();
    return prefs.getString(currentAccount) ?? ' - Log out - ';
  }

  Future<void> saveCurrentAccount(String account) async {
    final prefs = await _getPreferences();
    prefs.setString(currentAccount, account);
  }
}