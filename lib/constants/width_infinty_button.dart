import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WidthInfintyButton extends StatelessWidget {
  const WidthInfintyButton(
      {super.key, required this.onTap, required this.foregroundColor, required this.fontColor, required this.fontSize, required this.text});
  final GestureTapCallback onTap;
  final Color foregroundColor;
  final Color fontColor;
  final double fontSize;
  final String text;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: double.infinity,
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            primary: foregroundColor,
            shape:
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(35.0),
              ),
          ),
          onPressed: onTap,
          child: Text(
            text,
            style: TextStyle(color: fontColor, fontFamily: "ubuntu",  fontSize: fontSize),
          ),
        ),
      );
  }
}