import 'package:flutter/material.dart';

class CustomTheme {

  static const Color primaryWhiteColor = Color(0xFFD9DDE0);

  static const Color fontPurple01Color = Color(0xFF637FFF);
  static const Color fontPurple02Color = Color(0xFF5161AB);
  static const Color darkPurpleColor = Color(0xFFADBBFC);
  static const Color lightPurpleColor = Color(0xFFCED7FF);
  static const Color cellBackGroundColor = Color(0xFFF1F4FF);
  static const Color fontPurple04Color = Color(0xFFDCE2FF);
  static const Color cellYellowColor = Color(0xFFFCFF66);

  static const Color appBarGrayColor = Color(0xFF333333);
  static const Color primaryGrayColor = Color(0xFF666666);
  static const Color secondBlueColor = Color(0xFF0C2449);
  static const Color thirdBlueColor = Color(0xFF20375A);
  static const Color primaryRedColor = Color(0xFFFF3100);

  static const LinearGradient linearBackgroundPrimary = LinearGradient(colors: [Color(
      0xFF040D18), Color(0xFF133369)], begin: Alignment.topCenter, end: Alignment.bottomCenter);
  static const LinearGradient linearBackgroundSecond = LinearGradient(colors: [Color(
      0xFF1B2A3B), Color(0xFF213C67)], begin: Alignment.topCenter, end: Alignment.bottomCenter);

  // static const Color primaryGrayColor = Color(0xFF2B394D);
  static const Color secondGrayColor = Color(0xFF425874);
  static const Color thirdGrayColor = Color(0xFF394A5F);
  static const Color fourthGrayColor = Color(0xFF5D7990);
  static const Color fivethGrayColor = Color(0xFF22416F);
  static const Color sixthGrayColor = Color(0xFF1B2C40);

  static const Color labelWhiteColor = Colors.white;
  static const Color labelGreenColor = Color(0xFF287481);
  static const Color fontGreen01Color = Color(0xFF93EC7D);
  static const Color fontRed01Color = Color(0xFFFF6C63);
  static const Color labelGrayColor = Color(0xFFAFB6BE);
  static const Color buttonBlueColor = Color(0xFF1595EF);


//  static const List<Color> nftBgColors = [
//    CustomTheme.primaryBlueColor,
//    CustomTheme.secondBlueColor,
//    Colors.deepPurpleAccent,
//  ];

  // text white
  static const TextStyle textSmallPrimaryThin = TextStyle(color: labelWhiteColor, fontSize: 12, fontWeight: FontWeight.w300);
  static const TextStyle textSmallPrimary = TextStyle(color: labelWhiteColor, fontSize: 12, fontWeight: FontWeight.w600);
  static const TextStyle textPrimary14 = TextStyle(color: labelWhiteColor, fontSize: 14, fontWeight: FontWeight.w600);
  static const TextStyle textPrimary = TextStyle(color: labelWhiteColor, fontSize: 16, fontWeight: FontWeight.w600);
  static const TextStyle textMonster = TextStyle(color: labelWhiteColor, fontSize: 20, fontWeight: FontWeight.w600);
  static const TextStyle textBigMonster = TextStyle(color: labelWhiteColor, fontSize: 28, fontWeight: FontWeight.w600);

  static const TextStyle textPrimaryThird = TextStyle(color: labelGrayColor, fontSize: 14, fontWeight: FontWeight.w600);
  static const TextStyle textPrimaryPurple01 = TextStyle(color: fontPurple01Color, fontSize: 16, fontWeight: FontWeight.w600);
  static const TextStyle bigTextPrimaryPurple01 = TextStyle(color: fontPurple01Color, fontSize: 18, fontWeight: FontWeight.w600);
  static const TextStyle bigTextPrimaryPurple02 = TextStyle(color: fontPurple02Color, fontSize: 18, fontWeight: FontWeight.w600);
  static const TextStyle textPrimaryPurple02 = TextStyle(color: fontPurple02Color, fontSize: 14, fontWeight: FontWeight.w600);
  static const TextStyle textPrimaryPurple02Thin = TextStyle(color: fontPurple02Color, fontSize: 14, fontWeight: FontWeight.w300);
  static const TextStyle textSmallPrimaryThird = TextStyle(color: labelGrayColor, fontSize: 12, fontWeight: FontWeight.w600);

  // text green
  static const TextStyle textSmallSecond = TextStyle(color: labelGreenColor, fontSize: 12, fontWeight: FontWeight.w600);
  static const TextStyle textSecond = TextStyle(color: labelGreenColor, fontSize: 16, fontWeight: FontWeight.w600);
}
