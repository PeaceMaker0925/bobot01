import 'package:bobot01/constants/theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../main.dart';
import 'constants.dart';

Widget CircleStepView({required int step}) {
  return Container(
    // color: Colors.cyanAccent,
    alignment: Alignment.center,
    // margin: EdgeInsets.symmetric(vertical: Constant().getViewHeight(navigatorKey.currentContext!)/60),
    child: Stack(children: [
      Center(child: Container( height: 1, alignment: Alignment.center, color: CustomTheme.fontPurple02Color,)),
      Container(alignment: Alignment.centerLeft, child: Row(children: [
        Container(height: 30,width: 30,decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(300)),
            color: step == 1 ? CustomTheme.fontPurple02Color : Colors.white,
            border: Border.all(color: CustomTheme.fontPurple02Color)),
          child: Center(child: Text("1",style: TextStyle(color: step == 1 ? Colors.white : CustomTheme.fontPurple02Color ,),),),
        ),
        Container(height: 30,width: 30,decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(300)),
            color: step == 2 ? CustomTheme.fontPurple02Color : Colors.white,
            border: Border.all(color: CustomTheme.fontPurple02Color)),
          child: Center(child: Text("2",style: TextStyle(color: step == 2 ? Colors.white : CustomTheme.fontPurple02Color ,)),),
        ),
        Container(height: 30,width: 30,decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(300)),
            color: step == 3 ? CustomTheme.fontPurple02Color : Colors.white,
            border: Border.all(color: CustomTheme.fontPurple02Color)),
          child: Center(child: Text("3",style: TextStyle(color: step == 3 ? Colors.white : CustomTheme.fontPurple02Color ,)),),
        ),
      ],
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
      ),)
  ],),);
}