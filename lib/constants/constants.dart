import 'package:bobot01/ui/bot/bot_viewcontroller.dart';
import 'package:bobot01/ui/copy/copy_viewcontroller.dart';
import 'package:bobot01/ui/game/game_viewcontroller.dart';
import 'package:bobot01/ui/home/home_viewcontroller.dart';
import 'package:bobot01/ui/profile/account/deposit/deposit_viewcontroller.dart';
import 'package:bobot01/ui/profile/account/internal/internal_viewcontroller.dart';
import 'package:flutter/cupertino.dart';

import '../ui/bot/create_bot_viewcontroller.dart';
import '../ui/copy/create_copy_bot_viewcontroller.dart';
import '../ui/menu/about_us_viewcontroller.dart';
import '../ui/menu/activity/activity_viewcontroller.dart';
import '../ui/menu/menu_viewcontroller.dart';
import '../ui/menu/settings/api_setting_viewcontroller.dart';
import '../ui/menu/settings/binance_api_viewcontroller.dart';
import '../ui/menu/settings/binance_bind_viewcontroller.dart';
import '../ui/menu/settings/change_pw_viewcontroller.dart';
import '../ui/menu/settings/notification_viewcontroller.dart';
import '../ui/menu/settings/settings_viewcontroller.dart';
import '../ui/profile/account/account_viewcontroller.dart';
import '../ui/profile/account/send/send_viewcontroller.dart';
import '../ui/profile/account/transfer/transfer_viewcontroller.dart';
import '../ui/profile/contact_viewcontroller.dart';
import '../ui/profile/profile_viewcontroller.dart';
import '../ui/profile/profit_viewcontroller.dart';
import '../ui/profile/referral_viewcontroller.dart';

const String rootRouteName = '/root';
const String cartRouteName = '/cart';
const String createAccountRouteName = '/create-account';
const String detailsRouteName = '/details';
const String homeRouteName = '/home';
const String loggedInKey = 'LoggedIn';
const String loginRouteName = '/login';
const String launchRouteName = '/launch';
const String emailPhoneRouteName = '/email-phone';
const String verifyCodeRouteName = '/verify-code';
const String setPasswordRouteName = '/set-password';
const String moreInfoRouteName = '/moreInfo';
const String paymentRouteName = '/payment';
const String personalRouteName = '/personal';
const String profileMoreInfoRouteName = '/profile-moreInfo';
const String profilePaymentRouteName = '/profile-payment';
const String profilePersonalRouteName = '/profile-personal';
const String profileRouteName = '/profile';
const String profileSigninInfoRouteName = '/profile-signin';
const String subDetailsRouteName = '/shop-details';
const String shoppingRouteName = '/shopping';
const String signinInfoRouteName = '/signin';

final routeList = [
  HomeScreen(), BotScreen(), GameScreen(), CopyScreen(), ProfileScreen(), CreateBotScreen(), MenuScreen(), CreateCopyBotScreen(),
  // index:8
  ProfitScreen(),
  // index:9
  ProfileAccountScreen(),
  // index:10
  ReferralScreen(),
  // index:11
  ContactScreen(),
  // index:12
  ProfileDepositScreen(),
  // index:13
  ProfileSendScreen(),
  // index:14
  ProfileTransferScreen(),
  // index:15
  ProfileInternalScreen(),
  // index:16
  ActivityScreen(),

  SettingsScreen(),// index:17

  AboutUsScreen(),// index:18

  APISettingScreen(),// index:19

  BinanceApiScreen(),// index:20

  BinanceBindScreen(),// index:21

  NotifyScreen(),// index:22

  ChangePWScreen(),// index:23


];

List<String> warningTextList () {
  return [
    '請勿向上述地址發送任何非USDT(TRC20)的能量，否則能量將不可找回。',
    '最小接收金額：1USDT (TRC20)，小於最小金額將無法順利接收且無法退回。',
    '能量帳戶若低於<後台設定>，機器人將暫停量化。為確保機器人正常量化建議能量帳戶保持100 USDT以上。',
    '您發送至上述地址後，需要整個網路節點的確認。3次網路確認後到帳。',
    '目前不支持區塊鏈獎勵(Coinbase)，區塊鏈獎勵將無法接收，請您諒解。',
    '您的接收地址不會經常改變，可以重複接收；如有更改，我們會盡量通過網站公告或郵件通知您。',
    '請務必確認電腦及瀏覽器安全，防止資訊被竄改或洩露。'
  ];
}

bool boolToggle(bool input) {
  if (input == true){
    return false;
  } else {
    return true;
  }
}

class Constant {
  double getViewHeight (BuildContext context) {
    return MediaQuery.of(context).size.height;
  }
  double getViewWidth (BuildContext context) {
    return MediaQuery.of(context).size.width;
  }
}