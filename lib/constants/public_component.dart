import 'package:bobot01/constants/constants.dart';
import 'package:bobot01/ui/game/game_viewmodel.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import '../datetime/date_manager.dart';
import '../main.dart';
import '../ui/game/game_model/game_parameter.dart';
import 'SignInWidgets.dart';
import 'lightCell.dart';
import 'theme.dart';

Widget buildFlexBodyLayout(BuildContext context, Widget inputWidget, {bool isLoading = false}) {
  return Scaffold(
      backgroundColor: Colors.transparent,
      resizeToAvoidBottomInset: false,
      body: Container(
        // color: Colors.transparent,
        width: Constant().getViewWidth(context),
        height: Constant().getViewHeight(context) - Scaffold.of(context).appBarMaxHeight!.toInt(),
        child: Stack(children: [
          inputWidget,
          if(isLoading)CustomLoading()
        ],),
      )
  );
}


Widget buildFlexLayout(BuildContext context, Widget inputWidget, {bool isLoading = false}) {
  return Scaffold(
    resizeToAvoidBottomInset: false,
    appBar: PreferredSize(preferredSize: Size.fromHeight(0), child: AppBar(backgroundColor: CustomTheme.fontPurple01Color)),
    body: SafeArea(
      child: SingleChildScrollView(
        physics: NeverScrollableScrollPhysics(),
        child: Container(
          width: Constant().getViewWidth(context),
          height: Constant().getViewHeight(context) - MediaQuery.of(context).padding.top - MediaQuery.of(context).padding.bottom,
          child: Stack(children: [
            inputWidget,
            if(isLoading)CustomLoading()
          ],),
        ),
      ),
    ),
  );
}

Future<void> showResponseDialog(BuildContext context, String text, Function() onConfirm, {bool needCancel = false, String title = "", String img = "assets/icon.png"}) async {
  showDialog(
      context: context,
      builder: (_) =>  AlertDialog(
        backgroundColor: Colors.transparent,
        content: Container(
          alignment: Alignment.center,
          height: Constant().getViewHeight(navigatorKey.currentContext!) * 0.4,
          width: Constant().getViewWidth(navigatorKey.currentContext!) * 0.88,
          child: lightCell(isBar: true, Container(
          alignment: Alignment.center,
          color: Colors.transparent,
          height: Constant().getViewHeight(context)/2,
          margin: EdgeInsets.all(Constant().getViewHeight(context)/40),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Spacer(),
              Expanded(flex: 8, child: Container(height: Constant().getViewHeight(context)/8, child: Image.asset(img),)),
              Expanded(flex: 4, child: Container(alignment: Alignment.center, child:
              Text(title, style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 18, fontWeight: FontWeight.w700)),)),
              Expanded(flex: 6, child: SingleChildScrollView(child: Container(alignment: Alignment.center, child: Text(
                text,
                style: TextStyle(color: CustomTheme.fontPurple01Color, fontSize: 14, fontWeight: FontWeight.w700),
                textAlign: TextAlign.center,
              ),),)),
              Spacer(),
              Expanded(flex: 4, child: Row(children: [
                if(needCancel)Expanded(flex: 8, child: GestureDetector(child: Container(
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        border: Border.all(color: CustomTheme.fontPurple01Color),
                        borderRadius: BorderRadius.circular(35),
                        color: Colors.white
                    ),
                    child: Text("Cancel", style: CustomTheme.textPrimaryPurple01,)),
                  onTap: (){Navigator.of(navigatorKey.currentContext!).pop(false);},
                )),
                if(needCancel)Spacer(),
                Expanded(flex: 8, child: GestureDetector(child: Container(
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(35),
                        color: CustomTheme.fontPurple01Color
                    ),
                    child: Text("Confirm", style: CustomTheme.textPrimary,)),
                  onTap: (){onConfirm();Navigator.of(navigatorKey.currentContext!).pop(false);},
                )),
              ],)),
            ],
          ),)),),
  ));
}

Widget buildAppBar({required String title, required WidgetRef ref, int backPage = -2, bool needMenu = true}) {
  return Container(height: Constant().getViewHeight(navigatorKey.currentContext!)/20, alignment: Alignment.center, child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
    Expanded(flex: 1, child: backPage != -2 ? CircleButton(backPage, ref) : Container()),
    Expanded(flex: 6, child: Text(title, style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 18), textAlign: TextAlign.center,)),
    Expanded(flex: 1, child: needMenu ? SettingButton(ref) : Container()),
  ],));
}

class CustomLoading extends StatelessWidget {
  const CustomLoading({
    Key? key,
    this.size = 48.0,
  }) : super(key: key);

  final double size;

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      GestureDetector(child: Container(color: Colors.transparent, height: Constant().getViewHeight(context),width: Constant().getViewWidth(context)), onTap: (){},),
      Container(
        margin: EdgeInsets.symmetric(horizontal: Constant().getViewWidth(context)/3, vertical: Constant().getViewHeight(context)/3),
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(10.0), color: Colors.black87),
        child: SpinKitFadingCircle(
          color: Colors.white,
          size: size,
        ),)
    ],);
  }
}

Widget buildConfirmBtn(BuildContext context, String title, Function() confirmAction) {
  return Column(children: [
    GestureDetector(child: Container(
        width: double.maxFinite, height: 50,
        child: Center(child: Text(title, style: CustomTheme.textPrimary)),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(30),
            border: Border.all(width: 0.5, color: CustomTheme.fontPurple01Color),
            color: CustomTheme.fontPurple01Color
        )),
        onTap: () => confirmAction()),
    SizedBox(height: 120)
  ]);
}


Future<void> showGameSuccDialog(BuildContext context, String title, String des, Function() confirmAction) {
  return showCupertinoModalPopup(context: context, builder: (BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      actionsPadding: EdgeInsets.all(20),
      title: Image.asset('assets/img/img_bobot_yes_01.png', height: 100, width: 100,),
      content: Column(mainAxisSize: MainAxisSize.min, children: [
        SizedBox(height: 20),
        Text(title, style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 16, fontWeight: FontWeight.w800)),
      ]),
      contentPadding: EdgeInsets.zero,
      actions: <Widget>[
        GestureDetector(onTap: () { confirmAction(); },
            child: Container(
              child: Align(alignment: Alignment.center, child: Text('確認', style: CustomTheme.textPrimary)),
              height: 44, width: double.maxFinite,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: CustomTheme.fontPurple01Color
              ),
            ))
      ],
    );
  });
}

Future<void> showGameFailDialog(BuildContext context, String title, String des, Function() confirmLeftAction, Function() confirmRightAction) {
  return showCupertinoModalPopup(context: context, builder: (BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      actionsPadding: EdgeInsets.all(20),
      title: Image.asset('assets/img/img_bobot_no_01.png', height: 100, width: 100,),
      content: Column(mainAxisSize: MainAxisSize.min, children: [
        SizedBox(height: 20,),
        Text(title, style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 16, fontWeight: FontWeight.w800)),
        SizedBox(height: 20,),
        Text(des, style: TextStyle(color: CustomTheme.fontPurple01Color, fontSize: 14, fontWeight: FontWeight.w800)),
      ]),
      contentPadding: EdgeInsets.zero,
      actionsAlignment: MainAxisAlignment.center,
      actions: <Widget>[
        Visibility(visible: (confirmLeftAction != null), child: GestureDetector(onTap: () { confirmLeftAction(); },
            child: Container(
              child: Align(alignment: Alignment.center, child: Text('取消', style: TextStyle(color: CustomTheme.fontPurple01Color))),
              height: 35, width: Constant().getViewWidth(context)/3.5,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: CustomTheme.labelWhiteColor,
                  border: Border.all(width: 1, color: CustomTheme.fontPurple01Color)
              ),
            ))),
        Visibility(visible: (confirmRightAction != null), child: GestureDetector(onTap: () { confirmRightAction(); },
            child: Container(
              child: Align(alignment: Alignment.center, child: Text('確認', style: CustomTheme.textPrimary)),
              height: 35, width: Constant().getViewWidth(context)/3.5,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: CustomTheme.fontPurple01Color
              ),
            )))
      ],
    );
  });
}

Future<void> showGameHistoryDialog(BuildContext context, WidgetRef ref) async {
  final history = ref.watch(historyDataProvider);
  return showCupertinoModalPopup(context: context, builder: (BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      actionsPadding: EdgeInsets.all(20),
      title: Align(alignment: Alignment.center, child: Text('History', style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 24, fontWeight: FontWeight.w600))),
      content: Column(mainAxisSize: MainAxisSize.min, children: [
        Image.asset('assets/img/img_bobot_history_01.png', height: 100, width: 100),
        SizedBox(height: 20),
        Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          Text('總公里數', style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 14, fontWeight: FontWeight.w600)),
          Text('${history!.sumKm} KM', style: TextStyle(color: CustomTheme.fontPurple01Color, fontSize: 14, fontWeight: FontWeight.w600)),
        ]),
        SizedBox(height: 10),
        Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          Text('電池總量', style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 14, fontWeight: FontWeight.w600)),
          Text('${history.sumBattery} 顆', style: TextStyle(color: CustomTheme.fontPurple01Color, fontSize: 14, fontWeight: FontWeight.w600)),
        ]),
        SizedBox(height: 10),
        Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          Text('購買金額', style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 14, fontWeight: FontWeight.w600)),
          Text('${history.costPrice} USDT', style: TextStyle(color: CustomTheme.fontPurple01Color, fontSize: 14, fontWeight: FontWeight.w600)),
        ]),
        SizedBox(height: 10),
        Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          Text('總參與人數', style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 14, fontWeight: FontWeight.w600)),
          Text('${history.sumParticipate} 人', style: TextStyle(color: CustomTheme.fontPurple01Color, fontSize: 14, fontWeight: FontWeight.w600)),
        ]),
        SizedBox(height: 10),
        Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          Text('得獎人', style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 14, fontWeight: FontWeight.w600)),
          Text(history.winner, style: TextStyle(color: CustomTheme.fontPurple01Color, fontSize: 14, fontWeight: FontWeight.w600)),
        ]),
        SizedBox(height: 40),
      ]),
      contentPadding: EdgeInsets.symmetric(horizontal: 30),
    );
  });
}

Future<void> showGameBuyDialog(BuildContext context, String energy, String number, String balance, Function() confirmLeftAction, Function() confirmRightAction) {
  return showCupertinoModalPopup(context: context, builder: (BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      actionsPadding: EdgeInsets.all(20),
      title: Align(alignment: Alignment.center, child: Text('Buy Battery', style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 24, fontWeight: FontWeight.w600))),
      content: Column(mainAxisSize: MainAxisSize.min, children: [
        Image.asset('assets/img/img_battery_01.png', height: 100, width: 100),
        Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          Text('能量幣餘額', style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 14, fontWeight: FontWeight.w600)),
          Text('${energy} USDT', style: TextStyle(color: CustomTheme.fontPurple01Color, fontSize: 14, fontWeight: FontWeight.w600)),
        ]),
        SizedBox(height: 10),
        Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          Text('購買數量', style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 14, fontWeight: FontWeight.w600)),
          Text('${number} 顆', style: TextStyle(color: CustomTheme.fontPurple01Color, fontSize: 14, fontWeight: FontWeight.w600)),
        ]),
        SizedBox(height: 10),
        Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          Text('購買金額', style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 14, fontWeight: FontWeight.w600)),
          Text('${balance} USDT', style: TextStyle(color: CustomTheme.fontPurple01Color, fontSize: 14, fontWeight: FontWeight.w600)),
        ]),
      ]),
      contentPadding: EdgeInsets.symmetric(horizontal: 20),
      actionsAlignment: MainAxisAlignment.center,
      actions: <Widget>[
        GestureDetector(onTap: () { confirmLeftAction(); },
            child: Container(
              child: Align(alignment: Alignment.center, child: Text('取消', style: TextStyle(color: CustomTheme.fontPurple01Color))),
              height: 35, width: Constant().getViewWidth(context)/3.5,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: CustomTheme.labelWhiteColor,
                  border: Border.all(width: 1, color: CustomTheme.fontPurple01Color)
              ),
            )),
        GestureDetector(onTap: () { confirmRightAction(); },
            child: Container(
              child: Align(alignment: Alignment.center, child: Text('確認', style: CustomTheme.textPrimary)),
              height: 35, width: Constant().getViewWidth(context)/3.5,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: CustomTheme.fontPurple01Color
              ),
            ))
      ],
    );
  });
}
