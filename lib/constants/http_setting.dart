class HttpSetting {
  HttpSetting._();

  // base url
  static const String appUrl = "https://autotrader-dev.dbithk.com/";
  // static const String appUrl = "https://dashboard.bpowertech.com/";
  static const String commonUrl = 'https://feminist.social/gateway/common';
  static const String developSocketUrl='ws://autotrader-dev.dbithk.com/gateway/bobot3d/WebSocketServer';

  // receiveTimeout
  static const int receiveTimeout = 15000;

  // connectTimeout
  static const int connectionTimeout = 15000;
}
