import 'package:bobot01/constants/theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../main.dart';
import '../ui/sign_in/login_viewmodel.dart';
import 'constants.dart';

Widget CircleButton(int backPage, WidgetRef ref){
  var context = navigatorKey.currentContext!;
  return GestureDetector(
    onTap: (){
      backPage == -1
          ? Navigator.pop(context)
          : ref.read(bodyIndexProvider.notifier).setBodyIndex(backPage);
    },
    child: Container(
        width: Constant().getViewHeight(context)/14,
        child: Image.asset("assets/btn/btn_back_01_nor.png")
    )
  );
}
bool pwLengthValid(String pw){
  return pw.length >= 8 && pw.length <= 20;
}
Widget SettingButton(WidgetRef ref){
  return GestureDetector(
    onTap: (){
      scaffoldStateKey.currentState!.isEndDrawerOpen ? scaffoldStateKey.currentState!.closeEndDrawer() : scaffoldStateKey.currentState!.openEndDrawer();
    },
    child: Container(
        alignment: Alignment.centerLeft,
        child: Icon(Icons.menu, size: 40, color: CustomTheme.fontPurple01Color,),));
}

Widget CircleSearchButton(){
  var context = navigatorKey.currentContext!;
  return Container(
    alignment: Alignment.center,
    child: GestureDetector(
      onTap: (){
        // Navigator.pop(context);
      },
      child: Stack(children: [
        Container(
            width: Constant().getViewHeight(context)/14,
            alignment: Alignment.center,
            margin: EdgeInsets.symmetric(horizontal: Constant().getViewWidth(context)/100),
            decoration: BoxDecoration(
                color: CustomTheme.darkPurpleColor,
                border: Border.all(color: CustomTheme.darkPurpleColor),
                shape: BoxShape.circle
            ),
            child: Container(
              alignment: Alignment.center,
              padding: EdgeInsets.all(Constant().getViewWidth(context)/80),
              margin: EdgeInsets.only(left: 3, top: 1, bottom: Constant().getViewWidth(context)/60, right: Constant().getViewWidth(context)/80, ),
              decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: [BoxShadow(
                    color: CustomTheme.lightPurpleColor,
                    spreadRadius: 2,
                    blurRadius: 0,
                    offset: Offset(2, 2), // changes position of shadow
                  ),
                  ],
                  shape: BoxShape.circle),
              child: Container(),
            )),
        Container(alignment: Alignment.center, child: Icon(Icons.search_rounded, size: 30, color: CustomTheme.fontPurple01Color,),)
      ],)
  ),);
}

Widget SignInTextField(TextEditingController controller, Widget childWidget, FocusNode focusNode, String hint, {bool passwordVisible = false}) {
  return Container(
    decoration: BoxDecoration(
      border: Border.all(color: CustomTheme.darkPurpleColor),
      borderRadius: BorderRadius.all(Radius.circular(35)),
    ),
    child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(flex: 7, child: Container(
            // margin: EdgeInsets.all(Constant().getViewHeight(navigatorKey.currentContext!)/60),
            padding: EdgeInsets.symmetric(horizontal: Constant().getViewHeight(navigatorKey.currentContext!)/40),
            child: TextFormField(
              controller: controller,
              focusNode: focusNode,
              obscureText: passwordVisible,
              textAlign: TextAlign.start,
              textAlignVertical: TextAlignVertical.center,
              // inputFormatters: <TextInputFormatter>[
              //   FilteringTextInputFormatter.allow(RegExp("[0-9.]"))
              // ],
              decoration: InputDecoration(
                isDense: true,
                border: InputBorder.none,
                hintText: hint,
                // suffixIcon: Icon(Icons.percent, size: Constant().getViewWidth(context)/30, color: Colors.white)
              ),
              style: TextStyle(color: CustomTheme.fontPurple02Color, fontSize: 14),
              // focusNode: focusNode,
              // keyboardType: TextInputType.numberWithOptions(decimal: true)
            ))),
        Expanded(flex: 3, child: Container(alignment: Alignment.center, color: Colors.transparent, child: childWidget,))
      ],),
  );
}