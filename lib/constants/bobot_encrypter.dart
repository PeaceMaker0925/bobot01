import 'dart:convert';
import 'package:encrypt/encrypt.dart';


const String rsaKey =
    "-----BEGIN PUBLIC KEY-----\n"
    "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCdaqJNC+oIrljkNtxCf411HiVT95hDlB34937hz5ck92SZCNOLibnmv0xLJPP/Sw9jr8sdh2X7cMyMqfm2jADXMHTbswiCj093JkwrwBKkcK0mbqBZcuK/do51vcZJnosTpVOzXWCc+Z89LDpYVmjHnPoZHdvubC91DCJH2GUzKQIDAQAB"
    "\n-----END PUBLIC KEY-----";


class BobotEncrypter {
  String encodeSimpleData(Map<String, String> params){
    dynamic publicKey = RSAKeyParser().parse(rsaKey);
    final encrypter = Encrypter(RSA(publicKey: publicKey));

    dynamic bytes = utf8.encode(jsonEncode(params));

    var data = {
      "data": [encrypter.encrypt(base64.encode(bytes)).base64]
    };

    return jsonEncode(data);
  }
  String encodeData(Map<String, dynamic> params) {
    dynamic publicKey = RSAKeyParser().parse(rsaKey);
    final encrypter = Encrypter(RSA(publicKey: publicKey));

    dynamic bytes = utf8.encode(jsonEncode(params));

    List<int> sourceBytes = utf8.encode(base64.encode(bytes));
    int inputLen = sourceBytes.length;
    int maxLen = 117;
    List<int> totalBytes = List.empty(growable: true);

    for (var i = 0; i < inputLen; i += maxLen) {
      int endLen = inputLen - i;
      List<int> item;
      if (endLen > maxLen) {
        item = sourceBytes.sublist(i, i + maxLen);
      } else {
        item = sourceBytes.sublist(i, i + endLen);
      }
      totalBytes.addAll(encrypter.encryptBytes(item).bytes);
    }

    var data = {
      "data": [base64.encode(totalBytes)]
    };
    return jsonEncode(data);
  }

}