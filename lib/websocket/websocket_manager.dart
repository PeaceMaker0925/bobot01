import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:web_socket_channel/io.dart';
import 'package:web_socket_channel/web_socket_channel.dart';
import '../constants/http_setting.dart';
import '../constants/shared_preference.dart';

/// WebSocket地址
String _SOCKET_URL = HttpSetting.developSocketUrl;

/// WebSocket狀態
enum SocketStatus {
  SocketStatusConnected, // 已連線
  SocketStatusFailed, // 發生錯誤
  SocketStatusClosed, // 關閉連線
}

const DEFAULT_TIMEOUT = 30000 /*ms*/;

class WebSocketManager {
  /// 單例
  static WebSocketManager? _webSocketUtil;

  /// 內部構造方法，避免外部暴露
  WebSocketManager._();

  /// 獲取單例内部方法
  factory WebSocketManager() {
    // 只能有一个實例
    return _webSocketUtil ??= WebSocketManager._();
  }

  late IOWebSocketChannel _channel; // WebSocket
  StreamController streamController = StreamController.broadcast(); // 流控制器，多頁面才能監聽同一流
  late StreamSubscription dataSubscription;
  SocketStatus _socketStatus = SocketStatus.SocketStatusClosed; // 連線狀態
  late Timer _heartBeat; // 心跳
  final int _heartTimes = 5000; // 心跳間隔(毫秒)
  late int _heartBeatSendTimes = 0;
  final int _reconnectCount = 20; // 最大重連次數
  late int _reconnectTimes = 0; // 重連次數紀錄
  late Timer _reconnectTimer; // 重連定時
  late Function onError; // Socket Error
  late Function onOpen; // Socket Open
  late Function onMessage; // Get Message

  /// 初始化WebSocket
  void initWebSocket({required Function onOpen, required Function onMessage, required Function onError}) {
    this.onOpen = onOpen;
    this.onMessage = onMessage;
    this.onError = onError;
    if (_socketStatus != SocketStatus.SocketStatusConnected) {
      openSocket();
    }
  }

  /// 執行WebSocket連線
  void openSocket() async {
    Map<String, dynamic> header = {};
    final bool isLogin = "userToken" != await BobotSharedPreferences().getUserToken();
    final String memberId = await BobotSharedPreferences().getMemberId();
    debugPrint("isLoging: $isLogin, memberId: $memberId");
    _channel = IOWebSocketChannel.connect('${_SOCKET_URL}/${memberId}');
    debugPrint('WebSocket連線成功: $_SOCKET_URL');
    // 連線成功，將channel放進stream，由streamController來做監聽
    streamController.sink.addStream(_channel.stream);

    _socketStatus = SocketStatus.SocketStatusConnected;

    // 連線成功，重置計數器
    _reconnectTimes = 0;

    // 由MainScreen啟動心跳
    onOpen();

    // 接收消息
    socketListener();
  }

  /// WebSocket監聽
  void socketListener() {
    dataSubscription = streamController.stream.listen((data) => webSocketOnMessage(data),
        onError: webSocketOnError, onDone: webSocketOnDone);
  }

  /// WebSocket收到消息
  webSocketOnMessage(data) {
    debugPrint("reset hb time for receive msg.");
    _heartBeatSendTimes = 0;
    onMessage(data);
  }

  /// WebSocket關閉連線
  webSocketOnDone() {
    debugPrint('WebSocket關閉連線');
    reconnect();
  }

  /// WebSocket連線錯誤
  webSocketOnError(e) {
    WebSocketChannelException ex = e;
    _socketStatus = SocketStatus.SocketStatusFailed;
    onError(ex.message);
    closeSocket();
  }

  /// Init心跳
  void initHeartBeat() {
    _heartBeat =
        Timer.periodic(Duration(milliseconds: _heartTimes), (timer) {
          if(_heartBeatSendTimes > DEFAULT_TIMEOUT){
            // dataSubscription.cancel();
            // reconnect();
          }else{
            sentHeart();
            _heartBeatSendTimes += 5000;
          }
        });
  }

  /// 心跳
  void sentHeart() {
    debugPrint("send hb from app. There is ${DEFAULT_TIMEOUT-_heartBeatSendTimes} ms left to reconnect.");
    _sendMessage('hb');
  }

  /// 銷毀心跳
  void destroyHeartBeat() {
    _heartBeat.cancel();
  }

  /// 關閉WebSocket
  void closeSocket() {
    debugPrint('WebSocket關閉');
    _channel.sink.close();
    destroyHeartBeat();
    dataSubscription.cancel();
    _socketStatus = SocketStatus.SocketStatusClosed;
  }

  /// 心跳重連機制
  void reconnect() {
    debugPrint('wc reconnect');
    if (_reconnectTimes < _reconnectCount) {
      _reconnectTimes++;
      _reconnectTimer =
          Timer.periodic(Duration(milliseconds: _heartTimes), (timer) {
            openSocket();
          });
    } else {
      debugPrint('重連次數已達上限');
      _reconnectTimer.cancel();
      return;
    }
  }

  //////////////////////////////////////////////////////////////////////////////
  //// 上面連線 ////            //// 下面傳接 ////
  //////////////////////////////////////////////////////////////////////////////

  /// WebSocket Send Msg
  void _sendMessage(message) {
    switch (_socketStatus) {
      case SocketStatus.SocketStatusConnected:
        // debugPrint('訊息發送中');
        _channel.sink.add(message);
        break;
      case SocketStatus.SocketStatusClosed:
        debugPrint('已斷線');
        break;
      case SocketStatus.SocketStatusFailed:
        debugPrint('發訊息失敗');
        break;
      default:
        break;
    }
  }

  // /// WebSocket 發送Data轉binary (utf8)
  // sendMessage(WsSendMessageData data) {
  //   var jsonStr = json.encode(data);
  //   var utf8Data = utf8.encode(jsonStr);
  //   _sendMessage(utf8Data);
  //   debugPrint('發出去的jsonStr: $jsonStr');
  // }
  //
  // /// WebSocket ACK資料轉Data
  // WsAckSendMessageData getACKData(ackMessage) {
  //   var messageString = utf8.decode(ackMessage);
  //   var jsonStr = messageString; // Dio已自動轉成json
  //   debugPrint('ACK訊息：$jsonStr');
  //   WsAckSendMessageData data = wsAckSendMessageDataFromJson(jsonStr);
  //   return data;
  // }
}