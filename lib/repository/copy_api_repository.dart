import '../constants/shared_preference.dart';
import 'http_manager.dart';
import 'api_response.dart';

abstract class CopyApiRepository {
  Future<ApiResponse> queryCopyBot();
  Future<ApiResponse> queryCopyBotCoin();

}

class CopyApiRepositoryImpl extends HttpManager implements CopyApiRepository {
  @override
  Future<ApiResponse> queryCopyBot() async {
    return get('/gateway/app/follow/robotList', await BobotSharedPreferences().getUserToken());
  }
  @override
  Future<ApiResponse> queryCopyBotCoin() async {
    return get('/gateway/app/follow/coinList', await BobotSharedPreferences().getUserToken());
  }

}