import 'package:bobot01/ui/game/game_model/game_parameter.dart';

import '../constants/bobot_encrypter.dart';
import '../constants/shared_preference.dart';
import '../ui/bot/bot_model/CreateBotParameter.dart';
import 'http_manager.dart';
import 'api_response.dart';

abstract class GameApiRepository {
  Future<ApiResponse> createOrder(CreateOrderParameter param);
  Future<ApiResponse> buyBattery(BuyBatteryParameter param);
  Future<ApiResponse> getUserBalance(GetBalanceParameter param);
  Future<ApiResponse> getHistoryLottery(GetHistoryParameter param);
  Future<ApiResponse> getGameTimeConfig(GetGameTimeParameter param);
}

class GameApiRepositoryImpl extends HttpManager implements GameApiRepository {
  @override
  Future<ApiResponse> createOrder(CreateOrderParameter param) async {
    var params =  {
      "date": param.date,
      "accountID": param.accountID,
    };
    return post('gateway/bobot3d/api/Bobot3dOrder/add_Order', await BobotSharedPreferences().getUserToken(), data: BobotEncrypter().encodeData(params));
  }

  @override
  Future<ApiResponse> buyBattery(BuyBatteryParameter param) async {
    var params =  {
      "accountID": param.accountID,
      "amount": param.amount,
      "balance": param.balance
    };
    return post('gateway/bobot3d/api/Bobot3dOrder/buy_Gas', await BobotSharedPreferences().getUserToken(), data: BobotEncrypter().encodeData(params));
  }

  @override
  Future<ApiResponse> getUserBalance(GetBalanceParameter param) async {
    var params =  {
      "date": param.date,
      "accountID": param.accountID,
    };
    return post('gateway/bobot3d/api/Bobot3dInfo/get_user_balanceInfo', await BobotSharedPreferences().getUserToken(), data: BobotEncrypter().encodeData(params));
  }

  @override
  Future<ApiResponse> getHistoryLottery(GetHistoryParameter param) async {
    var params =  {
      "date": param.date,
    };
    return post('gateway/bobot3d/api/Bobot3dInfo/HistoryLottery', await BobotSharedPreferences().getUserToken(), data: BobotEncrypter().encodeData(params));
  }

  @override
  Future<ApiResponse> getGameTimeConfig(GetGameTimeParameter param) async {
    var params =  {
      "date": param.date,
    };
    return post('gateway/bobot3d/api/Bobot3dInfo/bobot3d_app_config', await BobotSharedPreferences().getUserToken(), data: BobotEncrypter().encodeData(params));
  }
}