import 'dart:convert';

// ApiResponse apiResponseFromJson(String str) =>
//     ApiResponse.fromJson(json.decode(str));

String apiResponseToJson(ApiResponse data) => json.encode(data.toJson());

class ApiResponse {
  ApiResponse({
    required this.code,
    required this.message,
    required this.url,
    this.data,
  });

  String code;
  String message;
  dynamic data;
  String url;

  factory ApiResponse.fromJson(Map<String, dynamic> json, String url) => ApiResponse(
      code: json["code"], message: json["message"], data: json["data"], url: url);

  Map<String, dynamic> toJson() => {
    "code": code,
    "message": message,
    "data": data,
  };

  void printLog(){
    print('=========== ApiResponse ===========');
    print('code:$code');
    print('message:$message');
    print(data.toString().length>1000?'data:$data'.substring(0,1000):'data:$data');
    print('=============== End ===============');
  }
}
