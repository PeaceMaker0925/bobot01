import '../constants/bobot_encrypter.dart';
import '../constants/shared_preference.dart';
import '../ui/profile/account/account_viewmodel.dart';
import 'api_response.dart';
import 'http_manager.dart';

abstract class AccountApiRepository {
  Future<ApiResponse> queryAssetInfo();
  Future<ApiResponse> queryAssetRecord(AssetRecordParameter parameter);
  Future<ApiResponse> queryTransationType();
  Future<ApiResponse> assetWithdraw(AssetWithdrawParameter params);
  Future<ApiResponse> assetTransfer(String amount);
  Future<ApiResponse> assetInnerTransfer(InnerTransferParameter params);
  Future<ApiResponse> queryDepositInfo();
  Future<ApiResponse> queryWithdrawInfo();
}

class AccountApiRepositoryImpl extends HttpManager implements AccountApiRepository {
  @override
  Future<ApiResponse> queryAssetInfo() async {
    return get('/gateway/app/asset/info', await BobotSharedPreferences().getUserToken());
  }

  @override
  Future<ApiResponse> queryWithdrawInfo() async {
    return get('/gateway/app/asset/withdraw/info', await BobotSharedPreferences().getUserToken());
  }

  @override
  Future<ApiResponse> queryTransationType() async {
    return get('/gateway/common/query/transationType?type=apart', await BobotSharedPreferences().getUserToken());
  }

  @override
  Future<ApiResponse> queryAssetRecord(AssetRecordParameter parameter) async {
    return get(
        '/gateway/app/asset/record?'
        'startTime=${parameter.startTime}&'
        'endTime=${parameter.endTime}&'
        'walletType=${parameter.walletType}&'
        'positionType=${parameter.positionType}'
        , await BobotSharedPreferences().getUserToken()
    );
  }

  @override
  Future<ApiResponse> assetInnerTransfer(InnerTransferParameter parameter) async {
    var params =  {
      "unit": parameter.unit,
      "chainName": parameter.chainName,
      "walletType": parameter.walletType,
      "payee": parameter.payee,
      "amount": parameter.amount,
      "password": parameter.password,
      "verifyCode": parameter.verifyCode,
      "code": parameter.code
    };
    return post('/gateway/app/asset/innerTransfer', await BobotSharedPreferences().getUserToken(),  data: BobotEncrypter().encodeData(params));

  }

  @override
  Future<ApiResponse> assetTransfer(String amount) async {
    var params =  {
      "amount": amount,
    };
    return post('/gateway/app/asset/transfer', await BobotSharedPreferences().getUserToken(),  data: BobotEncrypter().encodeData(params));

  }

  @override
  Future<ApiResponse> assetWithdraw(AssetWithdrawParameter parameter) async {
    var params =  {
      "unit": parameter.unit,
      "chainName": parameter.chainName,
      "address": parameter.address,
      "amount": parameter.amount,
      "fee": parameter.fee,
      "arrivedAmt": parameter.arrivedAmt,
      "password": parameter.password,
      "verifyCode": parameter.verifyCode,
      "code": parameter.code
    };
    return post('/gateway/app/asset/withdraw', await BobotSharedPreferences().getUserToken(),  data: BobotEncrypter().encodeData(params));
  }

  @override
  Future<ApiResponse> assetSendVerifyCode(String account) async {
    return get('/gateway/common/mail/send/verify?lang=zh-TW&action=withdraw', await BobotSharedPreferences().getUserToken());
  }

  @override
  Future<ApiResponse> assetInnerTransferVerifyCode(String account) async {
    return get('/gateway/common/mail/send/verify?lang=zh-TW&action=withdraw', await BobotSharedPreferences().getUserToken());
  }

  @override
  Future<ApiResponse> queryDepositInfo() async {
    return get('/gateway/app/asset/deposit/info', await BobotSharedPreferences().getUserToken());
  }

}