import '../constants/bobot_encrypter.dart';
import '../constants/shared_preference.dart';
import '../ui/profile/profile_viewmodel.dart';
import 'api_response.dart';
import 'http_manager.dart';

abstract class ProfileApiRepository {
  Future<ApiResponse> queryUserInfo();
  Future<ApiResponse> editUserName(String newName);
  Future<ApiResponse> queryTotalProfit();
  Future<ApiResponse> queryProfitInfo(ProfitInfoParameter parameter);
  // Future<ApiResponse> createBot(CreateBotParameter params);
}

class ProfileApiRepositoryImpl extends HttpManager implements ProfileApiRepository {
  @override
  Future<ApiResponse> queryUserInfo() async {
    return get('/gateway/app/user/info', await BobotSharedPreferences().getUserToken());
  }

  @override
  Future<ApiResponse> editUserName(String newName) async {
    var params =  {
      "updateMode": "NAME",
      "content": newName,
    };
    return post('/gateway/app/user/update', await BobotSharedPreferences().getUserToken(),  data: BobotEncrypter().encodeData(params));
  }

  @override
  Future<ApiResponse> queryProfitInfo(ProfitInfoParameter parameter) async {
    return get(
        '/gateway/app/quantify/robot/income/all?'
            'startTime=${parameter.startTime}&'
            'endTime=${parameter.endTime}'//&'
            // 'walletType=${parameter.walletType}&'
            // 'positionType=${parameter.positionType}'
        , await BobotSharedPreferences().getUserToken()
    );
  }

  @override
  Future<ApiResponse> queryAgentInfo() async {
    return get('/gateway/app/agent/info/v1', await BobotSharedPreferences().getUserToken());
  }

  @override
  Future<ApiResponse> queryTotalProfit() async {
    return get('/gateway/app/income/info/v1', await BobotSharedPreferences().getUserToken());
  }

}