import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:web_socket_channel/web_socket_channel.dart';
import '../constants/shared_preference.dart';
import '../ui/home/kline_model/candle.dart';
import 'api_response.dart';
import 'http_manager.dart';

abstract class HomeApiRepository {
  Future<ApiResponse> queryAssetDeposit();
  Future<ApiResponse> queryAssetWithdraw();
  Future<ApiResponse> queryIndexPrice();
  Future<ApiResponse> queryIndexPop();
  Future<ApiResponse> queryIncome();
  Future<ApiResponse> queryAllBotInfo();
}

class HomeApiRepositoryImpl extends HttpManager implements HomeApiRepository {

  @override
  Future<ApiResponse> queryEnergyBalance() async {
    return get('/gateway/app/quantify/energy/balance', await BobotSharedPreferences().getUserToken());
  }

  @override
  Future<ApiResponse> queryAssetDeposit() async {
    return get('/gateway/app/asset/deposit/info', await BobotSharedPreferences().getUserToken());
  }

  @override
  Future<ApiResponse> queryAssetWithdraw() async {
    return get('/gateway/app/asset/withdraw/info', await BobotSharedPreferences().getUserToken());
  }

  @override
  Future<ApiResponse> queryIndexPop() async {
    return get('/gateway/app/index/pop?lang=zh-TW', await BobotSharedPreferences().getUserToken());
  }

  @override
  Future<ApiResponse> queryIndexPrice() async {
    return get('/gateway/app/index/price/v1', await BobotSharedPreferences().getUserToken());
  }

  @override
  Future<ApiResponse> queryAllBotInfo() async {
    return get('/gateway/app/quantify/robot/info/all', await BobotSharedPreferences().getUserToken());
  }

  @override
  Future<ApiResponse> queryIncome() async {
    return get('/gateway/app/income/info/v1', await BobotSharedPreferences().getUserToken());
  }

  @override
  Future<ApiResponse> queryIncomeHome() async {
    return get('/gateway/app/income/home', await BobotSharedPreferences().getUserToken());
  }
}

class BinanceRepository {
  Future<List<Candle>> fetchCandles(
      {required String symbol, required String interval, int? endTime}) async {
    final uri = Uri.parse(
        "https://api.binance.com/api/v3/klines?symbol=$symbol&interval=$interval" +
            (endTime != null ? "&endTime=$endTime" : ""));
    final res = await http.get(uri);
    return (jsonDecode(res.body) as List<dynamic>)
        .map((e) => Candle.fromJson(e))
        .toList()
        .reversed
        .toList();
  }

  Future<List<String>> fetchSymbols() async {
    final uri = Uri.parse("https://api.binance.com/api/v3/ticker/price");
    final res = await http.get(uri);
    return (jsonDecode(res.body) as List<dynamic>)
        .map((e) => e["symbol"] as String)
        .toList();
  }

  WebSocketChannel establishConnection(String symbol, String interval) {
    final channel = WebSocketChannel.connect(
      Uri.parse('wss://stream.binance.com:9443/ws'),
    );
    channel.sink.add(
      jsonEncode(
        {
          "method": "SUBSCRIBE",
          "params": [symbol + "@kline_" + interval],
          "id": 1
        },
      ),
    );
    return channel;
  }
}