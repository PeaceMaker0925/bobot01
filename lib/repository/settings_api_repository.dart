import 'dart:convert';

import '../constants/bobot_encrypter.dart';
import '../constants/shared_preference.dart';
import '../ui/menu/settings/settings_viewmodel.dart';
import 'api_response.dart';
import 'http_manager.dart';

abstract class SettingsApiRepository {
  Future<ApiResponse> queryApiInfo();
  Future<ApiResponse> queryNotifyInfo();
  Future<ApiResponse> bindApi(ApiKeyParameter parameter);
  Future<ApiResponse> updateNotify(UpdateNotifyParameter parameter);
  Future<ApiResponse> cleanApi(String marketName);
  // Future<ApiResponse> queryTotalProfit();
  // Future<ApiResponse> queryProfitInfo();
// Future<ApiResponse> createBot(CreateBotParameter params);
}

class SettingsApiRepositoryImpl extends HttpManager implements SettingsApiRepository {


  @override
  Future<ApiResponse> queryNotifyInfo() async {
    return get('/gateway/app/preference/noticeSetting/v1', await BobotSharedPreferences().getUserToken());
  }

  @override
  Future<ApiResponse> updateNotify(UpdateNotifyParameter parameter) async {
    var params = {
      "lowEnergyStatus": parameter.lowEnergyStatus,
      "lowEnergyAmount": "50",
      "positionEnoughStatus": parameter.positionEnoughStatus,
      "receiveIncomeStatus": parameter.receiveIncomeStatus,
      "systemStatus": parameter.systemStatus
    };
    return post('/gateway/app/preference/update/noticeSetting/v1', await BobotSharedPreferences().getUserToken(), data: BobotEncrypter().encodeData(params));
  }

  @override
  Future<ApiResponse> changePassword(ChangePasswordParameter parameter) async {
    var params = {
      "lowEnergyStatus": parameter.lowEnergyStatus,
      "lowEnergyAmount": "50",
      "positionEnoughStatus": parameter.positionEnoughStatus,
      "receiveIncomeStatus": parameter.receiveIncomeStatus,
      "systemStatus": parameter.systemStatus
    };
    return post('/gateway/app/securitycenter/password/update', await BobotSharedPreferences().getUserToken(), data: BobotEncrypter().encodeData(params));
  }

  @override
  Future<ApiResponse> queryApiInfo() async {
    return get('/gateway/app/api/info', await BobotSharedPreferences().getUserToken());
  }

  @override
  Future<ApiResponse> bindApi(ApiKeyParameter parameter) async {

    var keys = {
    "accessKey": parameter.accessKey,
    "secretKey": parameter.secretKey
    };

    var pp = {
      "marketName": parameter.marketName,
      "apiKey": keys
      // jsonObject
      // {
      //   jsonEncode("accessKey"): jsonEncode(parameter.accessKey),
      //   jsonEncode("secretKey"): jsonEncode(parameter.secretKey),
      // },
    };
    // print(pp);
    return post('/gateway/app/api/bind', await BobotSharedPreferences().getUserToken(), data: BobotEncrypter().encodeData(pp));
  }

  @override
  Future<ApiResponse> cleanApi(String marketName) async {
    var params =  {
      "marketName": marketName,
    };
    return post('/gateway/app/api/delete', await BobotSharedPreferences().getUserToken(),  data: BobotEncrypter().encodeData(params));
  }}