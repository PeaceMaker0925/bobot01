import '../constants/shared_preference.dart';
import 'http_manager.dart';
import 'api_response.dart';
import '../ui/bot/bot_model/EnableBotParameter.dart';

abstract class ActivationApiRepository {
  Future<ApiResponse> queryActivationStatus();
  Future<ApiResponse> queryActivationRecord();
  Future<ApiResponse> queryActivationInfo();
  Future<ApiResponse> queryActivationCouponInfo(String code);
  Future<ApiResponse> activationEnable(EnableBotParameter parameter);
}

class ActivationApiRepositoryImpl extends HttpManager implements ActivationApiRepository {
  @override
  Future<ApiResponse> queryActivationStatus() async {
    return get('/gateway/app/activation/status', await BobotSharedPreferences().getUserToken());
  }

  @override
  Future<ApiResponse> queryActivationInfo() async {
    return get('/gateway/app/activation/info', await BobotSharedPreferences().getUserToken());
  }

  @override
  Future<ApiResponse> queryActivationRecord() async {
    return get('/gateway/app/activation/record', await BobotSharedPreferences().getUserToken());
  }

  @override
  Future<ApiResponse> queryActivationCouponInfo(String code) async {
    return get('/gateway/app/activation/coupon/info?coupon=$code', await BobotSharedPreferences().getUserToken());
  }

  @override
  Future<ApiResponse> activationEnable(EnableBotParameter parameter) {
    // TODO: implement activationEnable
    throw UnimplementedError();
  }

}