import 'package:dio/dio.dart';

import '../constants/http_setting.dart';
import 'api_response.dart';
import 'http_exceptions.dart';
typedef ResponseErrorFunction = void Function(String errorMessage);

class HttpManager {
  final _dio = Dio(BaseOptions(
      baseUrl: HttpSetting.appUrl,
    )
  );



  ApiResponse _checkResponse(Response response) {
    // print(response.toString().substring(0,1000));
    print(response.realUri);
    var result = ApiResponse.fromJson(response.data, response.realUri.toString());
    if (result.code.compareTo("0") == 0) {
    return result;
    } else {
      result.printLog();
      //取代錯誤code
      // response.statusCode = 404;
      // response.data['message'] = result.message;
      return result;
      // throw DioError(
      //     requestOptions: response.requestOptions,
      //     response: response,
      //     type: DioErrorType.response);

    }
  }

  Future<void> addHeaderToDio(String token) async {
    _dio.options.headers["Authorization"] = "Bearer $token";
    print("Authorization:${_dio.options.headers['Authorization']}");
  }

  Future<ApiResponse> get(
      String url,
      String token,{
        Map<String, dynamic>? queryParameters,
        Options? options,
        CancelToken? cancelToken,
        ProgressCallback? onReceiveProgress,
      }) async {
    await addHeaderToDio(token);
    try {
      final Response response = await _dio.get(
        url,
        queryParameters: queryParameters,
        options: options,
        cancelToken: cancelToken,
        onReceiveProgress: onReceiveProgress,
      );
      return _checkResponse(response);
    } on DioError catch (e) {
      final errorMessage = HttpExceptions.fromDioError(e).toString();
      print(errorMessage);
      throw errorMessage;
    } catch (e) {
      print(e.toString());
      rethrow;
    }
  }

  Future<ApiResponse> post(
      String url,
      String token,
      {
        data,
        Map<String, dynamic>? queryParameters,
        Options? options,
        CancelToken? cancelToken,
        ProgressCallback? onSendProgress,
        ProgressCallback? onReceiveProgress,
      }) async {
    await addHeaderToDio(token);
    try {
      final Response response = await _dio.post(
        url,
        data: data,
        queryParameters: queryParameters,
        options: options,
        cancelToken: cancelToken,
        onSendProgress: onSendProgress,
        onReceiveProgress: onReceiveProgress,
      );
      return _checkResponse(response);
    } on DioError catch (e) {
      final errorMessage = HttpExceptions.fromDioError(e).toString();
      throw errorMessage;
    } catch (e) {
      print(e.toString());
      rethrow;
    }
  }

}