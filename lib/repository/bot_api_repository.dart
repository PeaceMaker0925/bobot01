import '../constants/bobot_encrypter.dart';
import '../constants/shared_preference.dart';
import '../ui/bot/bot_model/ControlBotParameter.dart';
import 'http_manager.dart';
import 'api_response.dart';
import '../ui/bot/bot_model/CreateBotParameter.dart';

abstract class BotApiRepository {
  Future<ApiResponse> queryBalance();
  Future<ApiResponse> queryMyBot();
  Future<ApiResponse> queryBotInfo(String tradeType);
  Future<ApiResponse> queryBotSettings();
  Future<ApiResponse> createBot(CreateBotParameter params);
  Future<ApiResponse> controlBot(ControlBotParameter parameter);
}

class BotApiRepositoryImpl extends HttpManager implements BotApiRepository {
  @override
  Future<ApiResponse> queryBotInfo(String tradeType) async {
    return get('/gateway/app/quantify/robot/info?tradeType=$tradeType', await BobotSharedPreferences().getUserToken());
  }

  @override
  Future<ApiResponse> queryBotSettings() async {
    return get('/gateway/app/quantify/robot/setting?tradeType=spot', await BobotSharedPreferences().getUserToken());
  }

  @override
  Future<ApiResponse> queryConnectedEx() async {
    return get('/gateway/app/quantify/create/marketList', await BobotSharedPreferences().getUserToken());
  }

  @override
  Future<ApiResponse> queryExCoinList(String exName) async {
    return get('/gateway/app/quantify/create/coinList?marketName=$exName', await BobotSharedPreferences().getUserToken());
  }

  @override
  Future<ApiResponse> queryCreateBobotList(String exName, String coinName) async {
    return get('/gateway/app/quantify/create/robotList?marketName=$exName&coinName=$coinName', await BobotSharedPreferences().getUserToken());
  }

  @override
  Future<ApiResponse> queryRange(String exName, String coinName, String tradeType) async {
    return get('/gateway/app/quantify/robot/coin/setting?coinName=$coinName&marketName=$exName&tradeType=$tradeType', await BobotSharedPreferences().getUserToken());
  }

  @override
  Future<ApiResponse> queryMyBot() async {
    return get('/gateway/app/quantify/robot/info/all', await BobotSharedPreferences().getUserToken());
  }

  @override
  Future<ApiResponse> queryBalance() async {
    return get('/gateway/app/quantify/balance', await BobotSharedPreferences().getUserToken());
  }

  @override
  Future<ApiResponse> createBot(CreateBotParameter parameter) async {
    var params =  {
      "marketName": parameter.marketName,
      "coinName": parameter.coinName,
      "positionNum": parameter.positionNum,
      "strategyType": parameter.strategyType,
      "tradeType": parameter.tradeType,
      "lever": parameter.lever,
    };
    return post('/gateway/app/quantify/create', await BobotSharedPreferences().getUserToken(), data: BobotEncrypter().encodeData(params));
  }

  @override
  Future<ApiResponse> controlBot(ControlBotParameter parameter) async {
    var params =  {
      "robotNo": parameter.robotNo,
      "status": parameter.status,
    };
    return post('/gateway/app/quantify/update/control', await BobotSharedPreferences().getUserToken(), data: BobotEncrypter().encodeData(params));
  }


}