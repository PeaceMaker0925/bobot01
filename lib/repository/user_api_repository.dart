import 'dart:convert';
import 'package:bobot01/constants/shared_preference.dart';
import 'package:bobot01/repository/api_response.dart';
import 'package:bobot01/repository/http_manager.dart';
import 'package:intl/intl.dart';
import '../constants/bobot_encrypter.dart';
import '../ui/home/model/CheckSignupParameter.dart';
import '../ui/home/model/CheckVerifyCodeParameter.dart';
import '../ui/home/model/DeviceInfoParameter.dart';
import '../ui/home/model/ForgetPasswordParameter.dart';
import '../ui/home/model/SignUpParameter.dart';


abstract class UserApiRepository {
  /// User
  Future<ApiResponse> login(String account, String password, String verifyCode);
  Future<ApiResponse> logout();
  Future<ApiResponse> getLoginVerifyCode(String account);

  Future<ApiResponse> saveDeviceInfo(DeviceInfoParameter parameter);

  Future<ApiResponse> forgetPassword(ForgetPasswordParameter parameter);
  Future<ApiResponse> getForgetPswVerifyCode(String account);
  Future<ApiResponse> checkVerifyCode(CheckVerifyCodeParameter parameter);

  Future<ApiResponse> signup(SignUpParameter parameter);
  Future<ApiResponse> getSignUpVerifyCode(String account);
  Future<ApiResponse> checkSignup(CheckSignUpParameter parameter);
  /// My
  Future<ApiResponse> editMemberInfo();
}

class UserApiRepositoryImpl extends HttpManager implements UserApiRepository {

  @override
  Future<ApiResponse> login(String account, String password, String verifyCode) async {

    // var now = DateTime.now();
    // var formatter = DateFormat('yyyy-MM-dd');
    // String formattedDate = formatter.format(now);
    // print(formattedDate);
    var params =  {
      // "date": formattedDate,
      "account": account, //ericshih0925@gmail.com
      "password": password, //Test1234
      "code": verifyCode
    };

    return post('/gateway/app/user/login', "",  data: BobotEncrypter().encodeData(params));
  }

  @override
  Future<ApiResponse> getLoginVerifyCode(String account) {
    return get('/gateway/common/mail/send/login?email=$account&lang=zh-TW', "");
  }

  Future<String> getTestString(String account){
    return Future(() => "hello future");
  }

  @override
  Future<ApiResponse> logout() async {
    return post('/gateway/app/user/logout', await BobotSharedPreferences().getUserToken(), data: '');
  }

  @override
  Future<ApiResponse> saveDeviceInfo(DeviceInfoParameter parameter) async {
    // var now = DateTime.now();
    // var formatter = DateFormat('yyyy-MM-dd');
    // String formattedDate = formatter.format(now);
    var params =  {
      "uuid": parameter.uuid,
      "language": parameter.language,
      "device": parameter.device,
      "deviceInfo": parameter.deviceInfo,
      "deviceToken": parameter.deviceToken,
      // "date": formattedDate
    };

    return post('/gateway/app/user/save/deviceInfo', await BobotSharedPreferences().getUserToken(),  data: BobotEncrypter().encodeData(params));
  }

  @override
  Future<ApiResponse> forgetPassword(ForgetPasswordParameter parameter) async {
    var params =  {
      "registerMode": parameter.registerMode,
      "email": parameter.email,
      "country": parameter.country,
      "phone": parameter.phone,
      "password": parameter.password,
    };

    return post('/gateway/app/user/forgetPassword', await BobotSharedPreferences().getUserToken(),  data: BobotEncrypter().encodeData(params));
  }

  @override
  Future<ApiResponse> editMemberInfo() async {
    var params =  {
      "updateMode": "NAME",
      "content": "123",
    };
    return post('/gateway/app/user/update', await BobotSharedPreferences().getUserToken(),  data: BobotEncrypter().encodeData(params));
  }

  @override
  Future<ApiResponse> checkSignup(CheckSignUpParameter parameter) async {
    return get('/gateway/app/user/check/${parameter.name}?param=${parameter.param}', await BobotSharedPreferences().getUserToken());
  }

  @override
  Future<ApiResponse> checkVerifyCode(CheckVerifyCodeParameter parameter) async {
    // var now = DateTime.now();
    // var formatter = DateFormat('yyyy-MM-dd');
    // String formattedDate = formatter.format(now);
    var params =  {
      "action": parameter.action,
      "registerMode": parameter.registerMode,
      "email": parameter.email,
      "country": parameter.country,
      "phone": parameter.phone,
      "verifyCode": parameter.verifyCode,
      // "date": formattedDate
    };
    print(params);

    return post('/gateway/app/user/check/verifyCode', await BobotSharedPreferences().getUserToken(),  data: BobotEncrypter().encodeData(params));
  }

  @override
  Future<ApiResponse> signup(SignUpParameter parameter) async {
    // var now = DateTime.now();
    // var formatter = DateFormat('yyyy-MM-dd');
    // String formattedDate = formatter.format(now);
    var params =  {
      "memberId": parameter.memberId,
      "registerMode": parameter.registerMode,
      "email": parameter.email,
      "country": parameter.country,
      "phone": parameter.phone,
      "password": parameter.password,
      "name": parameter.name,
      "inviteCode": parameter.inviteCode,
      "imageUrl": parameter.imageUrl,
      // "date": formattedDate
    };

    return post('/gateway/app/user/register', await BobotSharedPreferences().getUserToken(),  data: BobotEncrypter().encodeData(params));
  }

  @override
  Future<ApiResponse> getForgetPswVerifyCode(String account) {
    return get('/gateway/common/mail/send/forgetPsw?email=$account&lang=zh-TW', "");
  }

  @override
  Future<ApiResponse> getSignUpVerifyCode(String account) {
    return get('/gateway/common/mail/send/register?email=$account&lang=zh-TW', "");
  }

}